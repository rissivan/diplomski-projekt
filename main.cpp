#include <iostream>
#include <thread>

#include <boost/asio.hpp>
#include <boost/asio/thread_pool.hpp>
#include <memory>
#include <random>
#include <chrono>
#include <fstream>
#include <iomanip>

#include "VehicleRoutingProblem/VRP.h"
#include "Phenotype/PermutationPhenotype.h"
#include "Phenotype/RKRealPhenotype.h"
#include "Algorithm/GA.h"
#include "Algorithm/DE.h"
#include "Algorithm/BSA.h"
#include "Algorithm/PSO.h"
#include "Algorithm/ACO.h"
#include "Algorithm/ABC.h"
#include "Algorithm/TS.h"
#include "Algorithm/SA.h"
#include "Util/Util.h"
#include "Runners.h"

using namespace std;


int main() {

//    runGAThreads();

//    runGA();
//    runABC();
//    runTS();
//
//    runDE();
//    runBSA();
//    runPSO();
//    runSA();
//
//    runACO();

//    runPSO2ndRun();
//    runACO2ndRun();

//    runGA2();
//    runDE2();
//    runBSA2();
//    runPSO2();
//    runACO2();
//    runABC2();
//    runTS2();
//    runSA2();

//    auto start = chrono::high_resolution_clock::now();

//    VRP vrp("../data/A/A-n32-k5.vrp");
//    cout << vrp.calculateDistanceFromFile("../data/A/A-n32-k5.sol");

//    double guided = 0, rnd = 0;
//
//    for(int i = 0; i < 100; i++){
//        PermutationPhenotype p(vrp, PermutationPhenotype::GUIDED_CLASSIC);
//        p.evaluate();
//        cout << p << endl << p.getPenaltyVal() << endl << p.isViableSolution();
//        guided += p.getPenaltyVal();
//    }
//
//    for(int i = 0; i < 100; i++){
//        PermutationPhenotype p(vrp, PermutationPhenotype::RANDOM_FEASIBLE);
//        p.evaluate();
//        cout << p << endl << p.getPenaltyVal() << endl << p.isViableSolution();
//        rnd += p.getPenaltyVal();
//    }
//
//    cout << endl << guided / 100 << " " << rnd / 100 << endl;



//    ofstream os("../results/output.txt");


//    for(int i = 0; i < 10; i++){
//        GA ga(vrp);
//        ga.setSearchNeighbourhood(false);
//        ga.setConstructionMode(BasePhenotype::RANDOM_FEASIBLE);
//        ga.outputToFile("../results/GA/iteration_graph_data/GA_iterations_n60_" + to_string(i + 1) + ".txt");
//        ga.run();
//    }

//    for (int i = 0; i < 10; i++) {
//
//        GA ga(vrp);
//        ga.setSearchNeighbourhood(true);
//        ga.setPM(0.03);
//        ga.setPopulationSize(100);
//        ga.setConstructionMode(BasePhenotype::GUIDED_CLASSIC);
//    //    ga.outputToFile("../results/GA/iteration_graph_data/GA_iterations_n60_" + to_string(i + 1) + ".txt");
//        ga.run();
//    }
//    ga.printBest(os);


//    DE de(vrp);
//    de.setConstructionMode(BasePhenotype::GUIDED_CLASSIC);
//    de.setSearchNeighbourhood(true);
//    de.run();
//    de.printBest(os);

//    BSA bsa(vrp);
//    bsa.setConstructionMode(BasePhenotype::GUIDED_CLASSIC);
//    bsa.setSearchNeighbourhood(true);
//    bsa.run();
//    bsa.printBest(os);

//    PSO pso(vrp);
//    pso.setConstructionMode(BasePhenotype::GUIDED_CLASSIC);
//    pso.setSearchNeighbourhood(true);
//    pso.run();
//    pso.printBest(os);

//    ACO aco(vrp);
//    aco.setConstructionMode(BasePhenotype::GUIDED_CLASSIC);
//    aco.setSearchNeighbourhood(true);
//    aco.run();
//    aco.printBest(os);

//    ABC abc(vrp);
//    abc.setConstructionMode(BasePhenotype::GUIDED_CLASSIC);
//    abc.run();
//    abc.printBest(os);

//    TS ts(vrp);
//    ts.setConstructionMode(BasePhenotype::GUIDED_CLASSIC);
//    ts.run();
//    ts.printBest(os);

//    SA sa(vrp);
//    sa.setConstructionMode(BasePhenotype::GUIDED_CLASSIC);
//    sa.run();
//    sa.printBest(os);
//
//    auto stop = chrono::high_resolution_clock::now();
//
//    cout << "Duration: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << "ms." << endl;
//    os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

    return 0;
}
