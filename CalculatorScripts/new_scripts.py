optima = {"A-n32-k5": 787.082,
          "A-n60-k9": 1355.799,
          "A-n80-k10": 1766.5}

vrpPaths = ["A-n32-k5", "A-n60-k9", "A-n80-k10"]

gcrfDict = {"GC": "PP",
            "RF": "NV"}

trueFalseDict = {"true": "DA",
                 "false": "NE"}


def decToString(d):
    return f'{d:.6f}'


def decToString3(d):
    return f'{d:.3f}'


def findMin2Vars(data, vrpPath, data1, data2):
    m = data[(vrpPath, data1[0], data2[0])]
    for d1 in data1:
        for d2 in data2:
            t = data[(vrpPath, d1, d2)]
            if t < m:
                m = t
    return m


def findMin1Var(data, vrpPath, data1):
    m = data[(vrpPath, data1[0])]
    for d1 in data1:
        t = data[(vrpPath, d1)]
        if t < m:
            m = t
    return m


def printSmallTableResults(alg, data, data1, data1Headline, secondRun=False, suffix=None):
    path = "../results/" + alg + "/table_data/" + alg + ".txt"
    output_file = open(path, 'w' if not secondRun else 'a')

    n = len(data1)

    output_file.write("\\begin{table}[ht!]\n")
    output_file.write("\t\\centering\n")
    output_file.write("\t\\begin{tabular}{|c||c||")
    for i in range(n):
        output_file.write("c|")
    output_file.write("}\n")
    output_file.write("\t\t\\hline & & \\multicolumn{" + str(n) + "}{c|}{" + data1Headline + "} \\\\\n")
    output_file.write("\t\t\\hhline{|>{\\arcw}->{\\arcb}||>{\\arcw}->{\\arcb}||*{" + str(n) +
                      "}{-|}} \\multirow{-2}{*}{Instanca} & \\multirow{-2}{*}{Optimum} ")
    for d in data1:
        output_file.write(" & " + str(d))
    output_file.write(" \\\\\n")

    for i in range(3):
        m = findMin1Var(data, vrpPaths[i], data1)

        def decorateOutput(x):
            if abs(x - m) < 1E-6:
                return "\\ccg " + (decToString3(x))
            return decToString3(x)

        if i == 0:
            output_file.write("\t\t\\hhline{|=||=||")
            for j in range(n):
                output_file.write("=|")
            output_file.write("} ")
        else:
            output_file.write("\t\t\\hline ")

        output_file.write(vrpPaths[i] + " & " + str(optima[vrpPaths[i]]))
        for d in data1:
            output_file.write(" & " + decorateOutput(data[vrpPaths[i], d]))
        output_file.write(" \\\\\n")

    output_file.write("\t\t\\hline\n")
    output_file.write("\t\\end{tabular}\n")
    output_file.write("\t\\caption{Vrijednosti najboljih rješenja " + alg + "}\n")
    output_file.write("\t\\label{tab:table-" + alg + "-res")
    if suffix:
        output_file.write("-" + suffix)
    elif secondRun:
        output_file.write("-2")
    output_file.write("}\n")
    output_file.write("\\end{table}\n\n")


def printSmallTableTimes(alg, data, data1, data1Headline, secondRun=False, suffix=None):
    path = "../results/" + alg + "/table_data/" + alg + ".txt"
    output_file = open(path, 'a')

    n = len(data1)

    output_file.write("\\begin{table}[ht!]\n")
    output_file.write("\t\\centering\n")
    output_file.write("\t\\rowcolors{1}{gray!25}{gray!25}\n")
    output_file.write("\t\\begin{tabular}{|c||")
    for i in range(n):
        output_file.write("c|")
    output_file.write("}\n")
    output_file.write("\t\t\\hline & \\multicolumn{" + str(n) + "}{c|}{" + data1Headline + "} \\\\\n")
    output_file.write("\t\t\\hhline{|>{\\arcg}->{\\arcb}||*{" + str(n) +
                      "}{-|}} \\multirow{-2}{*}{Instanca}")
    for d in data1:
        output_file.write(" & " + str(d))
    output_file.write(" \\\\\n")

    for i in range(3):
        m = findMin1Var(data, vrpPaths[i], data1)

        def decorateOutput(x):
            if abs(x - m) < 1E-6:
                return "\\ccw " + (str(x))
            return str(x)

        if i == 0:
            output_file.write("\t\t\\hhline{|=||")
            for j in range(n):
                output_file.write("=|")
            output_file.write("} ")
        else:
            output_file.write("\t\t\\hline ")

        output_file.write(vrpPaths[i])
        for d in data1:
            output_file.write(" & " + decorateOutput(data[vrpPaths[i], d]))
        output_file.write(" \\\\\n")

    output_file.write("\t\t\\hline\n")
    output_file.write("\t\\end{tabular}\n")
    output_file.write("\t\\caption{Prosječna duljina izvođenja " + alg + "(u $ms$)}\n")
    output_file.write("\t\\label{tab:table-" + alg + "-time")
    if suffix:
        output_file.write("-" + suffix)
    elif secondRun:
        output_file.write("-2")
    output_file.write("}\n")
    output_file.write("\\end{table}\n\n")


def printBigTableResults(alg, data, data1, data1Headline, data2, data2Headline):
    path = "../results/" + alg + "/table_data/" + alg + ".txt"
    output_file = open(path, 'w')

    n1 = len(data1)
    n2 = len(data2)

    output_file.write("\\begin{table}[ht!]\n")
    output_file.write("\t\\centering\n")
    output_file.write("\t\\begin{tabular}{|c|c||c||")
    for i in range(n2):
        output_file.write("c|")
    output_file.write("}\n")
    output_file.write("\t\t\\hline & & & \\multicolumn{" + str(n2) + "}{c|}{" + data2Headline + "} \\\\\n")
    output_file.write("\t\t\\hhline{|>{\\arcw}->{\\arcb}|>{\\arcw}->{\\arcb}||>{\\arcw}->{\\arcb}||*{" + str(len(data2))
                      + "}{-|}} \\multirow{-2}{*}{\\rotatebox{90}{Inst.}} & \\multirow{-2}{*}{\\rotatebox{90}{Opt.}}"
                        " & \\multirow{-2}{*}{" + data1Headline + "}")

    for d2 in data2:
        output_file.write(" & " + str(d2))

    output_file.write("\\\\\n")

    for vrpPath in vrpPaths:
        m = findMin2Vars(data, vrpPath, data1, data2)

        def decorateOutput(x):
            if abs(x - m) < 1E-6:
                return "\\ccg " + (decToString3(x))
            return decToString3(x)

        output_file.write("\n")

        for i in range(n1):
            if i == 0:
                output_file.write("\t\t\\hhline{|=|=||=||")
                for j in range(n2):
                    output_file.write("=|")
                output_file.write("} &")
            else:
                output_file.write("\t\t\\hhline{|~|~||-||*{" + str(n1) + "}{-|}}")
                if i != n1 - 1:
                    output_file.write(" &")
                else:
                    output_file.write(" \\multirow{-" + str(n1) + "}{*}{\\rotatebox{90}{" + vrpPath + "}} & " +
                                      "\\multirow{-" + str(n1) + "}{*}{\\rotatebox{90}{" + str(optima[vrpPath]) + "}}")

            output_file.write(" & " + str(data1[i]))

            for d2 in data2:
                output_file.write(" & " + decorateOutput(data[(vrpPath, data1[i], d2)]))
            output_file.write(" \\\\\n")

    output_file.write("\t\t\\hline\n")
    output_file.write("\t\\end{tabular}\n")
    output_file.write("\t\\caption{Vrijednosti najboljih rješenja " + alg + "}\n")
    output_file.write("\t\\label{tab:table-" + alg + "-res}\n")
    output_file.write("\\end{table}\n\n")


def printBigTableTimes(alg, data, data1, data1Headline, data2, data2Headline):
    path = "../results/" + alg + "/table_data/" + alg + ".txt"
    output_file = open(path, 'a')

    n1 = len(data1)
    n2 = len(data2)

    output_file.write("\\begin{table}[ht!]\n")
    output_file.write("\t\\centering\n")
    output_file.write("\t\\rowcolors{1}{gray!25}{gray!25}\n")
    output_file.write("\t\\begin{tabular}{|c||c||")
    for i in range(n2):
        output_file.write("c|")
    output_file.write("}\n")
    output_file.write("\t\t\\hline & & \\multicolumn{" + str(n2) + "}{c|}{" + data2Headline + "} \\\\\n")
    output_file.write("\t\t\\hhline{|>{\\arcg}->{\\arcb}||>{\\arcg}->{\\arcb}||*{" + str(len(data2)) +
                      "}{-|}} \\multirow{-2}{*}{\\rotatebox{90}{Inst.}} & \\multirow{-2}{*}{"
                      + data1Headline + "}")

    for d2 in data2:
        output_file.write(" & " + str(d2))

    output_file.write("\\\\\n")

    for vrpPath in vrpPaths:
        m = findMin2Vars(data, vrpPath, data1, data2)

        def decorateOutput(x):
            if abs(x - m) < 1E-6:
                return "\\ccw " + (str(x))
            return str(x)

        output_file.write("\n")

        for i in range(n1):
            if i == 0:
                output_file.write("\t\t\\hhline{|=||=||")
                for j in range(n2):
                    output_file.write("=|")
                output_file.write("}")
            else:
                output_file.write("\t\t\\hhline{|>{\\arcg}->{\\arcb}||-||*{" + str(n1) + "}{-|}}")
                if i == n1 - 1:
                    output_file.write(" \\multirow{-" + str(n1) + "}{*}{\\rotatebox{90}{" + vrpPath + "}}")

            output_file.write(" & " + str(data1[i]))

            for d2 in data2:
                output_file.write(" & " + decorateOutput(data[(vrpPath, data1[i], d2)]))
            output_file.write(" \\\\\n")

    output_file.write("\t\t\\hline\n")
    output_file.write("\t\\end{tabular}\n")
    output_file.write("\t\\caption{Prosječna duljina izvođenja " + alg + " (u $ms$)}\n")
    output_file.write("\t\\label{tab:table-" + alg + "-time}\n")
    output_file.write("\\end{table}\n\n")


def printGCRFTable2DResults(alg, data, data1, data2):
    path = "../results/" + alg + "/table_data/" + alg + ".txt"
    output_file = open(path, 'a')

    output_file.write("\\begin{table}[ht!]\n")
    output_file.write("\t\\centering\n")
    output_file.write("\t\\begin{tabular}{|c|c||c||*{2}{wc{\\mylen}|}}\n")
    output_file.write("\t\t\\hline & & Početna & \\multicolumn{2}{c|}{Pretraživanje susjedstva} \\\\\n")
    output_file.write("\t\t\\hhline{|~|~||~||*{2}{-|}} \\multirow{-2}{*}{Instanca} & "
                      "\\multirow{-2}{*}{Optimum} & populacija & NE & DA \\\\\n")

    for vrpPath in vrpPaths:
        m = findMin2Vars(data, vrpPath, data1, data2)

        def decorateOutput(x):
            if abs(x - m) < 1E-6:
                return "\\ccg " + (decToString3(x))
            return decToString3(x)

        output_file.write("\n")

        for mode in data1:
            if mode == "NV":
                output_file.write("\t\t\\hhline{|=|=||=||=|=|} & & " + mode)
            else:
                output_file.write("\t\t\\hhline{|~|~||-||*{2}{-|}} \\multirow{-2}{*}{" + vrpPath +
                                  "} & \\multirow{-2}{*}{" + str(optima[vrpPath]) + "} & " + mode)

            for sn in data2:
                output_file.write(" & " + decorateOutput(data[(vrpPath, mode, sn)]))
            output_file.write(" \\\\\n")

    output_file.write("\t\t\\hline\n")
    output_file.write("\t\\end{tabular}\n")
    output_file.write("\t\\caption{Vrijednosti najboljih rješenja " + alg +
                      " u ovisnosti o lokalnom pretraživanju i metodi generiranja početne populacije}\n")
    output_file.write("\t\\label{tab:table-" + alg + "-res-gcrf}\n")
    output_file.write("\\end{table}\n\n")


def printGCRFTable2DTimes(alg, data, data1, data2):
    path = "../results/" + alg + "/table_data/" + alg + ".txt"
    output_file = open(path, 'a')

    output_file.write("\\begin{table}[ht!]\n")
    output_file.write("\t\\centering\n")
    output_file.write("\t\\rowcolors{1}{gray!25}{gray!25}\n")
    output_file.write("\t\\begin{tabular}{|c|c||*{2}{wc{\\mylen}|}}\n")
    output_file.write("\t\t\\hline & Početna & \\multicolumn{2}{c|}{Pretraživanje susjedstva} \\\\\n")
    output_file.write("\t\t\\hhline{|>{\\arcg}->{\\arcb}|>{\\arcg}->{\\arcb}|*{2}{-|}} \\multirow{-2}{*}{Instanca}"
                      " & populacija & NE & DA \\\\\n")

    for vrpPath in vrpPaths:
        m = findMin2Vars(data, vrpPath, data1, data2)

        def decorateOutput(x):
            if abs(x - m) < 1E-6:
                return "\\ccw " + (str(x))
            return str(x)

        output_file.write("\n")

        for mode in data1:
            if mode == "NV":
                output_file.write("\t\t\\hhline{|=|=||=|=|} & " + mode)
            else:
                output_file.write("\t\t\\hhline{|>{\\arcg}->{\\arcb}|-||*{2}{-|}} \\multirow{-2}{*}{" + vrpPath +
                                  "} & " + mode)

            for sn in data2:
                output_file.write(" & " + decorateOutput(data[(vrpPath, mode, sn)]))
            output_file.write(" \\\\\n")

    output_file.write("\t\t\\hline\n")
    output_file.write("\t\\end{tabular}\n")
    output_file.write("\t\\caption{Prosječna duljina izvođenja " + alg +
                      " (u $ms$) u ovisnosti o lokalnom pretraživanju i metodi generiranja početne populacije}\n")
    output_file.write("\t\\label{tab:table-" + alg + "-time-gcrf}\n")
    output_file.write("\\end{table}\n\n")


def printGCRFTable1DResults(alg, data, data1, data1Headline):
    path = "../results/" + alg + "/table_data/" + alg + ".txt"
    output_file = open(path, 'a')

    output_file.write("\\begin{table}[ht!]\n")
    output_file.write("\t\\centering\n")
    output_file.write("\t\\begin{tabular}{|c||c||*{2}{wc{\\mylen}|}}\n")
    output_file.write("\t\t\\hline & & \\multicolumn{2}{c|}{" + data1Headline + "} \\\\\n")
    output_file.write("\t\t\\hhline{|~||~||*{2}{-|}} \\multirow{-2}{*}{Instanca} & "
                      "\\multirow{-2}{*}{Optimum}")
    for d in data1:
        output_file.write(" & " + d)
    output_file.write(" \\\\\n")

    for i in range(len(vrpPaths)):
        vrpPath = vrpPaths[i]
        m = findMin1Var(data, vrpPath, data1)

        def decorateOutput(x):
            if abs(x - m) < 1E-6:
                return "\\ccg " + (decToString3(x))
            return decToString3(x)

        if i == 0:
            output_file.write("\t\t\\hhline{|=||=||=|=|} ")
        else:
            output_file.write("\t\t\\hhline{|-||-||*{2}{-|}} ")

        output_file.write(vrpPath + " & " + str(optima[vrpPath]))

        for d1 in data1:
            output_file.write(" & " + decorateOutput(data[(vrpPath, d1)]))

        output_file.write(" \\\\\n")

    output_file.write("\t\t\\hline\n")
    output_file.write("\t\\end{tabular}\n")
    output_file.write("\t\\caption{Vrijednosti najboljih rješenja " + alg +
                      " u ovisnosti o lokalnom pretraživanju i metodi generiranja početne populacije}\n")
    output_file.write("\t\\label{tab:table-" + alg + "-res-gcrf}\n")
    output_file.write("\\end{table}\n\n")


def printGCRFTable1DTimes(alg, data, data1, data1Headline):
    path = "../results/" + alg + "/table_data/" + alg + ".txt"
    output_file = open(path, 'a')

    output_file.write("\\begin{table}[ht!]\n")
    output_file.write("\t\\centering\n")
    output_file.write("\t\\rowcolors{1}{gray!25}{gray!25}\n")
    output_file.write("\t\\begin{tabular}{|c||*{2}{wc{\\mylen}|}}\n")
    output_file.write("\t\t\\hline & \\multicolumn{2}{c|}{" + data1Headline + "} \\\\\n")
    output_file.write("\t\t\\hhline{|>\\arcg->\\arcb||*{2}{-|}} \\multirow{-2}{*}{Instanca}")
    for d in data1:
        output_file.write(" & " + d)
    output_file.write(" \\\\\n")

    for i in range(len(vrpPaths)):
        vrpPath = vrpPaths[i]
        m = findMin1Var(data, vrpPath, data1)

        def decorateOutput(x):
            if abs(x - m) < 1E-6:
                return "\\ccw " + str(x)
            return str(x)

        if i == 0:
            output_file.write("\t\t\\hhline{|=||=|=|} ")
        else:
            output_file.write("\t\t\\hhline{|-||*{2}{-|}} ")

        output_file.write(vrpPath)

        for d1 in data1:
            output_file.write(" & " + decorateOutput(data[(vrpPath, d1)]))

        output_file.write(" \\\\\n")

    output_file.write("\t\t\\hline\n")
    output_file.write("\t\\end{tabular}\n")
    output_file.write("\t\\caption{Prosječna duljina izvođenja " + alg +
                      " (u $ms$) u ovisnosti o lokalnom pretraživanju i metodi generiranja početne populacije}\n")
    output_file.write("\t\\label{tab:table-" + alg + "-time-gcrf}\n")
    output_file.write("\\end{table}\n\n")


def readGA():
    popSize = 100
    mrs = [0.0025, 0.005, 0.01, 0.02, 0.03]
    mrsp = [str(mrs[i] * 100) + "\\%" for i in range(len(mrs))]
    mrs_gcrf = {"A-n32-k5": 0.005,
                "A-n60-k9": 0.03,
                "A-n80-k10": 0.03}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]
    search = ["false", "true"]

    for vrpPath in vrpPaths:
        for mr in mrs:

            mr_num = decToString(mr)

            results = []
            times = []

            mrsp_curr = str(mr * 100) + "\\%"

            for i in range(1, 10 + 1):
                path = "../results/GA/experiment_data/GA_" + vrpPath.split(".")[0] + "_ps_" + str(popSize) + "_pm_" + \
                       mr_num + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBest[(vrpPath, mrsp_curr)] = min(results)
            timesAvg[(vrpPath, mrsp_curr)] = sum(times) / len(times)

        mr_num = mrs_gcrf[vrpPath]
        mr = decToString(mr_num)

        for mode in modes:
            for sn in search:

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/GA/experiment_data/GA_" + vrpPath.split(".")[0] + "_ps_" + str(popSize) + \
                           "_pm_" + mr + "_mode_" + mode + "_search_" + sn + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[1]))
                    times.append(int(lines[2].split(" ")[0]))

                penaltiesBestGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = min(results)
                timesAvgGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = sum(times) / len(times)

    printSmallTableResults(alg="GA", data=penaltiesBest, data1=mrsp, data1Headline="Vjerojatnost mutacije")
    printSmallTableTimes(alg="GA", data=timesAvg, data1=mrsp, data1Headline="Vjerojatnost mutacije")

    printGCRFTable2DResults(alg="GA", data=penaltiesBestGCRF, data1=[gcrfDict[mode] for mode in modes],
                            data2=[trueFalseDict[s] for s in search])
    printGCRFTable2DTimes(alg="GA", data=timesAvgGCRF, data1=[gcrfDict[mode] for mode in modes],
                          data2=[trueFalseDict[s] for s in search])


def readDE():
    fs = [0.1, 0.25, 0.5, 1, 1.5, 2]
    crs = [0.1, 0.25, 0.5, 0.75, 0.9, 1]
    fs_gcrf = {"A-n32-k5": 0.25,
               "A-n60-k9": 0.25,
               "A-n80-k10": 0.5}
    crs_gcrf = {"A-n32-k5": 0.25,
                "A-n60-k9": 0.9,
                "A-n80-k10": 0.1}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]
    search = ["false", "true"]

    for vrpPath in vrpPaths:
        for f_num in fs:
            for cr_num in crs:
                f = decToString(f_num)
                cr = decToString(cr_num)

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/DE/experiment_data/DE_" + vrpPath.split(".")[0] + "_f_" + f + \
                           "_cr_" + cr + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[2]))
                    times.append(int(lines[3].split(" ")[0]))

                penaltiesBest[(vrpPath, f_num, cr_num)] = min(results)
                timesAvg[(vrpPath, f_num, cr_num)] = sum(times) / len(times)

        f_num = fs_gcrf[vrpPath]
        f = decToString(f_num)
        cr_num = crs_gcrf[vrpPath]
        cr = decToString(cr_num)

        for mode in modes:
            for sn in search:

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/DE/experiment_data/DE_" + vrpPath.split(".")[0] + "_f_" + f + \
                           "_cr_" + cr + "_mode_" + mode + "_search_" + sn + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[2]))
                    times.append(int(lines[3].split(" ")[0]))

                penaltiesBestGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = min(results)
                timesAvgGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = sum(times) / len(times)

    printBigTableResults(alg="DE", data=penaltiesBest, data1=fs, data2=crs, data1Headline="F", data2Headline="CR")
    printBigTableTimes(alg="DE", data=timesAvg, data1=fs, data2=crs, data1Headline="F", data2Headline="CR")

    printGCRFTable2DResults(alg="DE", data=penaltiesBestGCRF, data1=[gcrfDict[mode] for mode in modes],
                            data2=[trueFalseDict[s] for s in search])
    printGCRFTable2DTimes(alg="DE", data=timesAvgGCRF, data1=[gcrfDict[mode] for mode in modes],
                          data2=[trueFalseDict[s] for s in search])


def readBSA():
    fs = [0.1, 0.25, 0.5, 1, 1.5, 2]
    fs_gcrf = {"A-n32-k5": 1.5,
               "A-n60-k9": 1,
               "A-n80-k10": 1}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]
    search = ["false", "true"]

    for vrpPath in vrpPaths:
        for f_num in fs:
            f = decToString(f_num)

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/BSA/experiment_data/BSA_" + vrpPath.split(".")[0] + "_f_" + f + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[2]))
                times.append(int(lines[3].split(" ")[0]))

            penaltiesBest[(vrpPath, f_num)] = min(results)
            timesAvg[(vrpPath, f_num)] = sum(times) / len(times)

        f_num = fs_gcrf[vrpPath]
        f = decToString(f_num)

        for mode in modes:
            for sn in search:

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/BSA/experiment_data/BSA_" + vrpPath.split(".")[0] + "_f_" + str(f) + \
                           "_mode_" + mode + "_search_" + sn + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[2]))
                    times.append(int(lines[3].split(" ")[0]))

                penaltiesBestGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = min(results)
                timesAvgGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = sum(times) / len(times)

    printSmallTableResults(alg="BSA", data=penaltiesBest, data1=fs, data1Headline="F")
    printSmallTableTimes(alg="BSA", data=timesAvg, data1=fs, data1Headline="F")

    printGCRFTable2DResults(alg="BSA", data=penaltiesBestGCRF, data1=[gcrfDict[mode] for mode in modes],
                            data2=[trueFalseDict[s] for s in search])
    printGCRFTable2DTimes(alg="BSA", data=timesAvgGCRF, data1=[gcrfDict[mode] for mode in modes],
                          data2=[trueFalseDict[s] for s in search])


def readPSO():
    cls = [1, 1.5, 2, 3]
    cgs = [1, 1.5, 2, 3]
    ws = [0.1, 0.2, 0.4, 0.5, 0.75, 1]

    cls_gcrf = {"A-n32-k5": 2,
                "A-n60-k9": 2,
                "A-n80-k10": 3}

    cgs_gcrf = {"A-n32-k5": 2,
                "A-n60-k9": 3,
                "A-n80-k10": 1}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBest2ndRun = {}
    timesAvg2ndRun = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]
    search = ["false", "true"]

    for vrpPath in vrpPaths:
        for cl_num in cls:
            for cg_num in cgs:
                cl = decToString(cl_num)
                cg = decToString(cg_num)

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/PSO/experiment_data/PSO_" + vrpPath.split(".")[0] + "_cl_" + cl + \
                           "_cg_" + cg + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[2]))
                    times.append(int(lines[3].split(" ")[0]))

                penaltiesBest[(vrpPath, cl_num, cg_num)] = min(results)
                timesAvg[(vrpPath, cl_num, cg_num)] = sum(times) / len(times)
        ##############################################
        cl_num = cls_gcrf[vrpPath]
        cg_num = cgs_gcrf[vrpPath]

        cl = decToString(cl_num)
        cg = decToString(cg_num)

        for w_num in ws:
            w = decToString(w_num)

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/PSO/experiment_data/PSO_" + vrpPath.split(".")[0] + "_cl_" + cl + \
                       "_cg_" + cg + "_w_" + w + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[2]))
                times.append(int(lines[3].split(" ")[0]))

            penaltiesBest2ndRun[(vrpPath, w_num)] = min(results)
            timesAvg2ndRun[(vrpPath, w_num)] = sum(times) / len(times)
        ##############################################
        cl_num = cls_gcrf[vrpPath]
        cg_num = cgs_gcrf[vrpPath]

        cl = decToString(cl_num)
        cg = decToString(cg_num)

        for mode in modes:
            for sn in search:

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/PSO/experiment_data/PSO_" + vrpPath.split(".")[0] + "_cl_" + str(cl) + "_cg_" + \
                           str(cg) + "_mode_" + mode + "_search_" + sn + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[2]))
                    times.append(int(lines[3].split(" ")[0]))

                penaltiesBestGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = min(results)
                timesAvgGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = sum(times) / len(times)

    printBigTableResults(alg="PSO", data=penaltiesBest, data1=cls,
                         data1Headline="$c_l$", data2=cgs, data2Headline="$c_g$")
    printBigTableTimes(alg="PSO", data=timesAvg, data1=cls, data1Headline="$c_l$", data2=cgs, data2Headline="$c_g$")

    printSmallTableResults(alg="PSO", data=penaltiesBest2ndRun, data1=ws, data1Headline="w", secondRun=True)
    printSmallTableTimes(alg="PSO", data=timesAvg2ndRun, data1=ws, data1Headline="w", secondRun=True)

    printGCRFTable2DResults(alg="PSO", data=penaltiesBestGCRF, data1=[gcrfDict[mode] for mode in modes],
                            data2=[trueFalseDict[s] for s in search])
    printGCRFTable2DTimes(alg="PSO", data=timesAvgGCRF, data1=[gcrfDict[mode] for mode in modes],
                          data2=[trueFalseDict[s] for s in search])


def readACO():
    alphas = [0.5, 1, 2, 3.5, 5]
    betas = [0.5, 1, 2, 3.5, 5]
    gammas = [0.5, 1, 2, 3, 5]
    gamma_num = 3
    g = decToString(gamma_num)

    alphas_gcrf = {"A-n32-k5": 1,
                   "A-n60-k9": 1,
                   "A-n80-k10": 1}

    betas_gcrf = {"A-n32-k5": 1,
                  "A-n60-k9": 0.5,
                  "A-n80-k10": 1}

    gammas_gcrf = {"A-n32-k5": 0.5,
                   "A-n60-k9": 3,
                   "A-n80-k10": 0.5}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBest2ndRun = {}
    timesAvg2ndRun = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    search = ["false", "true"]

    for vrpPath in vrpPaths:
        for alpha_num in alphas:
            for beta_num in betas:
                alpha = decToString(alpha_num)
                beta = decToString(beta_num)

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/ACO/experiment_data/ACO_" + vrpPath.split(".")[0] + "_a_" + alpha + \
                           "_b_" + beta + "_g_" + g + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[1]))
                    times.append(int(lines[2].split(" ")[0]))

                penaltiesBest[(vrpPath, alpha_num, beta_num)] = min(results)
                timesAvg[(vrpPath, alpha_num, beta_num)] = sum(times) / len(times)
        #############################################
        alpha_num = alphas_gcrf[vrpPath]
        beta_num = betas_gcrf[vrpPath]

        alpha = decToString(alpha_num)
        beta = decToString(beta_num)
        for gamma_num in gammas:
            gamma = decToString(gamma_num)

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/ACO/experiment_data/ACO_" + vrpPath.split(".")[0] + "_a_" + alpha + \
                       "_b_" + beta + "_g_" + gamma + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBest2ndRun[(vrpPath, gamma_num)] = min(results)
            timesAvg2ndRun[(vrpPath, gamma_num)] = sum(times) / len(times)
        ####################################################
        alpha_num = alphas_gcrf[vrpPath]
        beta_num = betas_gcrf[vrpPath]
        gamma_num = gammas_gcrf[vrpPath]

        alpha = decToString(alpha_num)
        beta = decToString(beta_num)
        gamma = decToString(gamma_num)

        for sn in search:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/ACO/experiment_data/ACO_" + vrpPath.split(".")[0] + "_a_" + str(alpha) + "_b_" \
                       + str(beta) + "_g_" + str(gamma) + "_search_" + sn + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBestGCRF[(vrpPath, trueFalseDict[sn])] = min(results)
            timesAvgGCRF[(vrpPath, trueFalseDict[sn])] = sum(times) / len(times)

    printBigTableResults(alg="ACO", data=penaltiesBest, data1=alphas, data1Headline="$\\alpha$",
                         data2=betas, data2Headline="$\\beta$")
    printBigTableTimes(alg="ACO", data=timesAvg, data1=alphas, data1Headline="$\\alpha$",
                       data2=betas, data2Headline="$\\beta$")

    printSmallTableResults(alg="ACO", data=penaltiesBest2ndRun, data1=gammas, data1Headline="$\\gamma$", secondRun=True)
    printSmallTableTimes(alg="ACO", data=timesAvg2ndRun, data1=gammas, data1Headline="$\\gamma$", secondRun=True)

    printGCRFTable1DResults(alg="ACO", data=penaltiesBestGCRF, data1=[trueFalseDict[s] for s in search],
                            data1Headline="Pretraživanje susjedstva")
    printGCRFTable1DTimes(alg="ACO", data=timesAvgGCRF, data1=[trueFalseDict[s] for s in search],
                          data1Headline="Pretraživanje susjedstva")


def readABC():
    popSizes = [10, 20, 30, 50, 80, 100]

    popSizes_gcrf = {"A-n32-k5": 10,
                     "A-n60-k9": 80,
                     "A-n80-k10": 80}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]

    for vrpPath in vrpPaths:
        for popSize in popSizes:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/ABC/experiment_data/ABC_" + vrpPath.split(".")[0] + "_ps_" + str(popSize) + "_" + \
                       str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBest[(vrpPath, popSize)] = min(results)
            timesAvg[(vrpPath, popSize)] = sum(times) / len(times)

        popSize = popSizes_gcrf[vrpPath]

        for mode in modes:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/ABC/experiment_data/ABC_" + vrpPath.split(".")[0] + "_ps_" + str(popSize) + \
                       "_mode_" + mode + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBestGCRF[(vrpPath, gcrfDict[mode])] = min(results)
            timesAvgGCRF[(vrpPath, gcrfDict[mode])] = sum(times) / len(times)

    printSmallTableResults(alg="ABC", data=penaltiesBest, data1=popSizes, data1Headline="Veličina populacije")
    printSmallTableTimes(alg="ABC", data=timesAvg, data1=popSizes, data1Headline="Veličina populacije")

    printGCRFTable1DResults(alg="ABC", data=penaltiesBestGCRF, data1=[gcrfDict[mode] for mode in modes],
                            data1Headline="Početna populacija")
    printGCRFTable1DTimes(alg="ABC", data=timesAvgGCRF, data1=[gcrfDict[mode] for mode in modes],
                          data1Headline="Početna populacija")


def readTS():
    tabuTenures = [1, 2, 5, 10, 20, 50]

    tabuTenures_gcrf = {"A-n32-k5": 20,
                        "A-n60-k9": 5,
                        "A-n80-k10": 2}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]

    for vrpPath in vrpPaths:
        for tabuTenure in tabuTenures:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/TS/experiment_data/TS_" + vrpPath.split(".")[0] + "_tt_" + str(tabuTenure) + "_" + \
                       str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBest[(vrpPath, tabuTenure)] = min(results)
            timesAvg[(vrpPath, tabuTenure)] = sum(times) / len(times)

        tabuTenure = tabuTenures_gcrf[vrpPath]

        for mode in modes:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/TS/experiment_data/TS_" + vrpPath.split(".")[0] + "_tt_" + str(tabuTenure) + \
                       "_mode_" + mode + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBestGCRF[(vrpPath, gcrfDict[mode])] = min(results)
            timesAvgGCRF[(vrpPath, gcrfDict[mode])] = sum(times) / len(times)

    printSmallTableResults(alg="TS", data=penaltiesBest, data1=tabuTenures, data1Headline="Veličina tabu liste")
    printSmallTableTimes(alg="TS", data=timesAvg, data1=tabuTenures, data1Headline="Veličina tabu liste")

    printGCRFTable1DResults(alg="TS", data=penaltiesBestGCRF, data1=[gcrfDict[mode] for mode in modes],
                            data1Headline="Početno rješenje")
    printGCRFTable1DTimes(alg="TS", data=timesAvgGCRF, data1=[gcrfDict[mode] for mode in modes],
                          data1Headline="Početno rješenje")


def readSA():
    decrements = ["lin", "geom", "vslow"]
    coefficients = [(0.005, 0.01, 0.05, 0.1), (0.99, 0.999, 0.9999, 0.99995), (0.001, 0.005, 0.01, 0.05)]
    headlines = ["B", "$\\beta$", "$\\alpha$"]
    alternativeSuffixes = [" uz linearni dekrement temperature", " uz geometrijski dekrement temperature",
                           " uz vrlo spor dekrement temperature"]

    decrements_gcrf = {"A-n32-k5": "vslow",
                       "A-n60-k9": "geom",
                       "A-n80-k10": "geom"}

    coefficients_gcrf = {"A-n32-k5": 0.01,
                         "A-n60-k9": 0.9999,
                         "A-n80-k10": 0.99995}

    penaltiesBestList = []
    timesAvgList = []
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]

    for ind in range(3):
        decrement = decrements[ind]
        coefs = coefficients[ind]

        penaltiesBest = {}
        timesAvg = {}

        for vrpPath in vrpPaths:

            for coef_num in coefs:
                coef = decToString(coef_num)

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/SA/experiment_data/SA_" + vrpPath.split(".")[0] + "_decF_" + decrement + \
                           "_coef_" + coef + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[1]))
                    times.append(int(lines[2].split(" ")[0]))

                penaltiesBest[(vrpPath, coef_num)] = min(results)
                timesAvg[(vrpPath, coef_num)] = sum(times) / len(times)

            penaltiesBestList.append(penaltiesBest)
            timesAvgList.append(timesAvg)

        printSmallTableResults(alg="SA", data=penaltiesBest, data1=coefs, data1Headline=headlines[ind],
                               secondRun=(True if ind != 0 else False), suffix=decrements[ind])
        printSmallTableTimes(alg="SA", data=timesAvg, data1=coefs, data1Headline=headlines[ind],
                             secondRun=(True if ind != 0 else False), suffix=decrements[ind])

    for vrpPath in vrpPaths:
        decrement = decrements_gcrf[vrpPath]
        coef = decToString(coefficients_gcrf[vrpPath])

        for mode in modes:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/SA/experiment_data/SA_" + vrpPath.split(".")[0] + "_decF_" + decrement + \
                       "_coef_" + coef + "_mode_" + mode + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBestGCRF[(vrpPath, gcrfDict[mode])] = min(results)
            timesAvgGCRF[(vrpPath, gcrfDict[mode])] = sum(times) / len(times)

    printGCRFTable1DResults(alg="SA", data=penaltiesBestGCRF, data1=[gcrfDict[mode] for mode in modes],
                            data1Headline="Početno rješenje")
    printGCRFTable1DTimes(alg="SA", data=timesAvgGCRF, data1=[gcrfDict[mode] for mode in modes],
                          data1Headline="Početno rješenje")


def readGAThreads():
    timesAvg = {}
    noThreads = ["1", "02", "04", "08", "16", "24"]
    noThreadsDict = {"1": 1, "02": 2, "04": 4, "08": 8, "16": 16, "24": 24}

    for noThreadsString in noThreads:
        times = []

        for i in range(1, 10 + 1):
            path = "../results/GA/experiment_data/thread_count/GA_" + \
                   str(noThreadsString) + "_threads_" + str(i) + ".txt"
            input_file = open(path, 'r')

            lines = input_file.read().splitlines()

            times.append(int(lines[2].split(" ")[0]))

        timesAvg[noThreadsString] = sum(times) / len(times)

    output_file = open("../results/GA/plot_data/threads/GA_threads.txt", 'w')

    for noThreadsString in noThreads:
        output_file.write(str(noThreadsDict[noThreadsString]) + " " + str(timesAvg[noThreadsString]) + "\n")


if __name__ == '__main__':
    # readGA()
    # readDE()
    # readBSA()
    # readPSO()
    # readACO()
    # readABC()
    # readTS()
    # readSA()
    readGAThreads()
