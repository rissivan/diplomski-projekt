from builtins import sum

optima = {"A-n32-k5.vrp": 787.808,
          "A-n60-k9.vrp": 1355.799,
          "A-n80-k10.vrp": 1766.5}

gcrfDict = {"GC": "PP",
            "RF": "NV"}

trueFalseDict = {"true": "DA",
                 "false": "NE"}


def decToString(d):
    return f'{d:.6f}'


def decToString3(d):
    return f'{d:.3f}'


def findMin2Vars(data, vrpPath, data1, data2):
    m = data[(vrpPath, data1[0], data2[0])]
    for d1 in data1:
        for d2 in data2:
            t = data[(vrpPath, d1, d2)]
            if t < m:
                m = t
    return m


def findMin1Var(data, vrpPath, data1):
    m = data[(vrpPath, data1[0])]
    for d1 in data1:
        t = data[(vrpPath, d1)]
        if t < m:
            m = t
    return m


def printTable1Var(alg, data, vrpPath, data1, data1Headline, mode, gcrf=False, suffix=None, alternativeSuffix=None):
    m = findMin1Var(data, vrpPath, data1)

    time = (mode == "time")

    path = "../results/" + alg + "/table_data/" + alg + "_" + vrpPath.split(".")[0] + "_table_data_1var" + \
           (("_" + suffix) if suffix else "") + ("_gcrf" if gcrf else "") + ".txt"
    output_file = open(path, ('a' if time else 'w'))

    caption = ("Prosječna duljina izvođenja (u $ms$) " if time else "Najbolje vrijednosti evaluacijske funkcije ") + \
              alg + (alternativeSuffix if alternativeSuffix else "") + " na instanci " + vrpPath + "."

    def decorateOutput(x):
        if (mode == "res" and (abs(x - m) < 1E-6)) or (mode == "time" and x == m):
            return "\\cellcolor" + ("{white}" if time else "{green!25}") + \
                   (decToString3(x) if (mode == "res") else str(x))
        return decToString3(x) if mode == "res" else str(x)

    output_file.write("\\begin{table}[ht!]\n")
    output_file.write("\t\\centering\n")
    if time:
        output_file.write("\t\\rowcolors{1}{gray!25}{gray!25}\n")

    output_file.write("\t\\begin{tabular}{|c||")
    for i in range(len(data1)):
        output_file.write("c|")
    output_file.write("}\n")

    output_file.write("\t\t\\hline " + ("" if time else "Optimum: ") + "& \\multicolumn{" + str(len(data1)) + "}{c|}{" +
                      data1Headline + "} \\\\\n")
    output_file.write("\t\t\\hhline{|>{\\arrayrulecolor{" + ("gray!25" if time else "white") +
                      "}}->{\\arrayrulecolor{black}}||*{" + str(len(data1)) + "}{-|}} ")
    output_file.write("" if time else str(optima[vrpPath]))
    for d1 in data1:
        output_file.write(" & " + str(d1))
    output_file.write(" \\\\\n")
    output_file.write("\t\t\\hhline{|=||")
    for i in range(len(data1)):
        output_file.write("=|")
    output_file.write("} " + ("duljina izvođenja [\\texttt{ms}]" if time else "vrijednost EF"))
    for d1 in data1:
        output_file.write(" & " + decorateOutput(data[(vrpPath, d1)]))
    output_file.write(" \\\\\n")

    output_file.write("\t\t\\hline\n")
    output_file.write("\t\\end{tabular}\n")
    output_file.write("\t\\caption{" + caption + "}\n")
    output_file.write("\t\\label{tab:table-" + alg + "-" + vrpPath.split("-")[1] + "-" +
                      ((suffix + "-") if suffix else "") + mode + ("-gcrf" if gcrf else "") + "}\n")
    output_file.write("\\end{table}\n")

    output_file.write("\n")


def printTable2Vars(alg, data, vrpPath, data1, data2, data1Headline, data2Headline, mode, gcrf=False, rotate=True):
    m = findMin2Vars(data, vrpPath, data1, data2)

    time = (mode == "time")

    path = "../results/" + alg + "/table_data/" + alg + "_" + vrpPath.split(".")[0] + "_table_data_2vars" + \
           ("_gcrf" if gcrf else "") + ".txt"
    output_file = open(path, ('a' if time else 'w'))

    caption = ("Prosječna duljina izvođenja (u $ms$) " if time else "Najbolje vrijednosti evaluacijske funkcije ") \
              + alg + " na instanci " + vrpPath + "."

    def decorateOutput(x):
        if (mode == "res" and (abs(x - m) < 1E-6)) or (mode == "time" and x == m):
            return "\\cellcolor" + ("{white}" if time else "{green!25}") + \
                   (decToString3(x) if (mode == "res") else str(x))
        return decToString3(x) if mode == "res" else str(x)

    def decorateRotation(x):
        if rotate:
            return "\\rotatebox{90}{" + x + "}"
        return x

    output_file.write("\\begin{table}[ht!]\n")
    output_file.write("\t\\centering\n")
    if time:
        output_file.write("\t\\rowcolors{1}{gray!25}{gray!25}\n")

    output_file.write("\t\\begin{tabular}{|c|c||")
    for i in range(len(data1)):
        output_file.write("c|")
    output_file.write("}\n")

    output_file.write("\t\t\\hline \\multicolumn{2}{|c||}{" + ("" if time else "Optimum:") + "} & \\multicolumn{" + str(
        len(data2)) + "}{c|}{" + data2Headline + "} \\\\\n")
    output_file.write("\t\t\\hhline{|" + (">{\\arrayrulecolor{gray!25}}-->{\\arrayrulecolor{black}}" if time else "~~")
                      + "||*{" + str(len(data2)) + "}{-|}} \\multicolumn{2}{|c||}{" + (
                          "" if time else str(optima[vrpPath])) + "}")
    for d2 in data2:
        output_file.write(" & " + str(d2))
    output_file.write(" \\\\\n")
    output_file.write("\t\t\\hhline{|=|=||")
    for i in range(len(data2)):
        output_file.write("=|")
    output_file.write("}")
    for d1 in data1:
        if d1 != data1[0]:
            output_file.write(
                "\t\t\\hhline{|" + (">{\\arrayrulecolor{gray!25}}->{\\arrayrulecolor{black}}" if time else "~") +
                "|-||*{" + str(len(data2)) + "}{-|}}")
        if d1 == data1[-1]:
            output_file.write("\\multirow{" + str(-len(data1)) + "}{*}{" + decorateRotation(data1Headline) + "}")
        output_file.write(" & " + str(d1) + " & ")
        for d2 in data2:
            output_file.write(decorateOutput(data[(vrpPath, d1, d2)]))
            if d2 != data2[-1]:
                output_file.write(" & ")
            else:
                output_file.write(" \\\\\n")
    output_file.write("\t\t\\hline\n")
    output_file.write("\t\\end{tabular}\n")
    output_file.write("\t\\caption{" + caption + "}\n")
    output_file.write("\t\\label{tab:table-" + alg + "-" + vrpPath.split("-")[1] + "-" + mode +
                      ("-gcrf" if gcrf else "") + "}\n")
    output_file.write("\\end{table}\n")
    output_file.write("\n")


def readGA():
    vrpPaths = ["A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"]
    popSize = 100
    mrs = [0.0025, 0.005, 0.01, 0.02, 0.03]
    mrs_gcrf = {"A-n32-k5.vrp": 0.005,
                "A-n60-k9.vrp": 0.03,
                "A-n80-k10.vrp": 0.03}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]
    search = ["false", "true"]

    for vrpPath in vrpPaths:
        for mr_num in mrs:
            mr = decToString(mr_num)

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/GA/experiment_data/GA_" + vrpPath.split(".")[0] + "_ps_" + str(popSize) + "_pm_" + \
                       mr + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBest[(vrpPath, mr_num)] = min(results)
            timesAvg[(vrpPath, mr_num)] = sum(times) / len(times)

        printTable1Var(alg="GA", data=penaltiesBest, vrpPath=vrpPath, data1=mrs, data1Headline="Vjerojatnost mutacije",
                       mode="res")
        printTable1Var(alg="GA", data=timesAvg, vrpPath=vrpPath, data1=mrs, data1Headline="Vjerojatnost mutacije",
                       mode="time")

        mr_num = mrs_gcrf[vrpPath]
        mr = decToString(mr_num)

        for mode in modes:
            for sn in search:

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/GA/experiment_data/GA_" + vrpPath.split(".")[0] + "_ps_" + str(popSize) + \
                           "_pm_" + mr + "_mode_" + mode + "_search_" + sn + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[1]))
                    times.append(int(lines[2].split(" ")[0]))

                penaltiesBestGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = min(results)
                timesAvgGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = sum(times) / len(times)

        printTable2Vars(alg="GA", data=penaltiesBestGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                        data1Headline="Metoda generiranja početnog rješenja", data2=[trueFalseDict[s] for s in search],
                        data2Headline="Pretraživanje susjedstva", mode="res", gcrf=True, rotate=False)
        printTable2Vars(alg="GA", data=timesAvgGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                        data1Headline="Metoda generiranja početnog rješenja", data2=[trueFalseDict[s] for s in search],
                        data2Headline="Pretraživanje susjedstva", mode="time", gcrf=True, rotate=False)


def readDE():
    vrpPaths = ["A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"]
    fs = [0.1, 0.25, 0.5, 1, 1.5, 2]
    crs = [0.1, 0.25, 0.5, 0.75, 0.9, 1]
    fs_gcrf = {"A-n32-k5.vrp": 0.25,
               "A-n60-k9.vrp": 0.25,
               "A-n80-k10.vrp": 0.5}
    crs_gcrf = {"A-n32-k5.vrp": 0.25,
                "A-n60-k9.vrp": 0.9,
                "A-n80-k10.vrp": 0.1}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]
    search = ["false", "true"]

    for vrpPath in vrpPaths:
        for f_num in fs:
            for cr_num in crs:
                f = decToString(f_num)
                cr = decToString(cr_num)

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/DE/experiment_data/DE_" + vrpPath.split(".")[0] + "_f_" + f + \
                           "_cr_" + cr + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[2]))
                    times.append(int(lines[3].split(" ")[0]))

                penaltiesBest[(vrpPath, f_num, cr_num)] = min(results)
                timesAvg[(vrpPath, f_num, cr_num)] = sum(times) / len(times)

        printTable2Vars(alg="DE", data=penaltiesBest, vrpPath=vrpPath, data1=fs, data2=crs, data1Headline="F",
                        data2Headline="CR", mode="res")
        printTable2Vars(alg="DE", data=timesAvg, vrpPath=vrpPath, data1=fs, data2=crs, data1Headline="F",
                        data2Headline="CR", mode="time")

        f_num = fs_gcrf[vrpPath]
        f = decToString(f_num)
        cr_num = crs_gcrf[vrpPath]
        cr = decToString(cr_num)

        for mode in modes:
            for sn in search:

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/DE/experiment_data/DE_" + vrpPath.split(".")[0] + "_f_" + f + \
                           "_cr_" + cr + "_mode_" + mode + "_search_" + sn + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[2]))
                    times.append(int(lines[3].split(" ")[0]))

                penaltiesBestGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = min(results)
                timesAvgGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = sum(times) / len(times)

        printTable2Vars(alg="DE", data=penaltiesBestGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                        data1Headline="Metoda generiranja početnog rješenja", data2=[trueFalseDict[s] for s in search],
                        data2Headline="Pretraživanje susjedstva", mode="res", gcrf=True, rotate=False)
        printTable2Vars(alg="DE", data=timesAvgGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                        data1Headline="Metoda generiranja početnog rješenja", data2=[trueFalseDict[s] for s in search],
                        data2Headline="Pretraživanje susjedstva", mode="time", gcrf=True, rotate=False)


def readBSA():
    vrpPaths = ["A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"]
    fs = [0.1, 0.25, 0.5, 1, 1.5, 2]
    fs_gcrf = {"A-n32-k5.vrp": 1.5,
               "A-n60-k9.vrp": 1,
               "A-n80-k10.vrp": 1}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]
    search = ["false", "true"]

    for vrpPath in vrpPaths:
        for f_num in fs:
            f = decToString(f_num)

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/BSA/experiment_data/BSA_" + vrpPath.split(".")[0] + "_f_" + f + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[2]))
                times.append(int(lines[3].split(" ")[0]))

            penaltiesBest[(vrpPath, f_num)] = min(results)
            timesAvg[(vrpPath, f_num)] = sum(times) / len(times)

        printTable1Var(alg="BSA", data=penaltiesBest, vrpPath=vrpPath, data1=fs, data1Headline="F",
                       mode="res")
        printTable1Var(alg="BSA", data=timesAvg, vrpPath=vrpPath, data1=fs, data1Headline="F",
                       mode="time")

        f_num = fs_gcrf[vrpPath]
        f = decToString(f_num)

        for mode in modes:
            for sn in search:

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/BSA/experiment_data/BSA_" + vrpPath.split(".")[0] + "_f_" + str(f) + \
                           "_mode_" + mode + "_search_" + sn + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[2]))
                    times.append(int(lines[3].split(" ")[0]))

                penaltiesBestGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = min(results)
                timesAvgGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = sum(times) / len(times)

        printTable2Vars(alg="BSA", data=penaltiesBestGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                        data1Headline="Metoda generiranja početnog rješenja", data2=[trueFalseDict[s] for s in search],
                        data2Headline="Pretraživanje susjedstva", mode="res", gcrf=True, rotate=False)
        printTable2Vars(alg="BSA", data=timesAvgGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                        data1Headline="Metoda generiranja početnog rješenja", data2=[trueFalseDict[s] for s in search],
                        data2Headline="Pretraživanje susjedstva", mode="time", gcrf=True, rotate=False)


def readPSO():
    vrpPaths = ["A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"]
    cls = [1, 1.5, 2, 3]
    cgs = [1, 1.5, 2, 3]

    cls_gcrf = {"A-n32-k5.vrp": 2,
                "A-n60-k9.vrp": 2,
                "A-n80-k10.vrp": 3}

    cgs_gcrf = {"A-n32-k5.vrp": 2,
                "A-n60-k9.vrp": 3,
                "A-n80-k10.vrp": 1}

    # ws_gcrf = {"A-n32-k5.vrp": 0.2,
    #            "A-n60-k9.vrp": 0.4,
    #            "A-n80-k10.vrp": 0.2}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]
    search = ["false", "true"]

    for vrpPath in vrpPaths:
        for cl_num in cls:
            for cg_num in cgs:
                cl = decToString(cl_num)
                cg = decToString(cg_num)

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/PSO/experiment_data/PSO_" + vrpPath.split(".")[0] + "_cl_" + cl + \
                           "_cg_" + cg + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[2]))
                    times.append(int(lines[3].split(" ")[0]))

                penaltiesBest[(vrpPath, cl_num, cg_num)] = min(results)
                timesAvg[(vrpPath, cl_num, cg_num)] = sum(times) / len(times)

        printTable2Vars(alg="PSO", data=penaltiesBest, vrpPath=vrpPath, data1=cls, data2=cgs, data1Headline="CL",
                        data2Headline="CG", mode="res")
        printTable2Vars(alg="PSO", data=timesAvg, vrpPath=vrpPath, data1=cls, data2=cgs, data1Headline="CL",
                        data2Headline="CG", mode="time")

        cl_num = cls_gcrf[vrpPath]
        cg_num = cgs_gcrf[vrpPath]
        # w_num = ws_gcrf[vrpPath]

        cl = decToString(cl_num)
        cg = decToString(cg_num)
        # w = decToString(w_num)

        for mode in modes:
            for sn in search:

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/PSO/experiment_data/PSO_" + vrpPath.split(".")[0] + "_cl_" + str(cl) + "_cg_" + \
                           str(cg) + "_mode_" + mode + "_search_" + sn + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[2]))
                    times.append(int(lines[3].split(" ")[0]))

                penaltiesBestGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = min(results)
                timesAvgGCRF[(vrpPath, gcrfDict[mode], trueFalseDict[sn])] = sum(times) / len(times)

        printTable2Vars(alg="PSO", data=penaltiesBestGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                        data1Headline="Metoda generiranja početnog rješenja", data2=[trueFalseDict[s] for s in search],
                        data2Headline="Pretraživanje susjedstva", mode="res", gcrf=True, rotate=False)
        printTable2Vars(alg="PSO", data=timesAvgGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                        data1Headline="Metoda generiranja početnog rješenja", data2=[trueFalseDict[s] for s in search],
                        data2Headline="Pretraživanje susjedstva", mode="time", gcrf=True, rotate=False)


def readACO():
    vrpPaths = ["A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"]
    alphas = [0.5, 1, 2, 3.5, 5]
    betas = [0.5, 1, 2, 3.5, 5]
    gamma_num = 3
    g = decToString(gamma_num)

    alphas_gcrf = {"A-n32-k5.vrp": 1,
                   "A-n60-k9.vrp": 1,
                   "A-n80-k10.vrp": 1}

    betas_gcrf = {"A-n32-k5.vrp": 1,
                  "A-n60-k9.vrp": 0.5,
                  "A-n80-k10.vrp": 1}

    gammas_gcrf = {"A-n32-k5.vrp": 0.5,
                   "A-n60-k9.vrp": 3,
                   "A-n80-k10.vrp": 0.5}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    search = ["false", "true"]

    for vrpPath in vrpPaths:
        for alpha_num in alphas:
            for beta_num in betas:
                alpha = decToString(alpha_num)
                beta = decToString(beta_num)

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/ACO/experiment_data/ACO_" + vrpPath.split(".")[0] + "_a_" + alpha + \
                           "_b_" + beta + "_g_" + g + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[1]))
                    times.append(int(lines[2].split(" ")[0]))

                penaltiesBest[(vrpPath, alpha_num, beta_num)] = min(results)
                timesAvg[(vrpPath, alpha_num, beta_num)] = sum(times) / len(times)

        printTable2Vars(alg="ACO", data=penaltiesBest, vrpPath=vrpPath, data1=alphas, data2=betas,
                        data1Headline="$\\alpha$",
                        data2Headline="$\\beta$", mode="res")
        printTable2Vars(alg="ACO", data=timesAvg, vrpPath=vrpPath, data1=alphas, data2=betas, data1Headline="$\\alpha$",
                        data2Headline="$\\beta$", mode="time")

        alpha_num = alphas_gcrf[vrpPath]
        beta_num = betas_gcrf[vrpPath]
        gamma_num = gammas_gcrf[vrpPath]

        alpha = decToString(alpha_num)
        beta = decToString(beta_num)
        gamma = decToString(gamma_num)

        for sn in search:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/ACO/experiment_data/ACO_" + vrpPath.split(".")[0] + "_a_" + str(alpha) + "_b_" \
                       + str(beta) + "_g_" + str(gamma) + "_search_" + sn + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBestGCRF[(vrpPath, trueFalseDict[sn])] = min(results)
            timesAvgGCRF[(vrpPath, trueFalseDict[sn])] = sum(times) / len(times)

        printTable1Var(alg="ACO", data=penaltiesBestGCRF, vrpPath=vrpPath, data1=[trueFalseDict[s] for s in search],
                       data1Headline="Pretraživanje susjedstva", mode="res", gcrf=True)
        printTable1Var(alg="ACO", data=timesAvgGCRF, vrpPath=vrpPath, data1=[trueFalseDict[s] for s in search],
                       data1Headline="Pretraživanje susjedstva", mode="time", gcrf=True)


def readABC():
    vrpPaths = ["A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"]
    popSizes = [10, 20, 30, 50, 80, 100]

    popSizes_gcrf = {"A-n32-k5.vrp": 10,
                     "A-n60-k9.vrp": 80,
                     "A-n80-k10.vrp": 80}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]

    for vrpPath in vrpPaths:
        for popSize in popSizes:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/ABC/experiment_data/ABC_" + vrpPath.split(".")[0] + "_ps_" + str(popSize) + "_" + \
                       str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBest[(vrpPath, popSize)] = min(results)
            timesAvg[(vrpPath, popSize)] = sum(times) / len(times)

        printTable1Var(alg="ABC", data=penaltiesBest, vrpPath=vrpPath, data1=popSizes,
                       data1Headline="Veličina populacije", mode="res")
        printTable1Var(alg="ABC", data=timesAvg, vrpPath=vrpPath, data1=popSizes, data1Headline="Veličina populacije",
                       mode="time")

        popSize = popSizes_gcrf[vrpPath]

        for mode in modes:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/ABC/experiment_data/ABC_" + vrpPath.split(".")[0] + "_ps_" + str(popSize) + \
                       "_mode_" + mode + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBestGCRF[(vrpPath, gcrfDict[mode])] = min(results)
            timesAvgGCRF[(vrpPath, gcrfDict[mode])] = sum(times) / len(times)

        printTable1Var(alg="ABC", data=penaltiesBestGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                       data1Headline="Metoda generiranja početnog rješenja", mode="res", gcrf=True)
        printTable1Var(alg="ABC", data=timesAvgGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                       data1Headline="Metoda generiranja početnog rješenja", mode="time", gcrf=True)


def readTS():
    vrpPaths = ["A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"]
    tabuTenures = [1, 2, 5, 10, 20, 50]

    tabuTenures_gcrf = {"A-n32-k5.vrp": 20,
                        "A-n60-k9.vrp": 5,
                        "A-n80-k10.vrp": 2}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]

    for vrpPath in vrpPaths:
        for tabuTenure in tabuTenures:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/TS/experiment_data/TS_" + vrpPath.split(".")[0] + "_tt_" + str(tabuTenure) + "_" + \
                       str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBest[(vrpPath, tabuTenure)] = min(results)
            timesAvg[(vrpPath, tabuTenure)] = sum(times) / len(times)

        printTable1Var(alg="TS", data=penaltiesBest, vrpPath=vrpPath, data1=tabuTenures,
                       data1Headline="Veličina tabu liste", mode="res")
        printTable1Var(alg="TS", data=timesAvg, vrpPath=vrpPath, data1=tabuTenures,
                       data1Headline="Veličina tabu liste", mode="time")

        tabuTenure = tabuTenures_gcrf[vrpPath]

        for mode in modes:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/TS/experiment_data/TS_" + vrpPath.split(".")[0] + "_tt_" + str(tabuTenure) + \
                       "_mode_" + mode + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBestGCRF[(vrpPath, gcrfDict[mode])] = min(results)
            timesAvgGCRF[(vrpPath, gcrfDict[mode])] = sum(times) / len(times)

        printTable1Var(alg="TS", data=penaltiesBestGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                       data1Headline="Metoda generiranja početnog rješenja", mode="res", gcrf=True)
        printTable1Var(alg="TS", data=timesAvgGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                       data1Headline="Metoda generiranja početnog rješenja", mode="time", gcrf=True)


def readSA():
    vrpPaths = ["A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"]
    decrements = ["lin", "geom", "vslow"]
    coefficients = [(0.005, 0.01, 0.05, 0.1), (0.99, 0.999, 0.9999, 0.99995), (0.001, 0.005, 0.01, 0.05)]
    headlines = ["B", "$\\beta$", "$\\alpha$"]
    alternativeSuffixes = [" uz linearni dekrement temperature", " uz geometrijski dekrement temperature",
                           " uz vrlo spor dekrement temperature"]

    decrements_gcrf = {"A-n32-k5.vrp": "vslow",
                       "A-n60-k9.vrp": "geom",
                       "A-n80-k10.vrp": "geom"}

    coefficients_gcrf = {"A-n32-k5.vrp": 0.01,
                         "A-n60-k9.vrp": 0.9999,
                         "A-n80-k10.vrp": 0.99995}

    penaltiesBest = {}
    timesAvg = {}
    penaltiesBestGCRF = {}
    timesAvgGCRF = {}

    modes = ["RF", "GC"]

    for vrpPath in vrpPaths:
        for ind in range(3):
            decrement = decrements[ind]
            coefs = coefficients[ind]

            data = []

            for coef_num in coefs:
                coef = decToString(coef_num)

                results = []
                times = []

                for i in range(1, 10 + 1):
                    path = "../results/SA/experiment_data/SA_" + vrpPath.split(".")[0] + "_decF_" + decrement + \
                           "_coef_" + coef + "_" + str(i) + ".txt"
                    input_file = open(path, 'r')

                    lines = input_file.read().splitlines()

                    results.append(float(lines[1]))
                    times.append(int(lines[2].split(" ")[0]))

                data.append((decrement, coef_num))
                penaltiesBest[(vrpPath, (decrement, coef_num))] = min(results)
                timesAvg[(vrpPath, (decrement, coef_num))] = sum(times) / len(times)

            printTable1Var(alg="SA", data=penaltiesBest, vrpPath=vrpPath, data1=data,
                           data1Headline=headlines[ind], mode="res", suffix=decrement,
                           alternativeSuffix=alternativeSuffixes[ind])
            printTable1Var(alg="SA", data=timesAvg, vrpPath=vrpPath, data1=data,
                           data1Headline=headlines[ind], mode="time", suffix=decrement,
                           alternativeSuffix=alternativeSuffixes[ind])

        decrement = decrements_gcrf[vrpPath]
        coef = decToString(coefficients_gcrf[vrpPath])

        for mode in modes:

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/SA/experiment_data/SA_" + vrpPath.split(".")[0] + "_decF_" + decrement + \
                       "_coef_" + coef + "_mode_" + mode + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBestGCRF[(vrpPath, gcrfDict[mode])] = min(results)
            timesAvgGCRF[(vrpPath, gcrfDict[mode])] = sum(times) / len(times)

        printTable1Var(alg="SA", data=penaltiesBestGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                       data1Headline="Metoda generiranja početnog rješenja", mode="res", gcrf=True)
        printTable1Var(alg="SA", data=timesAvgGCRF, vrpPath=vrpPath, data1=[gcrfDict[mode] for mode in modes],
                       data1Headline="Metoda generiranja početnog rješenja", mode="time", gcrf=True)


def readACO2ndRun():
    configs = [("A-n32-k5.vrp", 1, 1), ("A-n60-k9.vrp", 1, 0.5), ("A-n80-k10.vrp", 1, 1)]
    gammas = [0.5, 1, 2, 3, 5]

    penaltiesBest = {}
    timesAvg = {}

    for config in configs:
        vrpPath = config[0]

        alpha = decToString(config[1])
        beta = decToString(config[2])
        for gamma_num in gammas:
            gamma = decToString(gamma_num)

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/ACO/experiment_data/ACO_" + vrpPath.split(".")[0] + "_a_" + alpha + \
                       "_b_" + beta + "_g_" + gamma + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[1]))
                times.append(int(lines[2].split(" ")[0]))

            penaltiesBest[(vrpPath, gamma_num)] = min(results)
            timesAvg[(vrpPath, gamma_num)] = sum(times) / len(times)

        printTable1Var(alg="ACO", data=penaltiesBest, vrpPath=vrpPath, data1=gammas, data1Headline="$\\gamma$",
                       mode="res")
        printTable1Var(alg="ACO", data=timesAvg, vrpPath=vrpPath, data1=gammas, data1Headline="$\\gamma$", mode="time")


def readPSO2ndRun():
    configs = [("A-n32-k5.vrp", 2, 2), ("A-n60-k9.vrp", 2, 3), ("A-n80-k10.vrp", 3, 1)]
    ws = [0.1, 0.2, 0.4, 0.5, 0.75, 1]

    penaltiesBest = {}
    timesAvg = {}

    for config in configs:
        vrpPath = config[0]
        cl_num = config[1]
        cg_num = config[2]
        for w_num in ws:
            cl = decToString(cl_num)
            cg = decToString(cg_num)
            w = decToString(w_num)

            results = []
            times = []

            for i in range(1, 10 + 1):
                path = "../results/PSO/experiment_data/PSO_" + vrpPath.split(".")[0] + "_cl_" + cl + \
                       "_cg_" + cg + "_w_" + w + "_" + str(i) + ".txt"
                input_file = open(path, 'r')

                lines = input_file.read().splitlines()

                results.append(float(lines[2]))
                times.append(int(lines[3].split(" ")[0]))

            penaltiesBest[(vrpPath, w_num)] = min(results)
            timesAvg[(vrpPath, w_num)] = sum(times) / len(times)

        printTable1Var(alg="PSO", data=penaltiesBest, vrpPath=vrpPath, data1=ws, data1Headline="w", mode="res")
        printTable1Var(alg="PSO", data=timesAvg, vrpPath=vrpPath, data1=ws, data1Headline="w", mode="time")


if __name__ == '__main__':
    # readGA()
    # readDE()
    # readBSA()
    # readPSO()
    # readACO()
    # readABC()
    # readTS()
    readSA()
    # readACO2ndRun()
    # readPSO2ndRun()
