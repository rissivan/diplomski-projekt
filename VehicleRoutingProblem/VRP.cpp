//
// Created by ivan on 11/3/21.
//

#include <fstream>
#include <iostream>
#include <algorithm>
#include <cmath>
#include "VRP.h"
#include "../Util/Util.h"


using namespace std;

struct VRP::coordinate{
    double x, y;
    coordinate()= default;;
    coordinate(double x, double y){
        this->x = x;
        this->y = y;
    }

    friend ostream &operator<<(ostream &os, const coordinate &coordinate) {
        os << "(" << coordinate.x << ", " << coordinate.y << ")";
        return os;
    }

    double operator*(const coordinate &rhs) const {
        double dx = this->x - rhs.x;
        double dy = this->y - rhs.y;
        return sqrt(dx * dx + dy * dy);
    }
};

VRP::VRP(const string& path){
    parseInput(path);
//    this->engine = mt19937_64(this->rd());
//    this->engine = mt19937_64(3);
    //ignore the depot city
    this->intDist = uniform_int_distribution<int>(0, getPhenotypeSize());
//    this->dist_01 = uniform_real_distribution<double>(0, 1);
}

//dovrsiti ovaj dio s mijenjanjem enginea.

VRP::~VRP() = default;

const vector<vector<double>>& VRP::getDistances() {
    return distances;
}

void VRP::parseInput(const string &path) {
    ifstream input_file(path);
    string line;

    if(input_file){
        while(getline(input_file, line)){
            if(line == "EOF")
                break;
            if(line.find("NAME") != string::npos){
                string name = util::trim(util::split(line, " : ")[1]);
                vector<string> tokens = util::split(name, "-");
                for(const string& token: tokens){
                    if (token[0] == 'k'){
                        this->noVehicles = stoi(token.substr(1));
                    }
                }
            } else if (line.find("DIMENSION") != string::npos){
                this->dimension = stoi(util::trim(util::split(line, " : ")[1]));
            } else if (line.find("CAPACITY") != string::npos){
                this->capacity = stoi(util::trim(util::split(line, " : ")[1]));
            } else if (line.find("NODE_COORD_SECTION") != string::npos){
                for(int i = 0; i < this->dimension; i++){
                    getline(input_file, line);
                    vector<string> tokens = util::split(line, " ");
                    this->coordinates.emplace_back(stod(tokens[1]), stod(tokens[2]));
                }
                this->distances.reserve(this->dimension);
                for(coordinate first: this->coordinates){
                    vector<double> row;
                    row.reserve(this->dimension);
                    for (coordinate second: this->coordinates) {
                        row.push_back(first * second);
                    }
                    distances.push_back(row);
                }
            } else if (line.find("DEMAND_SECTION") != string::npos){
                this->demands.reserve(this->dimension);
                for(int i = 0; i < this->dimension; i++){
                    getline(input_file, line);
                    vector<string> tokens = util::split(line, " ");
                    this->demands.push_back(stoi(tokens[1]));
                }
            } else if (line.find("DEPOT_SECTION") != string::npos){
                while(line.find("-1") != string::npos){
                    getline(input_file, line);
                    this->depotIndex = stoi(line) - 1;
                }
            }
        }
    }
}

int VRP::getDimension() const {
    return dimension;
}

int VRP::getNoVehicles() const {
    return noVehicles;
}

const vector<int> &VRP::getDemands() const {
    return demands;
}

int VRP::getCapacity() const {
    return capacity;
}

//
//const mt19937_64 &VRP::getEngine() const {
//    return engine;
//}

//const uniform_int_distribution<int> &VRP::getIntDist() const {
//    return intDist;
//}
//
//const uniform_real_distribution<double> &VRP::getDist01() const {
//    return dist_01;
//}

int VRP::getInt() {
    return this->intDist(util::getEngine());
}

int VRP::getInt(int i1, int i2) {
    return static_cast<int>(lround(i1 + (i2 - i1) * getReal01()));
}

double VRP::getReal01() {
    return util::getDist01()(util::getEngine());
}


void VRP::shuffleVector(vector<int> &v) {
    shuffle(v.begin(), v.end(), util::getEngine());
}

void VRP::shuffleSubvector(std::vector<int>::iterator begin, std::vector<int>::iterator end) {
    shuffle(begin, end, util::getEngine());
}

int VRP::getPhenotypeSize() const {
    return dimension + noVehicles - 1;
}

double VRP::calculateDistanceFromFile(const string &path) {
    ifstream input_file(path);
    double dist = 0;
    while(input_file){
        string line;
        getline(input_file, line);
        if(line.starts_with('C'))
            break;
        vector<string> indices = util::split(util::split(line, ": ")[1], " ");
        auto size = indices.size();
        dist += distances[0][stoi(indices[0])];
        for(int i = 0; i < size - 1; i++){
            dist += distances[stoi(indices[i])][stoi(indices[i + 1])];
        }
        dist += distances[stoi(indices[size - 1])][0];
    }
    return dist;
}


