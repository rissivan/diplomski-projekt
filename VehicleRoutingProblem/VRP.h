//
// Created by ivan on 11/3/21.
//

#ifndef PROJEKT_VRP_H
#define PROJEKT_VRP_H

#include <vector>
#include <string>
#include <random>

class VRP {
private:
    struct coordinate;
    std::vector<coordinate> coordinates;
    std::vector<std::vector<double>> distances;
    std::vector<int> demands;
    int dimension{};
    int noVehicles{};
    int capacity{};
    //presumes 1
    int depotIndex{};
//    std::random_device& rd;
//    std::mt19937_64 engine;
    std::uniform_int_distribution<int> intDist;
//    std::uniform_real_distribution<double> dist_01;
    void parseInput(const std::string& path);
public:
    explicit VRP(const std::string& path);
    ~VRP();
    const std::vector<std::vector<double>>& getDistances();
    [[nodiscard]] int getDimension() const;
    [[nodiscard]] int getNoVehicles() const;
    [[nodiscard]] const std::vector<int> &getDemands() const;
    [[nodiscard]] int getCapacity() const;
    [[nodiscard]] int getPhenotypeSize() const;
    int getInt();
    static double getReal01();
    static int getInt(int i1, int i2);
    static void shuffleVector(std::vector<int>& v);
    static void shuffleSubvector(std::vector<int>::iterator begin, std::vector<int>::iterator end);
    double calculateDistanceFromFile(const std::string& path);
};


#endif //PROJEKT_VRP_H
