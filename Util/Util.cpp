//
// Created by ivan on 11/3/21.
//

#include "Util.h"

std::string util::trim(const std::string &str) {
    size_t first = str.find_first_not_of(' ');
    if (std::string::npos == first)
    {
        return str;
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

std::vector<std::string> util::split(const std::string &str, const std::string &delimiter) {
    std::vector<std::string> tokens;
    size_t previous = 0, position = 0;
    size_t len = str.length();
    do{
        position = str.find(delimiter, previous);
        if (position == std::string::npos)
            position = str.length();
        std::string token = str.substr(previous, position - previous);
        if(!token.empty())
            tokens.push_back(token);
        previous = position + delimiter.length();
    } while (position < len && previous < len);
    return tokens;
}

std::random_device& util::getRandomDevice() {
    static std::random_device rd;
    return rd;
}

std::mt19937_64& util::getEngine() {
    static std::mt19937_64 engine(getRandomDevice()());
    return engine;
}

std::uniform_real_distribution<double>& util::getDist01() {
    static std::uniform_real_distribution<double> dist01(0, 1);
    return dist01;
}