//
// Created by ivan on 11/3/21.
//

#ifndef PROJEKT_UTIL_H
#define PROJEKT_UTIL_H

#include <string>
#include <vector>
#include <random>

namespace util{
    std::string trim(const std::string& str);
    std::vector<std::string> split(const std::string& str, const std::string& delimiter);
    std::random_device& getRandomDevice();
    std::mt19937_64& getEngine();
    std::uniform_real_distribution<double>& getDist01();
}

#endif //PROJEKT_UTIL_H
