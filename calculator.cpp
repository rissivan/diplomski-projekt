//
// Created by ivan on 4/28/22.
//
#include <vector>
#include <iostream>
#include <fstream>
#include <map>
#include <iomanip>
#include "Phenotype/BasePhenotype.h"
#include "Util/Util.h"

using namespace std;

map<string, double> optima({{"A-n32-k5.vrp",  787.808},
                         {"A-n60-k9.vrp",  1355.799},
                         {"A-n80-k10.vrp", 1766.5}});


const string RES_DESC = "vrijednost evaluacijske funkcije";
const string TIME_DESC = "duljina izvođenja [\\texttt{ms}]";

enum tableType{
    RESULTS,
    TIME
};

enum alg{
    GA,
    DE,
    BSA,
    PSO,
    ACO,
    ABC,
    TS,
    SA
};

template<typename T>
T calculateAvg(vector<T> v){
    T sum = 0;
    for(T n: v){
        sum += n;
    }
    return sum / v.size();
}

template<typename T>
T calculateMin(vector<T> v){
    T minimum = v[0];
    for(T n: v){
        if(n < minimum){
            minimum = n;
        }
    }
    return minimum;
}

double getAbs(double x){
    if(x < 0)
        return -x;
    return x;
}

double getMin(map<tuple<string, string, int, double>, double>& m, const string& instance, const string& mode){
    double min = numeric_limits<double>::max();
    for(auto & it : m){
        if(get<0>(it.first) == instance && get<1>(it.first) == mode && it.second < min){
            min = it.second;
        }
    }
    return min;
}

void decorateSingleRowTable(ofstream& outputFile, const string& alg, tableType resultOrTime, const string& instance, const string& tableData, const string& headline, int columns){
    outputFile << "\\begin{table}[ht!]" << endl;
    outputFile << "\t\\centering" << endl;
    if(resultOrTime == TIME){
        outputFile << "\t\\rowcolors{1}{gray!25}{gray!25}" << endl;
    }
    outputFile << "\t\\begin{tabular}{|c||";
    for(int i = 0; i < columns; i++){
        outputFile << "c|";
    }
    outputFile << "}" << endl;

    outputFile << "\t\t\\hline " << ((resultOrTime == RESULTS) ? "Optimum: " : "") << "& \\multicolumn{" + to_string(columns) + "}{c|}{"+ headline +"}\\\\" << endl;
    outputFile << tableData << endl;
    outputFile << "\t\\end{tabular}" << endl;
    outputFile << "\t\\caption{";
    outputFile << (resultOrTime == RESULTS ? "Najbolji rezultati " : "Prosječna duljina izvođenja (u $ms$) ");
    outputFile << alg + " na instanci \\texttt{"+ instance +"}.}" << endl;
    outputFile << "\t\\label{tab:table-" + alg + "-" + util::split(instance, "-")[1] + "-" + ((resultOrTime == RESULTS) ? "res" : "time") + "}" << endl;
    outputFile << "\\end{table}" << endl;
}

void calculateGA(){
    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});

    vector<int> popSizes({100});
    vector<double> mutationRates({0.0025, 0.005, 0.01, 0.02, 0.03});
//    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE});
    vector<bool> searchNeighbourhood({false});


    for(const auto& vrpPath: vrpPaths){
        ofstream outputFile("../results/GA/table_data/GA_" + util::split(util::split(vrpPath, "/")[3], ".")[0] + "_data" + ".txt");
//        outputFile << "=======================" << endl;
//        outputFile << vrpPath << endl;
//        outputFile << "=======================" << endl << endl;

        for(int popSize: popSizes) {
            for (double mr: mutationRates) {
                for (auto mode: constructionModes) {
                    for (bool sn: searchNeighbourhood) {
                        vector<double> penalties;
                        vector<int> times;

                        for (int i = 1; i <= 10; i++) {
                            string inputFilePath = "../results/GA/experiment_data/GA_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                    "_ps_" + to_string(popSize) + "_pm_" + to_string(mr) + "_" + to_string(i) + ".txt";
                            ifstream inputFile(inputFilePath);
                            string line;

                            getline(inputFile, line);
                            getline(inputFile, line);
                            penalties.push_back(stod(line));

                            getline(inputFile, line);
                            times.push_back(stoi(util::split(line, " ")[0]));

                            inputFile.close();
                        }
                        outputFile << popSize << " " << mr << " " << (mode == BasePhenotype::GUIDED_CLASSIC ? "GC" : "RF") << " " << (sn ? "true" : "false") <<
                        " " << calculateAvg(penalties) << " " << calculateMin(penalties) << " " << calculateAvg(times) << "ms." << endl;
                    }
                }
            }
        }
        outputFile << endl;
        outputFile.flush();
        outputFile.close();
    }

}

void calculateABC(){
    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});

    vector<int> popSizes({5, 10, 20, 30, 50, 100});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    ofstream outputFile("../results/ABC_calculated.txt");

    for(const auto& vrpPath: vrpPaths) {
        outputFile << "=======================" << endl;
        outputFile << vrpPath << endl;
        outputFile << "=======================" << endl << endl;

        for (int popSize: popSizes) {
            for (auto mode: constructionModes) {
                vector<double> penalties;
                vector<int> times;

                for (int i = 1; i <= 5; i++) {

                    string inputFilePath = "../results/ABC/ABC_" + util::split(util::split(vrpPath, "/")[3], ".")[0] + "_ps_" + to_string(popSize) +
                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") + "_" + to_string(i) + ".txt";

                    ifstream inputFile(inputFilePath);
                    string line;

                    getline(inputFile, line);
                    getline(inputFile, line);
                    penalties.push_back(stod(line));

                    getline(inputFile, line);
                    times.push_back(stoi(util::split(line, " ")[0]));

                    inputFile.close();
                }

                outputFile << popSize << " " << (mode == BasePhenotype::GUIDED_CLASSIC ? "GC" : "RF") << " " << calculateAvg(penalties)
                            << " " << calculateMin(penalties) << " " << calculateAvg(times) << "ms." << endl;
            }
        }
        outputFile << endl;
    }
    outputFile.close();
}

void calculateTS(){
    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});

    vector<int> tabuTenures({1, 2, 5, 10, 20, 50});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    ofstream outputFile("../results/TS_calculated.txt");

    for(const auto& vrpPath: vrpPaths) {
        outputFile << "=======================" << endl;
        outputFile << vrpPath << endl;
        outputFile << "=======================" << endl << endl;

        for (int tabuTenure: tabuTenures) {
            for (auto mode: constructionModes) {
                vector<double> penalties;
                vector<int> times;

                for (int i = 1; i <= 5; i++) {

                    string inputFilePath = "../results/TS/TS_" + util::split(util::split(vrpPath, "/")[3], ".")[0] + "_tt_" + to_string(tabuTenure)
                                            + "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") + "_" + to_string(i) + ".txt";

                    ifstream inputFile(inputFilePath);
                    string line;

                    getline(inputFile, line);
                    getline(inputFile, line);
                    penalties.push_back(stod(line));

                    getline(inputFile, line);
                    times.push_back(stoi(util::split(line, " ")[0]));

                    inputFile.close();
                }

                outputFile << tabuTenure << " " << (mode == BasePhenotype::GUIDED_CLASSIC ? "GC" : "RF") << " " << calculateAvg(penalties)
                     << " " << calculateMin(penalties) << " " << calculateAvg(times) << "ms." << endl;
            }
        }
        outputFile << endl;
    }
    outputFile.close();
}

void writeTableGA(){
    map<tuple<string, string, int, double>, double> penaltiesAvg;
    map<tuple<string, string, int, double>, double> penaltiesBest;
    map<tuple<string, string, int, double>, int> timesAvg;

    vector<string> instances({"A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"});
    vector<string> modes({"RF"});
    vector<int> popSizes({100});
    vector<double> mutationRates({0.0025, 0.005, 0.01, 0.02, 0.03});

    for(const string& vrpPath: instances){

        ifstream inputFile("../results/GA/table_data/GA_" + util::split(vrpPath, ".")[0] + "_data" + ".txt");
        ofstream outputFile("../results/GA/table_data/GA_" + util::split(vrpPath, ".")[0] + "_table_data" + ".txt");
        ofstream outputFilePlot("../results/GA/plot_data/GA_" + util::split(vrpPath, ".")[0] + "_plot_data" + ".txt");
        outputFile << fixed << setprecision(3);
        string line;
        string currentInstance = vrpPath;
        while(!inputFile.eof()){
            getline(inputFile, line);

            if(line[0] == '=' || line.empty()){
                continue;
            } else if (line[0] == '.') {
                currentInstance = util::split(line, "/")[3];
                continue;
            }

            auto tokens = util::split(line, " ");
            penaltiesAvg[tuple<string, string, int, double>(currentInstance, tokens[2], stoi(tokens[0]), stod(tokens[1]))] = stod(tokens[4]);
            penaltiesBest[tuple<string, string, int, double>(currentInstance, tokens[2], stoi(tokens[0]), stod(tokens[1]))] = stod(tokens[5]);
            timesAvg[tuple<string, string, int, double>(currentInstance, tokens[2], stoi(tokens[0]), stod(tokens[1]))] = stoi(util::split(tokens[6], "ms")[0]);
        }

        stringstream strstr;

        //results

        for(const string& mode: modes){
            double m = getMin(penaltiesBest, vrpPath, mode);
            outputFile << endl << endl << vrpPath << " " << mode << endl << endl;
            strstr << "\t\t\\cline{" << 2 << "-" << mutationRates.size() + 1 << "}" << optima[currentInstance] << " & ";
            for(double mr: mutationRates){
                strstr << mr << (mr == *mutationRates.rbegin() ? " \\\\" : " & ");
            }
            strstr << endl;
            strstr << "\t\t\\hhline{|=||";
            for(int i = 0; i < mutationRates.size(); i++){
                strstr << "=|";
            }
            strstr << "} ";
            for(int popSize: popSizes){
                strstr << RES_DESC << " & ";
                for(double mr: mutationRates){
                    double res = penaltiesBest[tuple<string, string, int, double>(vrpPath, mode, popSize, mr)];
                    strstr << ((getAbs(m - res) < 1E-6) ? "\\cellcolor{green!25}" : "") << res << (mr == *mutationRates.rbegin() ? " \\\\" : " & ");
                }
                strstr << endl << "\t\t\\hline";
            }

            decorateSingleRowTable(outputFile, "GA", RESULTS, vrpPath, strstr.str(), "Vjerojatnost mutacije", static_cast<int>(mutationRates.size()));

            strstr.str(string());

            //times

            outputFile << endl << endl;
            strstr << "\t\t\\hhline{|>{\\arrayrulecolor{gray!25}}-|>{\\arrayrulecolor{black}}|*{" << mutationRates.size() << "}{-|}}";
            strstr << " & ";
            for(double mr: mutationRates){
                strstr << mr << (mr == *mutationRates.rbegin() ? " \\\\" : " & ");
            }
            strstr << endl;
            strstr << "\t\t\\hhline{|=||";
            for(int i = 0; i < mutationRates.size(); i++){
                strstr << "=|";
            }
            strstr << "} ";
//            strstr << "\t\t\\hline ";
            for(int popSize: popSizes){
                strstr << TIME_DESC << " & ";
                for(double mr: mutationRates){
                    strstr << timesAvg[tuple<string, string, int, double>(vrpPath, mode, popSize, mr)] << (mr == *mutationRates.rbegin() ? " \\\\" : " & ");
                }
                strstr << endl << "\t\t\\hline";
            }

            decorateSingleRowTable(outputFile, "GA", TIME, vrpPath, strstr.str(), "Vjerojatnost mutacije", static_cast<int>(mutationRates.size()));

            //plot data

            for(int popSize: popSizes){
                for(double mr: mutationRates){
                    outputFilePlot << mr << " " << penaltiesBest[tuple<string, string, int, double>(vrpPath, mode, popSize, mr)] << endl;
                }
            }

        }
    }

}

void writeTableABC() {
    ifstream inputFile("../results/ABC_calculated.txt");
    map<tuple<string, string, int>, double> penaltiesAvg;
    map<tuple<string, string, int>, double> penaltiesBest;
    map<tuple<string, string, int>, int> timesAvg;

    vector<string> instances({"A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"});
    vector<string> modes({"GC", "RF"});
    vector<int> popSizes({5, 10, 20, 30, 50, 100});

    string line;
    string currentInstance;
    while (!inputFile.eof()) {
        getline(inputFile, line);

        if (line[0] == '=' || line.empty()) {
            continue;
        } else if (line[0] == '.') {
            currentInstance = util::split(line, "/")[3];
            continue;
        }

        auto tokens = util::split(line, " ");
        penaltiesAvg[tuple<string, string, int>(currentInstance, tokens[1], stoi(tokens[0]))] = stod(tokens[2]);
        penaltiesBest[tuple<string, string, int>(currentInstance, tokens[1], stoi(tokens[0]))] = stod(tokens[3]);
        timesAvg[tuple<string, string, int>(currentInstance, tokens[1], stoi(tokens[0]))] = stoi(util::split(tokens[4], "ms")[0]);

    }
    cout << "=================" << endl;
    cout << "Average penalties" << endl;
    cout << "=================" << endl << endl;

    for(const string& inst: instances){
        cout << endl << endl << inst <<  endl << endl << "\\hline & ";
        for(int popSize: popSizes){
            cout << popSize << (popSize == *popSizes.rbegin() ? " \\\\" : " & ");
        }
        for(const string& mode: modes){
            cout << endl;
            cout << "\\hline " << mode << " & ";
            for(int popSize: popSizes){
                cout << penaltiesAvg[tuple<string, string, int>(inst, mode, popSize)] << (popSize == *popSizes.rbegin() ? " \\\\" : " & ");
            }
        }
    }
    cout << endl;

    cout << "=================" << endl;
    cout << "Minimal penalties" << endl;
    cout << "=================" << endl << endl;

    for(const string& inst: instances){
        cout << endl << endl << inst <<  endl << endl << "\\hline & ";
        for(int popSize: popSizes){
            cout << popSize << (popSize == *popSizes.rbegin() ? " \\\\" : " & ");
        }
        for(const string& mode: modes){
            cout << endl;
            cout << "\\hline " << mode << " & ";
            for(int popSize: popSizes){
                cout << penaltiesBest[tuple<string, string, int>(inst, mode, popSize)] << (popSize == *popSizes.rbegin() ? " \\\\" : " & ");
            }
        }
    }
    cout << endl;

    cout << "=============" << endl;
    cout << "Average times" << endl;
    cout << "=============" << endl << endl;

    for(const string& inst: instances){
        cout << endl << endl << inst <<  endl << endl << "\\hline & ";
        for(int popSize: popSizes){
            cout << popSize << (popSize == *popSizes.rbegin() ? " \\\\" : " & ");
        }
        for(const string& mode: modes){
            cout << endl;
            cout << "\\hline " << mode << " & ";
            for(int popSize: popSizes){
                cout << timesAvg[tuple<string, string, int>(inst, mode, popSize)] << (popSize == *popSizes.rbegin() ? " \\\\" : " & ");
            }
        }
    }
    cout << endl;
}

void writeTableTS() {
    ifstream inputFile("../results/TS_calculated.txt");
    map<tuple<string, string, int>, double> penaltiesAvg;
    map<tuple<string, string, int>, double> penaltiesBest;
    map<tuple<string, string, int>, int> timesAvg;

    vector<string> instances({"A-n32-k5.vrp", "A-n60-k9.vrp", "A-n80-k10.vrp"});
    vector<string> modes({"GC", "RF"});
    vector<int> tabuTenures({1, 2, 5, 10, 20, 50});

    string line;
    string currentInstance;
    while (!inputFile.eof()) {
        getline(inputFile, line);

        if (line[0] == '=' || line.empty()) {
            continue;
        } else if (line[0] == '.') {
            currentInstance = util::split(line, "/")[3];
            continue;
        }

        auto tokens = util::split(line, " ");
        penaltiesAvg[tuple<string, string, int>(currentInstance, tokens[1], stoi(tokens[0]))] = stod(tokens[2]);
        penaltiesBest[tuple<string, string, int>(currentInstance, tokens[1], stoi(tokens[0]))] = stod(tokens[3]);
        timesAvg[tuple<string, string, int>(currentInstance, tokens[1], stoi(tokens[0]))] = stoi(util::split(tokens[4], "ms")[0]);

    }
    cout << "=================" << endl;
    cout << "Average penalties" << endl;
    cout << "=================" << endl << endl;

    for(const string& inst: instances){
        cout << endl << endl << inst <<  endl << endl << "\\hline & ";
        for(int tabuTenure: tabuTenures){
            cout << tabuTenure << (tabuTenure == *tabuTenures.rbegin() ? " \\\\" : " & ");
        }
        for(const string& mode: modes){
            cout << endl;
            cout << "\\hline " << mode << " & ";
            for(int tabuTenure: tabuTenures){
                cout << penaltiesAvg[tuple<string, string, int>(inst, mode, tabuTenure)] << (tabuTenure == *tabuTenures.rbegin() ? " \\\\" : " & ");
            }
        }
    }
    cout << endl;

    cout << "=================" << endl;
    cout << "Minimal penalties" << endl;
    cout << "=================" << endl << endl;

    for(const string& inst: instances){
        cout << endl << endl << inst <<  endl << endl << "\\hline & ";
        for(int tabuTenure: tabuTenures){
            cout << tabuTenure << (tabuTenure == *tabuTenures.rbegin() ? " \\\\" : " & ");
        }
        for(const string& mode: modes){
            cout << endl;
            cout << "\\hline " << mode << " & ";
            for(int tabuTenure: tabuTenures){
                cout << penaltiesBest[tuple<string, string, int>(inst, mode, tabuTenure)] << (tabuTenure == *tabuTenures.rbegin() ? " \\\\" : " & ");
            }
        }
    }
    cout << endl;

    cout << "=============" << endl;
    cout << "Average times" << endl;
    cout << "=============" << endl << endl;

    for(const string& inst: instances){
        cout << endl << endl << inst <<  endl << endl << "\\hline & ";
        for(int tabuTenure: tabuTenures){
            cout << tabuTenure << (tabuTenure == *tabuTenures.rbegin() ? " \\\\" : " & ");
        }
        for(const string& mode: modes){
            cout << endl;
            cout << "\\hline " << mode << " & ";
            for(int tabuTenure: tabuTenures){
                cout << timesAvg[tuple<string, string, int>(inst, mode, tabuTenure)] << (tabuTenure == *tabuTenures.rbegin() ? " \\\\" : " & ");
            }
        }
    }
    cout << endl;
}


int main(){
    cout << fixed << setprecision(3);
    calculateGA();
    writeTableGA();
}