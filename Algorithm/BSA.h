//
// Created by ivan on 12/29/21.
//

#ifndef PROJEKT_BSA_H
#define PROJEKT_BSA_H

#include "AbstractAlgorithm.h"
#include "../Phenotype/RKRealPhenotype.h"

class BSA: public AbstractAlgorithm{
private:
    std::vector<std::shared_ptr<RKRealPhenotype>> population;
    std::vector<std::shared_ptr<RKRealPhenotype>> historicalPopulation;
    std::shared_ptr<RKRealPhenotype> bestSolution;
    int noGenerations = 100000;
    int populationSize = 80;
    double f = 0.1;
    bool normalize = false;
    void selectionI();
    std::shared_ptr<RKRealPhenotype> mutation(int index);
    std::shared_ptr<RKRealPhenotype> crossover(int index, const std::shared_ptr<RKRealPhenotype>& mutant);
    std::shared_ptr<RKRealPhenotype> selectionII(int index, const std::shared_ptr<RKRealPhenotype>& trial);
    std::shared_ptr<RKRealPhenotype> createAndEvaluate(int index);
public:
    explicit BSA(VRP& vrp);
    void run() override;
    void printBest(std::ostream &os) override;
    void setF(double f);
};


#endif //PROJEKT_BSA_H
