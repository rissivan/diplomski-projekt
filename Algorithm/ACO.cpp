//
// Created by ivan on 12/31/21.
//

#include "ACO.h"

#include <iostream>
#include <set>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/future.hpp>

using namespace std;


ACO::ACO(VRP &vrp) : AbstractAlgorithm(vrp) {

}

void ACO::run() {
    this->distances = vrp.getDistances();
    this->demands = vrp.getDemands();

    auto size = distances.size();

    this->pheromones.reserve(size);
    this->heuristics.reserve(size);
    this->savings.reserve(size);

    for(int i = 0; i < size; i++){
        vector<double> p(size);
        vector<double> h(size);
        vector<double> s(size);
        for(int j = 0; j < size; j++){
            if(i == j){
                p[j] = 0;
                h[j] = 0;
                s[j] = 0;
            } else {
                p[j] = initialPheromone;
                h[j] = (double) 1 / distances[i][j];
                if(i == 0 || j == 0)
                    s[j] = 1;
                else
                    s[j] = distances[i][0] + distances[0][j] - distances[i][j];
            }
        }
        pheromones.push_back(p);
        heuristics.push_back(h);
        savings.push_back(s);
    }

    bestSolution = make_shared<PermutationPhenotype>(vrp, BasePhenotype::RANDOM_FEASIBLE);

    int repetitionCounter = 0;

    using T = boost::packaged_task<shared_ptr<PermutationPhenotype>>;
    for(int G = 0; G < noIterations; G++) {

        vector<T> tasks;
        vector<shared_ptr<PermutationPhenotype>> antSolutions;

        tasks.reserve(populationSize);
        for(int i = 0; i < populationSize; i++){
            tasks.emplace_back([this](){return this->antWalk();});
        }

        vector<boost::unique_future<T::result_type>> futures;
        for(auto& t: tasks){
            futures.push_back(t.get_future());
            post(*pool, std::move(t));
        }

        for(auto& future: boost::when_all(futures.begin(), futures.end()).get()){
            antSolutions.push_back(future.get());
        }

        sort(antSolutions.begin(), antSolutions.end(),
             [](const shared_ptr<PermutationPhenotype>& p1, const shared_ptr<PermutationPhenotype>& p2){
            return p1->getPenaltyVal() < p2->getPenaltyVal();
        });

//        for(auto& p: antSolutions){
//            cout << *p << "; value = " << p->getPenaltyVal() << endl;
//        }

        double lCumulative = 0;
        for(const auto& antSolution: antSolutions){
            lCumulative += antSolution->getPenaltyVal();
        }

        updatePheromone(antSolutions, lCumulative / (double) antSolutions.size());

        bool foundNewBest = false;

        if(G == 0 || (antSolutions[0]->getPenaltyVal() < bestSolution->getPenaltyVal())){
            bestSolution = move(antSolutions[0]);
            foundNewBest = true;
            repetitionCounter = 0;
        }

        if(outputToFileStream){
            os << G + 1 << " " << bestSolution->getPenaltyVal() << endl;
        }

        if(!foundNewBest){
            repetitionCounter++;
            if(repetitionCounter >= maxNoRepetitions){
                cout << "Generation " << G + 1 << ": " << *bestSolution << "; value: " << bestSolution->getPenaltyVal() << endl;
                break;
            }
        }


        if ((G + 1) % (noIterations / 1000) == 0)
            cout << "Generation " << G + 1 << ": " << *bestSolution << "; value: " << bestSolution->getPenaltyVal() << endl;
    }
    if(outputToFileStream){
        os.flush();
    }
}

double ACO::feasibilityFormula(int index1, int index2) {
    double pheromoneValue = getFromTable(pheromones, index1, index2);
    double heuristicValue = getFromTable(heuristics, index1, index2);
    double savingsValue = getFromTable(savings, index1, index2);
    return pow(pheromoneValue, alpha) * pow(heuristicValue, beta) * pow(savingsValue, gamma);
}

shared_ptr<PermutationPhenotype> ACO::antWalk() {
    vector<int> path;
    int noCustomers = vrp.getDimension() - 1;
    int noVehicles = vrp.getNoVehicles();

    int maxCapacity = vrp.getCapacity();
    shared_ptr<PermutationPhenotype> p;

    do{
        path.clear();
        int depotIndex = vrp.getDimension();
        int currentCapacity = 0;
        int currentVehicles = 0;

        path.push_back(depotIndex++);
        currentVehicles++;

        int currentCustomer = getRandomInt(0, noCustomers);
        currentCapacity += getFromTable(demands, currentCustomer);
        path.push_back(currentCustomer);

        set<int> remainingCustomers;
        for(int i = 0; i < noCustomers; i++){
            if(i != currentCustomer){
                remainingCustomers.insert(i);
            }
        }

        while(!remainingCustomers.empty()){
            set<int> feasibleCustomers;
            for(int customer: remainingCustomers){
                if(currentCapacity + getFromTable(demands, customer) <= maxCapacity){
                    feasibleCustomers.insert(customer);
                }
            }
            if(!feasibleCustomers.empty()){
                vector<double> feasibilities;
                feasibilities.reserve(feasibleCustomers.size());
                double cumulativeFeasibility = 0;

                for(int customer: feasibleCustomers){
                    double feasibility = feasibilityFormula(currentCustomer, customer);
                    feasibilities.push_back(feasibility);
                    cumulativeFeasibility += feasibility;
                }

                vector<double> probabilities;
                probabilities.reserve(feasibilities.size());
                for(double f: feasibilities){
                    probabilities.push_back(f / cumulativeFeasibility);
                }

                double r = getRandom01();
                int ind = 0;
                double cumulativeProbability = 0;
                int nextCustomer = *(feasibleCustomers.rbegin());

                for(int customer: feasibleCustomers){
                    cumulativeProbability += probabilities[ind];
                    if(r < cumulativeProbability){
                        nextCustomer = customer;
                        break;
                    }
                    ind++;
                }

                path.push_back(nextCustomer);
                currentCapacity += getFromTable(demands, nextCustomer);
                remainingCustomers.erase(remainingCustomers.find(nextCustomer));
                currentCustomer = nextCustomer;
            } else {
                currentCapacity = 0;
                currentCustomer = depotIndex;
                path.push_back(depotIndex);
                currentVehicles++;
                if(currentVehicles > noVehicles){
                    break;
                }
                depotIndex++;
            }

        }

        p = make_shared<PermutationPhenotype>(path, vrp);
        p->evaluate();
//        cout << *p << endl;

    } while(!p->isViableSolution());

    if(searchNeighbourhood){
        auto n = p->generateRandomNeighbour();
        n->evaluate();
        if(PermutationPhenotype::compareByPenaltyValue(n, p)){
            return n;
        }
    }

    return p;
}

double ACO::getFromTable(vector<vector<double>> &table, int i, int j) {
    if(i > vrp.getDimension() - 2)
        i = 0;
    else
        i++;

    if(j > vrp.getDimension() - 2)
        j = 0;
    else
        j++;

    return table[i][j];
}

int ACO::getFromTable(vector<int> &table, int i) {
    if(i > vrp.getDimension() - 2)
        i = 0;
    else
        i++;

    return table[i];
}

void ACO::addInTable(vector<std::vector<double>> &table, int i, int j, double value) {
    if(i > vrp.getDimension() - 2)
        i = 0;
    else
        i++;

    if(j > vrp.getDimension() - 2)
        j = 0;
    else
        j++;

    table[i][j] += value;
    table[j][i] += value;
}

void ACO::pheromoneReduction(double lAvg) {
    auto size = pheromones.size();
    for(int i = 0; i < size; i++){
        for(int j = 0; j < size; j++){
            pheromones[i][j] *= (rho + theta / lAvg);
        }
    }
}

void ACO::updatePheromone(std::vector<std::shared_ptr<PermutationPhenotype>> paths, double lAvg) {
    pheromoneReduction(lAvg);

    double pathLength = bestSolution->getPenaltyVal();
    auto size = bestSolution->chromosome.size();
    double increment = (double) noUpdaters / pathLength;

    for(int i = 0; i < size - 1; i++){
        addInTable(pheromones, bestSolution->chromosome[i], bestSolution->chromosome[i + 1], increment);
    }
    addInTable(pheromones, bestSolution->chromosome[size - 1], bestSolution->chromosome[0], increment);

    for(int i = 1; i < noUpdaters; i++){
        pathLength = paths[i - 1]->getPenaltyVal();
        size = bestSolution->chromosome.size();
        increment = (double) (noUpdaters - i) / pathLength;
        for(int j = 0; j < size - 1; j++){
            addInTable(pheromones, paths[i]->chromosome[j], paths[i]->chromosome[j + 1], increment);
        }
        addInTable(pheromones, paths[i]->chromosome[size - 1], paths[i]->chromosome[0], increment);
    }
}

void ACO::printBest(ostream &os) {
    os << *bestSolution << endl;
    os << fixed << bestSolution->getPenaltyVal() << endl;
}

void ACO::setInitialPheromone(double initialPheromone) {
    ACO::initialPheromone = initialPheromone;
}

void ACO::setRho(double rho) {
    ACO::rho = rho;
}

void ACO::setAlpha(double alpha) {
    ACO::alpha = alpha;
}

void ACO::setBeta(double beta) {
    ACO::beta = beta;
}

void ACO::setGamma(double gamma) {
    ACO::gamma = gamma;
}


