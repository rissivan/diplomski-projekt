//
// Created by ivan on 1/4/22.
//
#define BOOST_THREAD_PROVIDES_FUTURE_WHEN_ALL_WHEN_ANY

#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/future.hpp>

#include "SA.h"


using namespace std;

double SA::SA_B = 0.01;
double SA::SA_ALPHA = 0.99995;
double SA::SA_BETA = 0.0001;

double SA::linearDecrement(double T) {
    return T - SA_B;
}

double SA::geometricDecrement(double T) {
    return T * SA_ALPHA;
}

double SA::verySlowDecrement(double T) {
    return T / (1 + SA_BETA * T);
}

SA::SA(VRP &vrp) : AbstractAlgorithm(vrp) {

}

void SA::run() {
    for(int i = 0; i < noReheats; i++){
        shared_ptr<PermutationPhenotype> p = make_shared<PermutationPhenotype>(vrp, constructionMode);
        currentSolution.push_back(p);
        p->evaluate();
        if(i == 0 || p->getPenaltyVal() < bestSolution->getPenaltyVal() && p->isViableSolution()){
            bestSolution = p;
        }
    }

    using T = boost::packaged_task<shared_ptr<PermutationPhenotype>>;

    double currentTemperature = startingTemperature;

    int G = 0;

    while(currentTemperature > finalTemperature){
        vector<T> tasks;
        vector<shared_ptr<PermutationPhenotype>> results;

        tasks.reserve(noReheats);
        for(int i = 0; i < noReheats; i++){
            tasks.emplace_back([this, currentTemperature, i](){return SA::generateRandomNeighbourAndEvaluate(this->currentSolution[i], currentTemperature);});
        }

        vector<boost::unique_future<T::result_type>> futures;
        for(auto& t: tasks){
            futures.push_back(t.get_future());
            post(*pool, std::move(t));
        }

        for(auto& future: boost::when_all(futures.begin(), futures.end()).get()){
            results.push_back(future.get());
        }

        for(int i = 0; i < noReheats; i++){
            if(results[i]->getPenaltyVal() < currentSolution[i]->getPenaltyVal()){
                currentSolution[i] = results[i];
                if(results[i]->getPenaltyVal() < bestSolution->getPenaltyVal() && results[i]->isViableSolution()){
                    bestSolution = move(results[i]);
                }
            }
        }

        currentTemperature = decrementFunction(currentTemperature);

        if(outputToFileStream){
            os << G + 1 << " " << bestSolution->getPenaltyVal() << endl;
        }

        cout << "Generation " << G++ + 1 << ", temperature = " << currentTemperature << ": " << *bestSolution << "; value: " << bestSolution->getPenaltyVal() << "; " << (bestSolution->isViableSolution() ? "" : "in") << "feasible" << endl;
    }
    if(outputToFileStream){
        os.flush();
    }
}

shared_ptr<PermutationPhenotype> SA::generateRandomNeighbourAndEvaluate(const shared_ptr<PermutationPhenotype>& solution, double currentTemperature) {
    auto p = solution->generateRandomNeighbour();
    p->evaluate();
    double chance = 1 / (1 + exp((p->getPenaltyVal() - solution->getPenaltyVal()) / currentTemperature));
    if(getRandom01() < chance)
        return p;
    return solution;
}

void SA::printBest(ostream &os) {
    os << *bestSolution << endl;
    os << fixed << bestSolution->getPenaltyVal() << endl;
}

void SA::setStartingTemperature(double startT) {
    SA::startingTemperature = startT;
}

void SA::setFinalTemperature(double finalT) {
    SA::finalTemperature = finalT;
}

void SA::setDecrementFunction(double (*decF)(double)) {
    SA::decrementFunction = decF;
}

void SA::setSaB(double saB) {
    SA_B = saB;
}

void SA::setSaAlpha(double saAlpha) {
    SA_ALPHA = saAlpha;
}

void SA::setSaBeta(double saBeta) {
    SA_BETA = saBeta;
}

