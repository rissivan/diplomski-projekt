//
// Created by ivan on 12/29/21.
//


#include "BSA.h"
#include "../Util/Util.h"

#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/future.hpp>


using namespace std;

BSA::BSA(VRP &vrp) : AbstractAlgorithm(vrp) {

}

void BSA::run() {
    population.reserve(populationSize);
    historicalPopulation.reserve(populationSize);
    for(int i = 0; i < populationSize; i++){
        shared_ptr<RKRealPhenotype> p = make_shared<RKRealPhenotype>(this->vrp, constructionMode);
        p->evaluate();
        if(i == 0 || p->getPenaltyVal() < bestSolution->getPenaltyVal()){
            bestSolution = p;
        }
        population.push_back(p);

        p = make_shared<RKRealPhenotype>(this->vrp, constructionMode);
        p->evaluate();
        if(p->getPenaltyVal() < bestSolution->getPenaltyVal()){
            bestSolution = p;
        }
        historicalPopulation.push_back(p);
    }

    using T = boost::packaged_task<shared_ptr<RKRealPhenotype>>;

    int repetitionCounter = 0;

    for(int G = 0; G < noGenerations; G++){
        if((G + 1) % (noGenerations / 1000) == 0)
            cout << fixed << "Generation " << G + 1 << "; value: " << bestSolution->getPenaltyVal() << ", " << (bestSolution->isViableSolution() ? "feasible" : "infeasible") << ": " << endl << *bestSolution << endl << endl;

        vector<T> tasks;
        vector<shared_ptr<RKRealPhenotype>> newPhenotypes;

        selectionI();

        tasks.reserve(populationSize);
        for(int i = 0; i < populationSize; i++){
            tasks.emplace_back([this, i](){return this->createAndEvaluate(i);});
        }

        vector<boost::unique_future<T::result_type>> futures;
        for(auto& t: tasks){
            futures.push_back(t.get_future());
            post(*pool, std::move(t));
        }

        for(auto& future: boost::when_all(futures.begin(), futures.end()).get()){
            newPhenotypes.push_back(future.get());
        }

        bool foundNewBest = false;

        for(int i = 0; i < populationSize; i++){
            population[i] = newPhenotypes[i];
            if(population[i]->getPenaltyVal() < bestSolution->getPenaltyVal()){
                bestSolution = population[i];
                foundNewBest = true;
                repetitionCounter = 0;
            }
        }

        if(outputToFileStream){
            os << G + 1 << " " << bestSolution->getPenaltyVal() << endl;
        }

        if(!foundNewBest){
            repetitionCounter++;
            if(repetitionCounter >= maxNoRepetitions){
                cout << fixed << "Generation " << G + 1 << "; value: " << bestSolution->getPenaltyVal() << ", " << (bestSolution->isViableSolution() ? "feasible" : "infeasible") << ": " << endl << *bestSolution << endl << endl;
                break;
            }
        }
    }
    if(outputToFileStream){
        os.flush();
    }
}

void BSA::selectionI() {
    double a = getRandom01();
    double b = getRandom01();
    //mozda staviti populacije kao shared_ptr<vector>
    if(a < b){
        historicalPopulation = population;
    }
    shuffle(historicalPopulation.begin(), historicalPopulation.end(), util::getEngine());
}

shared_ptr<RKRealPhenotype> BSA::mutation(int index) {
    int size = vrp.getPhenotypeSize();
    vector<double> mutantValues(size);      //mozda problem radi stack inicijalizacije

    for(int i = 0; i < size; i++){
        mutantValues[i] = population[index]->realValuesChromosome[i] + f * (historicalPopulation[index]->realValuesChromosome[i] - population[index]->realValuesChromosome[i]);
    }

    return make_shared<RKRealPhenotype>(mutantValues, vrp, normalize);
}

shared_ptr<RKRealPhenotype> BSA::crossover(int index, const shared_ptr<RKRealPhenotype>& mutant) {
    int size = vrp.getPhenotypeSize();
    vector<bool> map(size, true);

    double a = getRandom01();
    double b = getRandom01();

    if(a < b){
        vector<int> u(size);
        iota(u.begin(), u.end(), 0);
        int lastIndex = vrp.getInt();
        for(int i = 0; i < lastIndex; i++){
            map[u[i]] = false;
        }
    } else {
        map[vrp.getInt()] = false;
    }

    vector<double> trialValues(size);
    for(int i = 0; i < size; i++){
        trialValues[i] = (map[i]) ? mutant->realValuesChromosome[i] : population[index]->realValuesChromosome[i];
    }

    return make_shared<RKRealPhenotype>(trialValues, vrp, normalize);
}

shared_ptr<RKRealPhenotype> BSA::selectionII(int index, const shared_ptr<RKRealPhenotype> &trial) {
    return (trial->getPenaltyVal() < population[index]->getPenaltyVal()) ? trial : population[index];
}

shared_ptr<RKRealPhenotype> BSA::createAndEvaluate(int index) {
    shared_ptr<RKRealPhenotype> mutant = mutation(index);
    shared_ptr<RKRealPhenotype> trial = crossover(index, mutant);
    trial->evaluate();
    if(searchNeighbourhood){
        auto n = trial->generateRandomNeighbour();
        n->evaluate();
        if(RKRealPhenotype::compareByPenaltyValue(n, trial)){
            trial = n;
        }
    }
    return selectionII(index, trial);
}

void BSA::printBest(ostream &os) {
    os << *bestSolution << endl;
    os << fixed << bestSolution->getPenaltyVal() << endl;
}

void BSA::setF(double f) {
    BSA::f = f;
}
