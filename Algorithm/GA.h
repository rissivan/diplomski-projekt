//
// Created by ivan on 11/22/21.
//

#ifndef PROJEKT_GA_H
#define PROJEKT_GA_H

#include "AbstractAlgorithm.h"
#include "../Phenotype/PermutationPhenotype.h"

class GA: public AbstractAlgorithm{
private:
    std::vector<std::shared_ptr<PermutationPhenotype>> population;
    int noGenerations = 100000;
    int populationSize = 100;
    double p_m = 0.02;
    double mortalityRate = 0.3;
    void sortPopulation();
    std::shared_ptr<std::vector<int>> randomIndices(const bool *eliminated, int n);
    static void sortIndices(const std::shared_ptr<std::vector<int>>& indices);
    bool indexComparator(int ind1, int ind2);
public:
    std::shared_ptr<PermutationPhenotype> createAndEvaluate(const std::shared_ptr<PermutationPhenotype>& p1, const std::shared_ptr<PermutationPhenotype>& p2);
    explicit GA(VRP& vrp);
    void run() override;
    void setPopulationSize(int populationSize);
    void setPM(double pM);

    void printBest(std::ostream &os) override;
};


#endif //PROJEKT_GA_H
