//
// Created by ivan on 12/30/21.
//

#ifndef PROJEKT_PSO_H
#define PROJEKT_PSO_H

#include "AbstractAlgorithm.h"
#include "../Phenotype/RKRealPhenotype.h"

class PSO: public AbstractAlgorithm {
private:
    std::vector<std::shared_ptr<RKRealPhenotype>> particles;
    std::vector<std::shared_ptr<RKRealPhenotype>> bestLocal;
    std::vector<std::vector<double>> velocities;
    std::shared_ptr<RKRealPhenotype> bestGlobal;
    int noGenerations = 100000;
    int populationSize = 80;
    double w = 0.4; //mozda napraviti padajuci w
    double cl = 2;
    double cg = 2;
    bool normalize = false;
    std::shared_ptr<RKRealPhenotype> singleIteration(int index);
public:
    explicit PSO(VRP& vrp);
    void run() override;
    void printBest(std::ostream &os) override;

    void setW(double w);
    void setCl(double cL);
    void setCg(double cG);
};


#endif //PROJEKT_PSO_H
