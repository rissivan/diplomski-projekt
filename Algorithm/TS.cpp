//
// Created by ivan on 1/4/22.
//
#define BOOST_THREAD_PROVIDES_FUTURE_WHEN_ALL_WHEN_ANY

#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/future.hpp>

#include "TS.h"


using namespace std;

void TS::TabuList::add(const std::shared_ptr<PermutationPhenotype>& solution) {
    if(tabuList.size() == tenure){
        tabuList.erase(tabuList.begin());
    }
    tabuList.push_back(solution);
}

bool TS::TabuList::isTabu(const std::shared_ptr<PermutationPhenotype>& solution) {
    return find(tabuList.begin(), tabuList.end(), solution) != tabuList.end();
}

TS::TS(VRP &vrp) : AbstractAlgorithm(vrp) {
}

void TS::run() {
    tabuList = TabuList(tabuTenure);
    currentSolution = make_shared<PermutationPhenotype>(vrp, constructionMode);
    currentSolution->evaluate();
    bestSolution = currentSolution;

    using T = boost::packaged_task<shared_ptr<PermutationPhenotype>>;

    vector<int> vehicleIndices = currentSolution->getVehicleIndices();
    vector<int> routeLengths = currentSolution->getRouteLengths();

    int vehicles = vrp.getNoVehicles();

    for(int G = 0; G < noIterations; G++){
        vector<T> tasks;
        vector<shared_ptr<PermutationPhenotype>> neighbours;

        //intra swap
        for(int i = 0; i < vehicles; i++){
            if(routeLengths[i] < 2){
                continue;
            }
            for(int j = 0; j < routeLengths[i] - 1; j++){
                for(int k = j + 1; k < routeLengths[i]; k++){
                    tasks.emplace_back([this, i, j, k](){return this->intraSwapAndEvaluate(i, j, k);});
                }
            }
        }

        //inter swap
        for(int i = 0; i < vehicles - 1; i++){
            if(routeLengths[i] < 2){
                continue;
            }
            for(int j = i + 1; j < vehicles; j++){
                if(routeLengths[j] < 2){
                    continue;
                }
                for(int ii = 0; ii < routeLengths[i]; ii++){
                    for(int jj = 0; jj < routeLengths[j]; jj++){
                        tasks.emplace_back([this, i, j, ii, jj](){return this->interSwapAndEvaluate(i, j, ii, jj);});
                    }
                }
            }
        }

        //intra shift
        for(int i = 0; i < vehicles; i++){
            if(routeLengths[i] < 2){
                continue;
            }
            for(int j = 0; j < routeLengths[i]; j++){
                for(int k = 1; k < routeLengths[i]; k++){
                    tasks.emplace_back([this, i, j, k](){return this->intraShiftAndEvaluate(i, j, k);});
                }
            }
        }

        //inter shift
        for(int i = 0; i < vrp.getDimension() - 1; i++){
            for(int j = 1; j < vrp.getPhenotypeSize() - 1; j++){
                tasks.emplace_back([this, i, j](){return this->interShiftAndEvaluate(i, j);});
            }
        }

        //intra 2 opt
        for(int i = 0; i < vehicles; i++){
            if(routeLengths[i] < 2){
                continue;
            }
            for(int j = 0; j < routeLengths[i] - 1; j++){
                for(int k = j + 1; k < routeLengths[i]; k++){
                    tasks.emplace_back([this, i, j, k](){return this->intra2OptAndEvaluate(i, j, k);});
                }
            }
        }

        //inter 2 opt
        for(int i = 0; i < vehicles - 1; i++){
            if(routeLengths[i] < 2){
                continue;
            }
            for(int j = i + 1; j < vehicles; j++){
                if(routeLengths[j] < 2){
                    continue;
                }
                for(int ii = 0; ii < routeLengths[i]; ii++){
                    for(int jj = 0; jj < routeLengths[j]; jj++){
                        tasks.emplace_back([this, i, j, ii, jj](){return this->inter2OptAndEvaluate(i, j, ii, jj);});
                    }
                }
            }
        }

        vector<boost::unique_future<T::result_type>> futures;
        for(auto& t: tasks){
            futures.push_back(t.get_future());
            post(*pool, std::move(t));
        }

        for(auto& future: boost::when_all(futures.begin(), futures.end()).get()){
            neighbours.push_back(future.get());
        }

        set<shared_ptr<PermutationPhenotype>, decltype(&PermutationPhenotype::compareByPenaltyValue)> neighbourSet(&PermutationPhenotype::compareByPenaltyValue);

        for(const auto& n: neighbours){
            neighbourSet.insert(n);
        }

        for(const auto& n: neighbourSet){
            if(!tabuList.isTabu(n)){
                currentSolution = n;
                if(currentSolution->getPenaltyVal() < bestSolution->getPenaltyVal()){
                    bestSolution = currentSolution;
                }
                tabuList.add(n);
                break;
            }
        }

        if(outputToFileStream){
            os << G + 1 << " " << bestSolution->getPenaltyVal() << endl;
        }

        cout << "Generation " << G + 1 << ": " << *bestSolution << "; value: " << bestSolution->getPenaltyVal() << "; " << (bestSolution->isViableSolution() ? "" : "in") << "feasible" << endl;
    }
    if(outputToFileStream){
        os.flush();
    }
}


shared_ptr<PermutationPhenotype> TS::intraSwapAndEvaluate(int vehicle, int offset1, int offset2) {
    shared_ptr<PermutationPhenotype> p = currentSolution->intraSwap(vehicle, offset1, offset2);
    p->evaluate();
    return p;
}

shared_ptr<PermutationPhenotype> TS::interSwapAndEvaluate(int vehicle1, int vehicle2, int offset1,
                                                                            int offset2) {
    shared_ptr<PermutationPhenotype> p = currentSolution->interSwap(vehicle1, vehicle2, offset1, offset2);
    p->evaluate();
    return p;
}

shared_ptr<PermutationPhenotype> TS::intraShiftAndEvaluate(int vehicle, int customerOffset,
                                                                             int shiftOffset) {
    shared_ptr<PermutationPhenotype> p = currentSolution->intraShift(vehicle, customerOffset, shiftOffset);
    p->evaluate();
    return p;
}

shared_ptr<PermutationPhenotype> TS::interShiftAndEvaluate(int customerOffset, int shiftOffset) {
    shared_ptr<PermutationPhenotype> p = currentSolution->interShift(customerOffset, shiftOffset);
    p->evaluate();
    return p;
}

shared_ptr<PermutationPhenotype> TS::intra2OptAndEvaluate(int vehicle, int offset1, int offset2) {
    shared_ptr<PermutationPhenotype> p = currentSolution->intra2Opt(vehicle, offset1, offset2);
    p->evaluate();
    return p;
}

shared_ptr<PermutationPhenotype> TS::inter2OptAndEvaluate(int vehicle1, int vehicle2, int offset1,
                                                                            int offset2) {
    shared_ptr<PermutationPhenotype> p = currentSolution->inter2Opt(vehicle1, vehicle2, offset1, offset2);
    p->evaluate();
    return p;
}

void TS::printBest(ostream &os) {
    os << *bestSolution << endl;
    os << fixed << bestSolution->getPenaltyVal() << endl;
}

void TS::setTabuTenure(int tabuTenure) {
    TS::tabuTenure = tabuTenure;
}
