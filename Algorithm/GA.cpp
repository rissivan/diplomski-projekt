//
// Created by ivan on 11/22/21.
//
#define BOOST_THREAD_PROVIDES_FUTURE_WHEN_ALL_WHEN_ANY

#include <algorithm>
#include <iostream>
#include <cstring>
#include <set>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/future.hpp>

#include "GA.h"
#include "../Util/Util.h"


using namespace std;

//initial population generator has to be better
//got to find a viable function

GA::GA(VRP &vrp) : AbstractAlgorithm(vrp) {
}

void GA::run() {

    population.reserve(populationSize);
    for(int i = 0; i < populationSize; i++){
        cout << "Generating phenotype #" << i + 1 << endl;
        shared_ptr<PermutationPhenotype> p = make_shared<PermutationPhenotype>(this->vrp, constructionMode);
        p->evaluate();
        population.push_back(p);
    }
    sortPopulation();

    int mortality = (int) (mortalityRate * populationSize);

    using T = boost::packaged_task<shared_ptr<PermutationPhenotype>>;

    for(int G = 0; G < noGenerations; G++){
        if((G + 1) % (noGenerations / 1000) == 0)
            cout << "Generation " << G + 1 << ": " << *population[0] << "; value: " << population[0]->getPenaltyVal() << endl;

        bool eliminated[populationSize];
        vector<shared_ptr<PermutationPhenotype>> newPhenotypes;
        memset(eliminated, false, sizeof(eliminated));

        vector<T> tasks;

        for(int i = 0; i < mortality; i++){
            shared_ptr<vector<int>> tournamentIndices = randomIndices(eliminated, 3);
            sortIndices(tournamentIndices);
            eliminated[(*tournamentIndices)[2]] = true;
            shared_ptr<PermutationPhenotype> p1 = population[(*tournamentIndices)[0]];
            shared_ptr<PermutationPhenotype> p2 = population[(*tournamentIndices)[1]];
            tasks.emplace_back([this, p1, p2](){return this->createAndEvaluate(p1, p2);});
        }

        vector<boost::unique_future<T::result_type>> futures;
        for(auto& t: tasks){
            futures.push_back(t.get_future());
            post(*pool, std::move(t));
        }

        for(auto& future: boost::when_all(futures.begin(), futures.end()).get()){
            auto r = future.get();
            newPhenotypes.push_back(r);
        }

//        cout << ">>>" << newPhenotypes.size() << endl;

        auto it = newPhenotypes.begin();
        for(int i = 0; i < populationSize; i++){
            if(eliminated[i]){
                population[i] = *it;
                it++;
            }
        }
        sortPopulation();

        if(outputToFileStream){
            os << G + 1 << " " << population[0]->getPenaltyVal() << endl;
        }
    }
    if(outputToFileStream){
        os.flush();
    }
}

shared_ptr<PermutationPhenotype>
GA::createAndEvaluate(const shared_ptr<PermutationPhenotype>& p1, const shared_ptr<PermutationPhenotype>& p2) {
    shared_ptr<PermutationPhenotype> newPhenotype = p1->crossover(p2);
    if(getRandom01() < p_m)
        newPhenotype->mutate();
    newPhenotype->evaluate();

    if(searchNeighbourhood){
        auto n = newPhenotype->generateRandomNeighbour();
        n->evaluate();
        if(PermutationPhenotype::compareByPenaltyValue(n, newPhenotype)){
            return n;
        }
    }

    return newPhenotype;
}

void GA::sortPopulation() {
    sort(population.begin(), population.end(), PermutationPhenotype::compareByPenaltyValue);
}

shared_ptr<vector<int>> GA::randomIndices(const bool *eliminated, int n) {
    shared_ptr<vector<int>> indices = make_shared<vector<int>>(n);
    set<int> selectedIndices;
    for(int i = 0; i < n; i++){
        int r;
        do{
            r = getRandomInt(0, populationSize);
        } while (eliminated[r] || selectedIndices.find(r) != selectedIndices.end());
        (*indices)[i] = r;
        selectedIndices.insert(r);
    }
    return indices;
}

void GA::sortIndices(const shared_ptr<vector<int>>& indices) {
    sort(indices->begin(), indices->end());
}

bool GA::indexComparator(int ind1, int ind2) {
    return population[ind1]->getPenaltyVal() < population[ind2]->getPenaltyVal();
}

void GA::printBest(ostream &os) {
    os << *population[0] << endl;
    os << fixed << population[0]->getPenaltyVal() << endl;
}

void GA::setPopulationSize(int popSize) {
    this->populationSize = popSize;
}

void GA::setPM(double pM) {
    this->p_m = pM;
}

