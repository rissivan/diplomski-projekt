//
// Created by ivan on 1/4/22.
//

#ifndef PROJEKT_SA_H
#define PROJEKT_SA_H

#include "AbstractAlgorithm.h"
#include "../Phenotype/PermutationPhenotype.h"

class SA: public AbstractAlgorithm{
private:
    std::shared_ptr<PermutationPhenotype> bestSolution;
    std::vector<std::shared_ptr<PermutationPhenotype>> currentSolution;
    int noReheats = 30;
    double startingTemperature = 500;
    double finalTemperature = 0.01;
    double (*decrementFunction)(double) = &geometricDecrement;
    static double SA_B;
    static double SA_ALPHA;
    static double SA_BETA;
    static std::shared_ptr<PermutationPhenotype> generateRandomNeighbourAndEvaluate(const std::shared_ptr<PermutationPhenotype>& solution, double currentTemperature);
public:
    static double linearDecrement(double T);
    static double geometricDecrement(double T);
    static double verySlowDecrement(double T);
    explicit SA(VRP& vrp);
    void run() override;
    void setStartingTemperature(double startT);
    void setFinalTemperature(double finalT);
    void setDecrementFunction(double (*decF)(double));
    void printBest(std::ostream &os) override;

    static void setSaB(double saB);
    static void setSaAlpha(double saAlpha);
    static void setSaBeta(double saBeta);
};


#endif //PROJEKT_SA_H
