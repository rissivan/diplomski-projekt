//
// Created by ivan on 1/4/22.
//

#ifndef PROJEKT_ABC_H
#define PROJEKT_ABC_H

#include "AbstractAlgorithm.h"
#include "../Phenotype/PermutationPhenotype.h"

class ABC: public AbstractAlgorithm {
private:
    std::vector<std::shared_ptr<PermutationPhenotype>> workers;
    std::vector<std::shared_ptr<PermutationPhenotype>> onlookers;
    std::shared_ptr<PermutationPhenotype> bestSolution;
    std::vector<int> onlookerIndices;
    std::vector<int> lifetimes;
    int noIterations = 100000;
    int populationSize = 30;
    int noWorkers{};
    int noOnlookers{};
    int foodSourceLifetime{};
    std::shared_ptr<PermutationPhenotype> workerFlight(int index);
    std::shared_ptr<PermutationPhenotype> onlookerFlight(int workerIndex);
    std::shared_ptr<PermutationPhenotype> scoutFlight();
public:
    explicit ABC(VRP& vrp);
    void run() override;
    void printBest(std::ostream &os) override;
    void setPopulationSize(int populationSize);
};


#endif //PROJEKT_ABC_H
