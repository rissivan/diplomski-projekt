//
// Created by ivan on 1/4/22.
//
#define BOOST_THREAD_PROVIDES_FUTURE_WHEN_ALL_WHEN_ANY

#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/future.hpp>

#include "ABC.h"


using namespace std;

ABC::ABC(VRP &vrp) : AbstractAlgorithm(vrp) {
}

void ABC::run() {
    this->noWorkers = populationSize / 2;
    this->noOnlookers = populationSize / 2;
    this->workers.reserve(noWorkers);
    this->onlookers.reserve(noOnlookers);
    this->lifetimes = vector<int>(noWorkers);
    this->onlookerIndices = vector<int>(noOnlookers);
    this->foodSourceLifetime = populationSize * vrp.getNoVehicles();
    for(int i = 0; i < noWorkers; i++){
        cout << "Generating phenotype #" << i + 1 << endl;
        shared_ptr<PermutationPhenotype> p = make_shared<PermutationPhenotype>(this->vrp, constructionMode);
        p->evaluate();
        this->workers.push_back(p);
        if(i == 0 || p->getPenaltyVal() < bestSolution->getPenaltyVal()){
            bestSolution = p;
        }
    }

    using T = boost::packaged_task<shared_ptr<PermutationPhenotype>>;

    for(int G = 0; G < noIterations; G++){
        //workers
        vector<T> workerTasks;
        vector<shared_ptr<PermutationPhenotype>> workerResults;

        workerTasks.reserve(noWorkers);
        for(int i = 0; i < noWorkers; i++){
            workerTasks.emplace_back([this, i](){return this->workerFlight(i);});
        }

        vector<boost::unique_future<T::result_type>> workerFutures;
        for(auto& t: workerTasks){
            workerFutures.push_back(t.get_future());
            post(*pool, std::move(t));
        }

        for(auto& future: boost::when_all(workerFutures.begin(), workerFutures.end()).get()){
            workerResults.push_back(future.get());
        }

        for(int i = 0; i < noWorkers; i++){
            workers[i] = workerResults[i];
            if(workerResults[i]->getPenaltyVal() < bestSolution->getPenaltyVal() && workerResults[i]->isViableSolution()){
                bestSolution = move(workerResults[i]);
            }
        }

        //onlookers
        double maxPenalty = workers[0]->getPenaltyVal();
        for(int i = 1; i < noWorkers; i++){
            if(workers[i]->getPenaltyVal() < maxPenalty){
                maxPenalty = workers[i]->getPenaltyVal();
            }
        }

        double totalFitness = 0;

        vector<double> fitness(noWorkers);
        for(int i = 0; i < noWorkers; i++){
            fitness[i] = maxPenalty - workers[i]->getPenaltyVal();
            totalFitness += fitness[i];
        }

        vector<double> probabilities(noWorkers);
        for(int i = 0; i < noWorkers; i++){
            probabilities[i] = fitness[i] / totalFitness;
        }

        for(int i = 0; i < noOnlookers; i++){
            double r = getRandom01();
            double cumulativeProbability = 0;
            int workerIndex = 0;

            for (int j = 0; j < noWorkers; j++) {
                cumulativeProbability += probabilities[j];
                if (r < cumulativeProbability) {
                    workerIndex = j;
                    break;
                }
            }
            onlookerIndices[i] = workerIndex;
        }

        vector<T> onlookerTasks;

        onlookers.clear();
        onlookers.reserve(noOnlookers);

        onlookerTasks.reserve(noWorkers);
        for(int i = 0; i < noWorkers; i++){
            onlookerTasks.emplace_back([this, i](){return this->onlookerFlight(onlookerIndices[i]);});
        }

        vector<boost::unique_future<T::result_type>> onlookerFutures;
        for(auto& t: onlookerTasks){
            onlookerFutures.push_back(t.get_future());
            post(*pool, std::move(t));
        }

        for(auto& future: boost::when_all(onlookerFutures.begin(), onlookerFutures.end()).get()){
            onlookers.push_back(future.get());
        }

        for(int i = 0; i < noOnlookers; i++){
            if(onlookers[i]->getPenaltyVal() < workers[onlookerIndices[i]]->getPenaltyVal()){
                workers[onlookerIndices[i]] = onlookers[i];
                lifetimes[onlookerIndices[i]] = 0;
                if(onlookers[i]->getPenaltyVal() < bestSolution->getPenaltyVal() && onlookers[i]->isViableSolution()){
                    bestSolution = move(onlookers[i]);
                }
            }
        }

        //scouts
        for(int i = 0; i < noWorkers; i++){
            if(lifetimes[i] >= foodSourceLifetime){
                workers[i] = scoutFlight();
                lifetimes[i] = 0;
            }
        }

        if(outputToFileStream){
            os << G + 1 << " " << bestSolution->getPenaltyVal() << endl;
        }

        if ((G + 1) % (noIterations / 1000) == 0)
            cout << "Generation " << G + 1 << ": " << *bestSolution << "; value: " << bestSolution->getPenaltyVal() << "; " << (bestSolution->isViableSolution() ? "" : "in") << "feasible" << endl;;
    }
    if(outputToFileStream){
        os.flush();
    }
}

std::shared_ptr<PermutationPhenotype> ABC::workerFlight(int index) {
    shared_ptr<PermutationPhenotype> neighbour;
    do{
        neighbour = workers[index]->generateRandomNeighbour();
        neighbour->evaluate();
    } while(!neighbour->isViableSolution());
    if(neighbour->getPenaltyVal() < workers[index]->getPenaltyVal()){
        lifetimes[index] = 0;
        return neighbour;
    }
    lifetimes[index]++;
    return workers[index];
}

std::shared_ptr<PermutationPhenotype> ABC::onlookerFlight(int workerIndex) {
    shared_ptr<PermutationPhenotype> neighbour;
    do{
        neighbour = workers[workerIndex]->generateRandomNeighbour();
        neighbour->evaluate();
    } while(!neighbour->isViableSolution());
    if(neighbour->getPenaltyVal() < workers[workerIndex]->getPenaltyVal()){
        return neighbour;
    }
    return workers[workerIndex];
}

std::shared_ptr<PermutationPhenotype> ABC::scoutFlight() {
    shared_ptr<PermutationPhenotype> scoutResult;
    do{
        scoutResult = bestSolution->generateRandomNeighbour();
        scoutResult->evaluate();
    } while(!scoutResult->isViableSolution());
    return scoutResult;
}

void ABC::printBest(ostream &os) {
    os << *bestSolution << endl;
    os << fixed << bestSolution->getPenaltyVal() << endl;
}

void ABC::setPopulationSize(int populationSize) {
    ABC::populationSize = populationSize;
}
