//
// Created by ivan on 11/22/21.
//

#ifndef PROJEKT_ABSTRACTALGORITHM_H
#define PROJEKT_ABSTRACTALGORITHM_H

#include <boost/asio.hpp>
#include <thread>
#include <memory>
#include <fstream>

#include "../VehicleRoutingProblem/VRP.h"
#include "../Phenotype/BasePhenotype.h"
#include "../Phenotype/PermutationPhenotype.h"

#define BOOST_THREAD_PROVIDES_FUTURE_WHEN_ALL_WHEN_ANY


class AbstractAlgorithm {
protected:
    VRP& vrp;
    std::unique_ptr<boost::asio::thread_pool> pool;
    static double getRandom01();
    BasePhenotype::ConstructionMode constructionMode = BasePhenotype::GUIDED_CLASSIC;
    bool searchNeighbourhood = false;
    bool outputToFileStream = false;
    std::ofstream os;
    int maxNoRepetitions = 20000;
    std::shared_ptr<PermutationPhenotype> generateBestImprovingNeighbour(const std::shared_ptr<PermutationPhenotype>& phenotype);
public:
    static int getRandomInt(int n1, int n2);
    explicit AbstractAlgorithm(VRP& vrp);
    virtual void run() = 0;
    virtual void printBest(std::ostream& os) = 0;
    void setConstructionMode(BasePhenotype::ConstructionMode mode);
    void setSearchNeighbourhood(bool search);
    void outputToFile(const std::string& path);
};


#endif //PROJEKT_ABSTRACTALGORITHM_H
