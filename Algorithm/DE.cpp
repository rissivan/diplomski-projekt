//
// Created by ivan on 12/29/21.
//
#define BOOST_THREAD_PROVIDES_FUTURE_WHEN_ALL_WHEN_ANY


#include "DE.h"

#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/future.hpp>

using namespace std;


DE::DE(VRP &vrp) : AbstractAlgorithm(vrp) {

}

void DE::run() {
    population.reserve(populationSize);
    for(int i = 0; i < populationSize; i++){
        shared_ptr<RKRealPhenotype> p = make_shared<RKRealPhenotype>(this->vrp, constructionMode);
        p->evaluate();
        if(i == 0 || p->getPenaltyVal() < bestSolution->getPenaltyVal()){
            bestSolution = p;
        }
        population.push_back(p);
    }

    using T = boost::packaged_task<shared_ptr<RKRealPhenotype>>;
    int repetitionCounter = 0;

    for(int G = 0; G < noGenerations; G++){
        if((G + 1) % (noGenerations / 1000) == 0)
            cout << fixed << "Generation " << G + 1 << "; value: " << bestSolution->getPenaltyVal() << ", " << (bestSolution->isViableSolution() ? "feasible" : "infeasible") << ": " << endl << *bestSolution << endl << endl;

        vector<T> tasks;
        vector<shared_ptr<RKRealPhenotype>> newPhenotypes;

        for(int i = 0; i < populationSize; i++){
            shared_ptr<vector<int>> indices = randomIndices(i);
            tasks.emplace_back([this, indices, i](){return this->createAndEvaluate(i, (*indices)[0], (*indices)[1], (*indices)[2]);});
        }

        vector<boost::unique_future<T::result_type>> futures;
        for(auto& t: tasks){
            futures.push_back(t.get_future());
            post(*pool, std::move(t));
        }

        for(auto& future: boost::when_all(futures.begin(), futures.end()).get()){
            newPhenotypes.push_back(future.get());
        }

        bool foundNewBest = false;

        for(int i = 0; i < populationSize; i++){
            population[i] = newPhenotypes[i];
            if(population[i]->getPenaltyVal() < bestSolution->getPenaltyVal()){
                bestSolution = population[i];
                repetitionCounter = 0;
                foundNewBest = true;
            }
        }

        if(outputToFileStream){
            os << G + 1 << " " << bestSolution->getPenaltyVal() << endl;
        }

        if(!foundNewBest){
            repetitionCounter++;
            if(repetitionCounter >= maxNoRepetitions){
                cout << fixed << "Generation " << G + 1 << "; value: " << bestSolution->getPenaltyVal() << ", " << (bestSolution->isViableSolution() ? "feasible" : "infeasible") << ": " << endl << *bestSolution << endl << endl;
                break;
            }
        }
    }
    if(outputToFileStream){
        os.flush();
    }
}

shared_ptr<vector<int>> DE::randomIndices(int x) const {
    shared_ptr<vector<int>> indices = make_shared<vector<int>>();
    indices->reserve(3);

    for(int i = 0; i < 3; i++){
        int r;
        do{
            r = getRandomInt(0, populationSize);
        } while(x == r || find(indices->begin(), indices->end(), r) != indices->end());
        indices->push_back(r);
    }

    return indices;
}

std::shared_ptr<RKRealPhenotype> DE::createMutant(int baseIndex, int diffIndex1, int diffIndex2) {
    int size = vrp.getPhenotypeSize();
    vector<double> mutantValues(size);      //mozda problem radi stack inicijalizacije

    for(int i = 0; i < size; i++){
        mutantValues[i] = population[baseIndex]->realValuesChromosome[i] + f * (population[diffIndex1]->realValuesChromosome[i] - population[diffIndex2]->realValuesChromosome[i]);
    }

    return make_shared<RKRealPhenotype>(mutantValues, vrp, normalize);
}

shared_ptr<RKRealPhenotype>
DE::crossover(const shared_ptr<RKRealPhenotype>& old, const shared_ptr<RKRealPhenotype>& mutant) {
    int size = vrp.getPhenotypeSize();
    vector<double> trialValues(size);

    int l = vrp.getInt();

    for(int i = 0; i < size; i++){
        double r = getRandom01();
        trialValues[i] = (r < cr || l == i) ? old->realValuesChromosome[i] : mutant->realValuesChromosome[i];
    }

    return make_shared<RKRealPhenotype>(trialValues, vrp, normalize);
}

std::shared_ptr<RKRealPhenotype> DE::createAndEvaluate(int currentIndex, int baseIndex, int diffIndex1, int diffIndex2) {
    shared_ptr<RKRealPhenotype> mutant = createMutant(baseIndex, diffIndex1, diffIndex2);
    shared_ptr<RKRealPhenotype> trial = crossover(population[currentIndex], mutant);
    trial->evaluate();

    if(searchNeighbourhood){
        auto n = trial->generateRandomNeighbour();
        n->evaluate();
        if(RKRealPhenotype::compareByPenaltyValue(n, trial)){
            trial = n;
        }
    }

    return (trial->getPenaltyVal() < population[currentIndex]->getPenaltyVal()) ? trial : population[currentIndex];
}

void DE::printBest(ostream &os) {
    os << *bestSolution << endl;
    os << fixed << bestSolution->getPenaltyVal() << endl;
}

void DE::setF(double f) {
    DE::f = f;
}

void DE::setCr(double cr) {
    DE::cr = cr;
}
