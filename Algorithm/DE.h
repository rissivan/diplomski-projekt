//
// Created by ivan on 12/29/21.
//

#ifndef PROJEKT_DE_H
#define PROJEKT_DE_H

#include "AbstractAlgorithm.h"
#include "../Phenotype/RKRealPhenotype.h"

class DE: public AbstractAlgorithm {
private:
    std::vector<std::shared_ptr<RKRealPhenotype>> population;
    std::shared_ptr<RKRealPhenotype> bestSolution;
    int noGenerations = 100000;
    int populationSize = 50;
    double f = 0.4;
    double cr = 0.8;
    bool normalize = false;
    [[nodiscard]] std::shared_ptr<std::vector<int>> randomIndices(int x) const;
    std::shared_ptr<RKRealPhenotype> createMutant(int baseIndex, int diffIndex1, int diffIndex2);
    std::shared_ptr<RKRealPhenotype> crossover(const std::shared_ptr<RKRealPhenotype>& old, const std::shared_ptr<RKRealPhenotype>& mutant);
    std::shared_ptr<RKRealPhenotype> createAndEvaluate(int currentIndex, int baseIndex, int diffIndex1, int diffIndex2);
public:
    explicit DE(VRP& vrp);
    void run() override;
    void printBest(std::ostream &os) override;
    void setF(double f);
    void setCr(double cr);
};


#endif //PROJEKT_DE_H
