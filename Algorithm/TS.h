//
// Created by ivan on 1/4/22.
//

#ifndef PROJEKT_TS_H
#define PROJEKT_TS_H

#include "AbstractAlgorithm.h"
#include "../Phenotype/PermutationPhenotype.h"

class TS: public AbstractAlgorithm {
private:
    class TabuList{
        private:
            int tenure{};
            std::vector<std::shared_ptr<PermutationPhenotype>> tabuList;
        public:
            TabuList()= default;
            explicit TabuList(int tenure): tenure(tenure){}
            void add(const std::shared_ptr<PermutationPhenotype>& solution);
            bool isTabu(const std::shared_ptr<PermutationPhenotype>& solution);
    };
    std::shared_ptr<PermutationPhenotype> bestSolution;
    std::shared_ptr<PermutationPhenotype> currentSolution;
    TabuList tabuList;
    int noIterations = 1000;
    int tabuTenure = 50;
    std::shared_ptr<PermutationPhenotype> intraSwapAndEvaluate(int vehicle, int offset1, int offset2);
    std::shared_ptr<PermutationPhenotype> interSwapAndEvaluate(int vehicle1, int vehicle2, int offset1, int offset2);
    std::shared_ptr<PermutationPhenotype> intraShiftAndEvaluate(int vehicle, int customerOffset, int shiftOffset);
    std::shared_ptr<PermutationPhenotype> interShiftAndEvaluate(int customerOffset, int shiftOffset);
    std::shared_ptr<PermutationPhenotype> intra2OptAndEvaluate(int vehicle, int offset1, int offset2);
    std::shared_ptr<PermutationPhenotype> inter2OptAndEvaluate(int vehicle1, int vehicle2, int offset1, int offset2);
public:
    explicit TS(VRP& vrp);
    void run() override;
    void setTabuTenure(int tabuTenure);
    void printBest(std::ostream &os) override;
};


#endif //PROJEKT_TS_H
