//
// Created by ivan on 11/22/21.
//

#include "AbstractAlgorithm.h"
#include "../Util/Util.h"
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/future.hpp>

using namespace std;

AbstractAlgorithm::AbstractAlgorithm(VRP& vrp): vrp(vrp) {
    this->pool = std::make_unique<boost::asio::thread_pool>(std::thread::hardware_concurrency());
}

double AbstractAlgorithm::getRandom01() {
    return util::getDist01()(util::getEngine());
}

int AbstractAlgorithm::getRandomInt(int n1, int n2) {
    return n1 + (int)((n2 - n1) * getRandom01());
}

void AbstractAlgorithm::setConstructionMode(BasePhenotype::ConstructionMode mode) {
    AbstractAlgorithm::constructionMode = mode;
}

void AbstractAlgorithm::setSearchNeighbourhood(bool search) {
    AbstractAlgorithm::searchNeighbourhood = search;
}

void AbstractAlgorithm::outputToFile(const std::string& path) {
    this->outputToFileStream = true;
    os = std::ofstream(path);
}


shared_ptr<PermutationPhenotype> AbstractAlgorithm::generateBestImprovingNeighbour(const shared_ptr<PermutationPhenotype>& phenotype){
    vector<int> vehicleIndices = phenotype->getVehicleIndices();
    vector<int> routeLengths = phenotype->getRouteLengths();

    int vehicles = vrp.getNoVehicles();

    using T = boost::packaged_task<shared_ptr<PermutationPhenotype>>;
    vector<T> tasks;
    vector<shared_ptr<PermutationPhenotype>> neighbours;

    //intra swap

    for(int vehicle = 0; vehicle < vehicles; vehicle++){
        if(routeLengths[vehicle] < 2){
            continue;
        }
        for(int offset1 = 0; offset1 < routeLengths[vehicle]; offset1++){
            for(int offset2 = offset1 + 1; offset2 < routeLengths[vehicle]; offset2++){
                tasks.emplace_back([phenotype, vehicle, offset1, offset2](){return phenotype->intraSwapAndEvaluate(vehicle, offset1, offset2);});
            }
        }
    }

    //inter swap
    for(int vehicle1 = 0; vehicle1 < vehicles - 1; vehicle1++){
        if(routeLengths[vehicle1] < 2){
            continue;
        }
        for(int vehicle2 = vehicle1 + 1; vehicle2 < vehicles; vehicle2++){
            for(int offset1 = 0; offset1 < routeLengths[vehicle1]; offset1++){
                for(int offset2 = 0; offset2 < routeLengths[vehicle2]; offset2++){
                    tasks.emplace_back([phenotype, vehicle1, vehicle2, offset1, offset2](){return phenotype->interSwapAndEvaluate(vehicle1, vehicle2, offset1, offset2);});
                }
            }
        }
    }

    //intra shift
    for(int vehicle = 0; vehicle < vehicles; vehicle++){
        if(routeLengths[vehicle] < 2){
            continue;
        }
        for(int customerOffset = 0; customerOffset < routeLengths[vehicle]; customerOffset++){
            for(int shiftOffset = 1; shiftOffset < routeLengths[vehicle]; shiftOffset++){
                tasks.emplace_back([phenotype, vehicle, customerOffset, shiftOffset](){return phenotype->intraShiftAndEvaluate(vehicle, customerOffset, shiftOffset);});
            }
        }
    }

    //inter shift
    for(int customerOffset = 0; customerOffset < vrp.getDimension() - 1; customerOffset++){
        for(int shiftOffset = 1; shiftOffset < vrp.getPhenotypeSize() - 1; shiftOffset++){
            tasks.emplace_back([phenotype, customerOffset, shiftOffset](){return phenotype->interShiftAndEvaluate(customerOffset, shiftOffset);});
        }
    }

    //intra 2 opt
    for(int vehicle = 0; vehicle < vehicles; vehicle++){
        if(routeLengths[vehicle] < 2){
            continue;
        }
        for(int offset1 = 0; offset1 < routeLengths[vehicle] - 1; offset1++){
            for(int offset2 = offset1 + 1; offset2 < routeLengths[vehicle]; offset2++){
                tasks.emplace_back([phenotype, vehicle, offset1, offset2](){return phenotype->intra2OptAndEvaluate(vehicle, offset1, offset2);});
            }
        }
    }

    //inter 2 opt
    for(int vehicle1 = 0; vehicle1 < vehicles - 1; vehicle1++){
        if(routeLengths[vehicle1] < 2){
            continue;
        }
        for(int vehicle2 = vehicle1 + 1; vehicle2 < vehicles; vehicle2++){
            if(routeLengths[vehicle2] < 2){
                continue;
            }
            for(int offset1 = 0; offset1 < routeLengths[vehicle1]; offset1++){
                for(int offset2 = 0; offset2 < routeLengths[vehicle2]; offset2++){
                    tasks.emplace_back([phenotype, vehicle1, vehicle2, offset1, offset2](){return phenotype->inter2OptAndEvaluate(vehicle1, vehicle2, offset1, offset2);});
                }
            }
        }
    }

    vector<boost::unique_future<T::result_type>> futures;
    for(auto& t: tasks){
        futures.push_back(t.get_future());
        post(*pool, std::move(t));
    }

    for(auto& future: boost::when_all(futures.begin(), futures.end()).get()){
        neighbours.push_back(future.get());
    }

    set<shared_ptr<PermutationPhenotype>, decltype(&PermutationPhenotype::compareByPenaltyValue)> neighbourSet(&PermutationPhenotype::compareByPenaltyValue);

    for(const auto& n: neighbours){
        neighbourSet.insert(n);
    }

    auto best = *(neighbourSet.begin());
    if(best->isViableSolution() && !phenotype->viableSolution || best->viableSolution && best->penaltyVal < phenotype->penaltyVal){
        return best;
    }

    auto ret = make_shared<PermutationPhenotype>(phenotype->chromosome, vrp);
    ret->evaluate();
    return ret;
}
