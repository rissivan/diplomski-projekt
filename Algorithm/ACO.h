//
// Created by ivan on 12/31/21.
//

#ifndef PROJEKT_ACO_H
#define PROJEKT_ACO_H

#include "AbstractAlgorithm.h"
#include "../Phenotype/PermutationPhenotype.h"

class ACO: public AbstractAlgorithm {
private:
    std::vector<int> demands;
    std::vector<std::vector<double>> distances;
    std::vector<std::vector<double>> pheromones;
    std::vector<std::vector<double>> heuristics;
    std::vector<std::vector<double>> savings;           //ujedno brine da nema bespotrebnih povrataka u depot
    std::shared_ptr<PermutationPhenotype> bestSolution;
    int noIterations = 100000;
    int populationSize = 30;
    int noUpdaters = 3;
    double theta = 80;
    double initialPheromone = 1e-6;
    double rho = 0.8;
    double alpha = 1;
    double beta = 1;
    double gamma = 3;
    double feasibilityFormula(int index1, int index2);
    std::shared_ptr<PermutationPhenotype> antWalk();
    double getFromTable(std::vector<std::vector<double>>& table, int i, int j);
    int getFromTable(std::vector<int>& table, int i);
    void addInTable(std::vector<std::vector<double>>& table, int i, int j, double value);
    void pheromoneReduction(double lAvg);
    void updatePheromone(std::vector<std::shared_ptr<PermutationPhenotype>> paths, double lAvg);
public:
    explicit ACO(VRP& vrp);
    void run() override;
    void printBest(std::ostream &os) override;
    void setInitialPheromone(double initialPheromone);
    void setRho(double rho);
    void setAlpha(double alpha);
    void setBeta(double beta);
    void setGamma(double gamma);
};


#endif //PROJEKT_ACO_H
