//
// Created by ivan on 12/30/21.
//

#include "PSO.h"
#include "../Util/Util.h"

#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/future.hpp>
#include <iostream>

using namespace std;


PSO::PSO(VRP &vrp) : AbstractAlgorithm(vrp) {

}

void PSO::run() {
    particles.reserve(populationSize);
    bestLocal.reserve(populationSize);
    velocities.reserve(populationSize);
    for(int i = 0; i < populationSize; i++){
        shared_ptr<RKRealPhenotype> p = make_shared<RKRealPhenotype>(this->vrp, constructionMode);
        p->evaluate();
        if(i == 0 || p->getPenaltyVal() < bestGlobal->getPenaltyVal()){
            bestGlobal = p;
        }
        particles.push_back(p);
        bestLocal.push_back(p);
        auto size = p->chromosome.size();
        vector<double> v;
        v.reserve(size);
        for(int j = 0; j < size; j++){
            v.push_back(0);
        }
        velocities.push_back(v);
    }

    using T = boost::packaged_task<shared_ptr<RKRealPhenotype>>;

    int repetitionCounter = 0;

    for(int G = 0; G < noGenerations; G++) {
        if((G + 1) % (noGenerations / 1000) == 0)
            cout << fixed << "Generation " << G + 1 << "; value: " << bestGlobal->getPenaltyVal() << ": "  << endl << *bestGlobal << endl << endl;

        vector<T> tasks;
        vector<shared_ptr<RKRealPhenotype>> newParticles;

        tasks.reserve(populationSize);
        for(int i = 0; i < populationSize; i++){
            tasks.emplace_back([this, i](){return this->singleIteration(i);});
        }

        vector<boost::unique_future<T::result_type>> futures;
        for(auto& t: tasks){
            futures.push_back(t.get_future());
            post(*pool, std::move(t));
        }

        for(auto& future: boost::when_all(futures.begin(), futures.end()).get()){
            newParticles.push_back(future.get());
        }

        bool foundNewBest = false;

        for(int i = 0; i < populationSize; i++){
//            cout << particles[i]->getPenaltyVal() << endl;
            particles[i] = newParticles[i];
            if(particles[i]->getPenaltyVal() < bestLocal[i]->getPenaltyVal()){
                bestLocal[i] = particles[i];
                foundNewBest = true;
                repetitionCounter = 0;
                if(particles[i]->getPenaltyVal() < bestGlobal->getPenaltyVal()){
                    bestGlobal = particles[i];
                }
            }
        }

        if(outputToFileStream){
            os << G + 1 << " " << bestGlobal->getPenaltyVal() << endl;
        }

        if(!foundNewBest){
            repetitionCounter++;
            if(repetitionCounter >= maxNoRepetitions){
                cout << fixed << "Generation " << G + 1 << "; value: " << bestGlobal->getPenaltyVal() << ": "  << endl << *bestGlobal << endl << endl;
                break;
            }
        }
    }
    if(outputToFileStream){
        os.flush();
    }
}

std::shared_ptr<RKRealPhenotype> PSO::singleIteration(int index) {
    auto size = particles[index]->chromosome.size();
    for(int i = 0; i < size; i++){
        double rl = getRandom01();
        double rg = getRandom01();
        velocities[index][i] = w * velocities[index][i] +
                               cl * rl * (bestLocal[index]->realValuesChromosome[i] - particles[index]->realValuesChromosome[i]) +
                               cg * rg * (bestGlobal->realValuesChromosome[i] - particles[index]->realValuesChromosome[i]);
    }
    vector<double> values(size);
    for(int i = 0; i < size; i++){
        values[i] = particles[index]->realValuesChromosome[i] + velocities[index][i];
    }
    shared_ptr<RKRealPhenotype> newParticle = make_shared<RKRealPhenotype>(values, vrp, normalize);
//    cout << *particles[index] << endl << *newParticle << endl << endl << endl;
    newParticle->evaluate();

    if(searchNeighbourhood){
        auto n = newParticle->generateRandomNeighbour();
        n->evaluate();
        if(RKRealPhenotype::compareByPenaltyValue(n, newParticle)){
            newParticle = n;
        }
    }

    return newParticle;
}

void PSO::printBest(ostream &os) {
    os << *bestGlobal << endl;
    os << fixed << bestGlobal->getPenaltyVal() << endl;
}

void PSO::setW(double w) {
    PSO::w = w;
}

void PSO::setCl(double cL) {
    PSO::cl = cL;
}

void PSO::setCg(double cG) {
    PSO::cg = cG;
}
