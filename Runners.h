//
// Created by ivan on 5/10/22.
//

#ifndef PROJEKT_RUNNERS_H
#define PROJEKT_RUNNERS_H



void runGA();
void runDE();
void runBSA();
void runPSO();
void runACO();
void runABC();
void runTS();
void runSA();

void runPSO2ndRun();
void runACO2ndRun();

void runGA2();
void runDE2();
void runBSA2();
void runPSO2();
void runACO2();
void runABC2();
void runTS2();
void runSA2();

void runGAThreads();

#endif //PROJEKT_RUNNERS_H
