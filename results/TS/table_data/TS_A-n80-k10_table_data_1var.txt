\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c|c|c|c|c|c|}
		\hline Optimum: & \multicolumn{6}{c|}{Veličina tabu liste} \\
		\hhline{|>{\arrayrulecolor{white}}->{\arrayrulecolor{black}}||*{6}{-|}} 1766.5 & 1 & 2 & 5 & 10 & 20 & 50 \\
		\hhline{|=||=|=|=|=|=|=|} vrijednost EF & 1825.425 & \cellcolor{green!25}1820.732 & 1869.268 & 1860.262 & 1826.859 & 1864.675 \\
		\hline
	\end{tabular}
	\caption{Najbolje vrijednosti evaluacijske funkcije TS na instanci A-n80-k10.vrp.}
	\label{tab:table-TS-n80-res}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|c|c|}
		\hline & \multicolumn{6}{c|}{Veličina tabu liste} \\
		\hhline{|>{\arrayrulecolor{gray!25}}->{\arrayrulecolor{black}}||*{6}{-|}}  & 1 & 2 & 5 & 10 & 20 & 50 \\
		\hhline{|=||=|=|=|=|=|=|} duljina izvođenja [\texttt{ms}] & 52022.9 & 52003.9 & 52053.7 & 52043.7 & \cellcolor{white}51969.1 & 52009.1 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja (u $ms$) TS na instanci A-n80-k10.vrp.}
	\label{tab:table-TS-n80-time}
\end{table}

