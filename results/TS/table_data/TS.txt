\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c||c|c|c|c|c|c|}
		\hline & & \multicolumn{6}{c|}{Veličina tabu liste} \\
		\hhline{|>{\arcw}->{\arcb}||>{\arcw}->{\arcb}||*{6}{-|}} \multirow{-2}{*}{Instanca} & \multirow{-2}{*}{Optimum}  & 1 & 2 & 5 & 10 & 20 & 50 \\
		\hhline{|=||=||=|=|=|=|=|=|} A-n32-k5 & 787.082 & 795.199 & \ccg 787.082 & 797.451 & \ccg 787.082 & \ccg 787.082 & \ccg 787.082 \\
		\hline A-n60-k9 & 1355.799 & 1417.746 & 1419.485 & \ccg 1378.975 & 1402.098 & 1390.180 & 1385.862 \\
		\hline A-n80-k10 & 1766.5 & 1825.425 & \ccg 1820.732 & 1869.268 & 1860.262 & 1826.859 & 1864.675 \\
		\hline
	\end{tabular}
	\caption{Vrijednosti najboljih rješenja TS}
	\label{tab:table-TS-res}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|c|c|}
		\hline & \multicolumn{6}{c|}{Veličina tabu liste} \\
		\hhline{|>{\arcg}->{\arcb}||*{6}{-|}} \multirow{-2}{*}{Instanca} & 1 & 2 & 5 & 10 & 20 & 50 \\
		\hhline{|=||=|=|=|=|=|=|} A-n32-k5 & 9355.4 & 9251.6 & 9303.7 & 9265.8 & \ccw 9241.5 & 9282.8 \\
		\hline A-n60-k9 & 27347.0 & 27393.9 & 27375.7 & \ccw 27326.9 & 27411.0 & 27356.1 \\
		\hline A-n80-k10 & 52022.9 & 52003.9 & 52053.7 & 52043.7 & \ccw 51969.1 & 52009.1 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja TS(u $ms$)}
	\label{tab:table-TS-time}
\end{table}

\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c||*{2}{wc{\mylen}|}}
		\hline & & \multicolumn{2}{c|}{Početno rješenje} \\
		\hhline{|~||~||*{2}{-|}} \multirow{-2}{*}{Instanca} & \multirow{-2}{*}{Optimum} & NV & PP \\
		\hhline{|=||=||=|=|} A-n32-k5 & 787.082 & \ccg 787.082 & \ccg 787.082 \\
		\hhline{|-||-||*{2}{-|}} A-n60-k9 & 1355.799 & \ccg 1389.184 & 1420.289 \\
		\hhline{|-||-||*{2}{-|}} A-n80-k10 & 1766.5 & \ccg 1860.586 & 1883.021 \\
		\hline
	\end{tabular}
	\caption{Vrijednosti najboljih rješenja TS u ovisnosti o lokalnom pretraživanju i metodi generiranja početne populacije}
	\label{tab:table-TS-res-gcrf}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||*{2}{wc{\mylen}|}}
		\hline & \multicolumn{2}{c|}{Početno rješenje} \\
		\hhline{|>\arcg->\arcb||*{2}{-|}} \multirow{-2}{*}{Instanca} & NV & PP \\
		\hhline{|=||=|=|} A-n32-k5 & \ccw 9699.6 & 9964.1 \\
		\hhline{|-||*{2}{-|}} A-n60-k9 & 27464.5 & \ccw 27398.0 \\
		\hhline{|-||*{2}{-|}} A-n80-k10 & 51818.4 & \ccw 51806.6 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja TS (u $ms$) u ovisnosti o lokalnom pretraživanju i metodi generiranja početne populacije}
	\label{tab:table-TS-time-gcrf}
\end{table}

