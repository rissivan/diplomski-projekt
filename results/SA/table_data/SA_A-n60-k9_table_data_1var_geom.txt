\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c|c|c|c|}
		\hline Optimum: & \multicolumn{4}{c|}{$\beta$} \\
		\hhline{|>{\arrayrulecolor{white}}->{\arrayrulecolor{black}}||*{4}{-|}} 1355.799 & ('geom', 0.99) & ('geom', 0.999) & ('geom', 0.9999) & ('geom', 0.99995) \\
		\hhline{|=||=|=|=|=|} vrijednost EF & 1997.173 & 1472.283 & \cellcolor{green!25}1365.202 & 1379.586 \\
		\hline
	\end{tabular}
	\caption{Najbolje vrijednosti evaluacijske funkcije SA uz geometrijski dekrement temperature na instanci A-n60-k9.vrp.}
	\label{tab:table-SA-n60-geom-res}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|}
		\hline & \multicolumn{4}{c|}{$\beta$} \\
		\hhline{|>{\arrayrulecolor{gray!25}}->{\arrayrulecolor{black}}||*{4}{-|}}  & ('geom', 0.99) & ('geom', 0.999) & ('geom', 0.9999) & ('geom', 0.99995) \\
		\hhline{|=||=|=|=|=|} duljina izvođenja [\texttt{ms}] & \cellcolor{white}243.9 & 2288.5 & 22417.3 & 44719.3 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja (u $ms$) SA uz geometrijski dekrement temperature na instanci A-n60-k9.vrp.}
	\label{tab:table-SA-n60-geom-time}
\end{table}

