\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c|c|c|c|}
		\hline Optimum: & \multicolumn{4}{c|}{B} \\
		\hhline{|>{\arrayrulecolor{white}}->{\arrayrulecolor{black}}||*{4}{-|}} 1766.5 & ('lin', 0.005) & ('lin', 0.01) & ('lin', 0.05) & ('lin', 0.1) \\
		\hhline{|=||=|=|=|=|} vrijednost EF & \cellcolor{green!25}1875.438 & 1944.877 & 2287.873 & 2511.054 \\
		\hline
	\end{tabular}
	\caption{Najbolje vrijednosti evaluacijske funkcije SA uz linearni dekrement temperature na instanci A-n80-k10.vrp.}
	\label{tab:table-SA-n80-lin-res}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|}
		\hline & \multicolumn{4}{c|}{B} \\
		\hhline{|>{\arrayrulecolor{gray!25}}->{\arrayrulecolor{black}}||*{4}{-|}}  & ('lin', 0.005) & ('lin', 0.01) & ('lin', 0.05) & ('lin', 0.1) \\
		\hhline{|=||=|=|=|=|} duljina izvođenja [\texttt{ms}] & 21614.9 & 10868.1 & 2243.5 & \cellcolor{white}1163.7 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja (u $ms$) SA uz linearni dekrement temperature na instanci A-n80-k10.vrp.}
	\label{tab:table-SA-n80-lin-time}
\end{table}

