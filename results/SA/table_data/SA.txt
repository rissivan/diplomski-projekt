\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c||c|c|c|c|}
		\hline & & \multicolumn{4}{c|}{B} \\
		\hhline{|>{\arcw}->{\arcb}||>{\arcw}->{\arcb}||*{4}{-|}} \multirow{-2}{*}{Instanca} & \multirow{-2}{*}{Optimum}  & 0.005 & 0.01 & 0.05 & 0.1 \\
		\hhline{|=||=||=|=|=|=|} A-n32-k5 & 787.082 & \ccg 787.082 & \ccg 787.082 & 791.079 & 798.377 \\
		\hline A-n60-k9 & 1355.799 & \ccg 1367.357 & 1411.752 & 1563.973 & 1700.560 \\
		\hline A-n80-k10 & 1766.5 & \ccg 1875.438 & 1944.877 & 2287.873 & 2511.054 \\
		\hline
	\end{tabular}
	\caption{Vrijednosti najboljih rješenja SA}
	\label{tab:table-SA-res-lin}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|}
		\hline & \multicolumn{4}{c|}{B} \\
		\hhline{|>{\arcg}->{\arcb}||*{4}{-|}} \multirow{-2}{*}{Instanca} & 0.005 & 0.01 & 0.05 & 0.1 \\
		\hhline{|=||=|=|=|=|} A-n32-k5 & 19750.1 & 9894.3 & 1978.0 & \ccw 997.4 \\
		\hline A-n60-k9 & 21208.4 & 10368.9 & 2105.6 & \ccw 1071.1 \\
		\hline A-n80-k10 & 21614.9 & 10868.1 & 2243.5 & \ccw 1163.7 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja SA(u $ms$)}
	\label{tab:table-SA-time-lin}
\end{table}

\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c||c|c|c|c|}
		\hline & & \multicolumn{4}{c|}{$\beta$} \\
		\hhline{|>{\arcw}->{\arcb}||>{\arcw}->{\arcb}||*{4}{-|}} \multirow{-2}{*}{Instanca} & \multirow{-2}{*}{Optimum}  & 0.99 & 0.999 & 0.9999 & 0.99995 \\
		\hhline{|=||=||=|=|=|=|} A-n32-k5 & 787.082 & 848.980 & \ccg 787.082 & \ccg 787.082 & \ccg 787.082 \\
		\hline A-n60-k9 & 1355.799 & 1997.173 & 1472.283 & \ccg 1365.202 & 1379.586 \\
		\hline A-n80-k10 & 1766.5 & 2953.029 & 2146.190 & 1872.385 & \ccg 1815.205 \\
		\hline
	\end{tabular}
	\caption{Vrijednosti najboljih rješenja SA}
	\label{tab:table-SA-res-geom}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|}
		\hline & \multicolumn{4}{c|}{$\beta$} \\
		\hhline{|>{\arcg}->{\arcb}||*{4}{-|}} \multirow{-2}{*}{Instanca} & 0.99 & 0.999 & 0.9999 & 0.99995 \\
		\hhline{|=||=|=|=|=|} A-n32-k5 & \ccw 207.8 & 2070.6 & 21411.5 & 42621.4 \\
		\hline A-n60-k9 & \ccw 243.9 & 2288.5 & 22417.3 & 44719.3 \\
		\hline A-n80-k10 & \ccw 294.7 & 2438.1 & 23444.4 & 46794.3 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja SA(u $ms$)}
	\label{tab:table-SA-time-geom}
\end{table}

\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c||c|c|c|c|}
		\hline & & \multicolumn{4}{c|}{$\alpha$} \\
		\hhline{|>{\arcw}->{\arcb}||>{\arcw}->{\arcb}||*{4}{-|}} \multirow{-2}{*}{Instanca} & \multirow{-2}{*}{Optimum}  & 0.001 & 0.005 & 0.01 & 0.05 \\
		\hhline{|=||=||=|=|=|=|} A-n32-k5 & 787.082 & \ccg 787.082 & \ccg 787.082 & \ccg 787.082 & 853.302 \\
		\hline A-n60-k9 & 1355.799 & \ccg 1381.630 & 1407.318 & 1487.901 & 1812.052 \\
		\hline A-n80-k10 & 1766.5 & \ccg 1843.386 & 1988.423 & 2155.665 & 2597.973 \\
		\hline
	\end{tabular}
	\caption{Vrijednosti najboljih rješenja SA}
	\label{tab:table-SA-res-vslow}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|}
		\hline & \multicolumn{4}{c|}{$\alpha$} \\
		\hhline{|>{\arcg}->{\arcb}||*{4}{-|}} \multirow{-2}{*}{Instanca} & 0.001 & 0.005 & 0.01 & 0.05 \\
		\hhline{|=||=|=|=|=|} A-n32-k5 & 19683.6 & 3954.3 & 1977.1 & \ccw 397.7 \\
		\hline A-n60-k9 & 20594.0 & 4167.7 & 2106.4 & \ccw 440.0 \\
		\hline A-n80-k10 & 23388.9 & 4395.3 & 2250.6 & \ccw 502.1 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja SA(u $ms$)}
	\label{tab:table-SA-time-vslow}
\end{table}

\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c||*{2}{wc{\mylen}|}}
		\hline & & \multicolumn{2}{c|}{Početno rješenje} \\
		\hhline{|~||~||*{2}{-|}} \multirow{-2}{*}{Instanca} & \multirow{-2}{*}{Optimum} & NV & PP \\
		\hhline{|=||=||=|=|} A-n32-k5 & 787.082 & \ccg 787.082 & \ccg 787.082 \\
		\hhline{|-||-||*{2}{-|}} A-n60-k9 & 1355.799 & 1380.278 & \ccg 1372.760 \\
		\hhline{|-||-||*{2}{-|}} A-n80-k10 & 1766.5 & 1837.589 & \ccg 1832.575 \\
		\hline
	\end{tabular}
	\caption{Vrijednosti najboljih rješenja SA u ovisnosti o lokalnom pretraživanju i metodi generiranja početne populacije}
	\label{tab:table-SA-res-gcrf}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||*{2}{wc{\mylen}|}}
		\hline & \multicolumn{2}{c|}{Početno rješenje} \\
		\hhline{|>\arcg->\arcb||*{2}{-|}} \multirow{-2}{*}{Instanca} & NV & PP \\
		\hhline{|=||=|=|} A-n32-k5 & \ccw 2025.7 & 2140.9 \\
		\hhline{|-||*{2}{-|}} A-n60-k9 & 26477.1 & \ccw 25260.9 \\
		\hhline{|-||*{2}{-|}} A-n80-k10 & \ccw 51181.3 & 51802.5 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja SA (u $ms$) u ovisnosti o lokalnom pretraživanju i metodi generiranja početne populacije}
	\label{tab:table-SA-time-gcrf}
\end{table}

