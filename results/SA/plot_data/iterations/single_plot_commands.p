set title "Kvaliteta rješenja SA tijekom iteracija"
set xrange [0:216391]
set xlabel "Iteracija"
set ylabel "Vrijednost evaluacijske funkcije"
set term png size 1200,675
set output '../../plots/SA_iterations.png'
plot 787.808 title "Optimum problema manjih dimenzija" lt rgb "#FF0000", 'SA_A-n32-k5_iterations.txt' t "Vrijednost evaluacijske funkcije SA na problemu manjih dimenzija" w lines lt rgb "#994C00",\
1355.799 title "Optimum problema srednjih dimenzija" lt rgb "#00FF00", 'SA_A-n60-k9_iterations.txt' t "Vrijednost evaluacijske funkcije SA na problemu srednjih dimenzija" w lines lt rgb "#00994C",\
1766.5 title "Optimum problema većih dimenzija" lt rgb "#0000FF", 'SA_A-n80-k10_iterations.txt' t "Vrijednost evaluacijske funkcije SA na problemu većih dimenzija" w lines lt rgb "#4C0099"
