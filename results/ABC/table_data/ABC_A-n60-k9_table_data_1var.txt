\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c|c|c|c|c|c|}
		\hline Optimum: & \multicolumn{6}{c|}{Veličina populacije} \\
		\hhline{|>{\arrayrulecolor{white}}->{\arrayrulecolor{black}}||*{6}{-|}} 1355.799 & 10 & 20 & 30 & 50 & 80 & 100 \\
		\hhline{|=||=|=|=|=|=|=|} vrijednost EF & 1387.322 & 1364.500 & 1360.593 & 1360.593 & \cellcolor{green!25}1355.799 & \cellcolor{green!25}1355.799 \\
		\hline
	\end{tabular}
	\caption{Najbolje vrijednosti evaluacijske funkcije ABC na instanci A-n60-k9.vrp.}
	\label{tab:table-ABC-n60-res}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|c|c|}
		\hline & \multicolumn{6}{c|}{Veličina populacije} \\
		\hhline{|>{\arrayrulecolor{gray!25}}->{\arrayrulecolor{black}}||*{6}{-|}}  & 10 & 20 & 30 & 50 & 80 & 100 \\
		\hhline{|=||=|=|=|=|=|=|} duljina izvođenja [\texttt{ms}] & \cellcolor{white}15845.0 & 23019.5 & 28546.0 & 40067.2 & 55718.1 & 66129.8 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja (u $ms$) ABC na instanci A-n60-k9.vrp.}
	\label{tab:table-ABC-n60-time}
\end{table}

