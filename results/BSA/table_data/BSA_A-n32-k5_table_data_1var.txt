\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c|c|c|c|c|c|}
		\hline Optimum: & \multicolumn{6}{c|}{F} \\
		\hhline{|>{\arrayrulecolor{white}}->{\arrayrulecolor{black}}||*{6}{-|}} 787.808 & 0.1 & 0.25 & 0.5 & 1 & 1.5 & 2 \\
		\hhline{|=||=|=|=|=|=|=|} vrijednost EF & 1347.740 & 1390.390 & 1355.580 & 1440.480 & \cellcolor{green!25}1207.010 & 1207.240 \\
		\hline
	\end{tabular}
	\caption{Najbolje vrijednosti evaluacijske funkcije BSA na instanci A-n32-k5.vrp.}
	\label{tab:table-BSA-n32-res}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|c|c|}
		\hline & \multicolumn{6}{c|}{F} \\
		\hhline{|>{\arrayrulecolor{gray!25}}->{\arrayrulecolor{black}}||*{6}{-|}}  & 0.1 & 0.25 & 0.5 & 1 & 1.5 & 2 \\
		\hhline{|=||=|=|=|=|=|=|} duljina izvođenja [\texttt{ms}] & 12038.1 & 12799.1 & 14841.6 & \cellcolor{white}8566.7 & 12064.1 & 14915.5 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja (u $ms$) BSA na instanci A-n32-k5.vrp.}
	\label{tab:table-BSA-n32-time}
\end{table}

