\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c|c|c|c|c|c|}
		\hline Optimum: & \multicolumn{6}{c|}{F} \\
		\hhline{|>{\arrayrulecolor{white}}->{\arrayrulecolor{black}}||*{6}{-|}} 1766.5 & 0.1 & 0.25 & 0.5 & 1 & 1.5 & 2 \\
		\hhline{|=||=|=|=|=|=|=|} vrijednost EF & 4270.100 & 4166.260 & 4190.280 & \cellcolor{green!25}4072.480 & 4325.190 & 4307.750 \\
		\hline
	\end{tabular}
	\caption{Najbolje vrijednosti evaluacijske funkcije BSA na instanci A-n80-k10.vrp.}
	\label{tab:table-BSA-n80-res}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|c|c|}
		\hline & \multicolumn{6}{c|}{F} \\
		\hhline{|>{\arrayrulecolor{gray!25}}->{\arrayrulecolor{black}}||*{6}{-|}}  & 0.1 & 0.25 & 0.5 & 1 & 1.5 & 2 \\
		\hhline{|=||=|=|=|=|=|=|} duljina izvođenja [\texttt{ms}] & 18230.3 & 23658.8 & 22176.5 & 15479.5 & \cellcolor{white}15417.5 & 15895.6 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja (u $ms$) BSA na instanci A-n80-k10.vrp.}
	\label{tab:table-BSA-n80-time}
\end{table}

