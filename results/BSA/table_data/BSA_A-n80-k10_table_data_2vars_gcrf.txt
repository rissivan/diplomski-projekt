\begin{table}[ht!]
	\centering
	\begin{tabular}{|c|c||c|c|}
		\hline \multicolumn{2}{|c||}{Optimum:} & \multicolumn{2}{c|}{Pretraživanje susjedstva} \\
		\hhline{|~~||*{2}{-|}} \multicolumn{2}{|c||}{1766.5} & NE & DA \\
		\hhline{|=|=||=|=|} & NV & 4125.770 & \cellcolor{green!25}1893.750 \\
		\hhline{|~|-||*{2}{-|}}\multirow{-2}{*}{Metoda generiranja početnog rješenja} & PP & 4147.730 & 1912.030 \\
		\hline
	\end{tabular}
	\caption{Najbolje vrijednosti evaluacijske funkcije BSA na instanci A-n80-k10.vrp.}
	\label{tab:table-BSA-n80-res-gcrf}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c|c||c|c|}
		\hline \multicolumn{2}{|c||}{} & \multicolumn{2}{c|}{Pretraživanje susjedstva} \\
		\hhline{|>{\arrayrulecolor{gray!25}}-->{\arrayrulecolor{black}}||*{2}{-|}} \multicolumn{2}{|c||}{} & NE & DA \\
		\hhline{|=|=||=|=|} & NV & \cellcolor{white}15917.8 & 30175.9 \\
		\hhline{|>{\arrayrulecolor{gray!25}}->{\arrayrulecolor{black}}|-||*{2}{-|}}\multirow{-2}{*}{Metoda generiranja početnog rješenja} & PP & 20968.1 & 36389.5 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja (u $ms$) BSA na instanci A-n80-k10.vrp.}
	\label{tab:table-BSA-n80-time-gcrf}
\end{table}

