set title "Kvaliteta rješenja PSO tijekom iteracija na problemu manjih dimenzija"
set xlabel "Iteracija"
set ylabel "Vrijednost evaluacijske funkcije"
set term png
set output '../../plots/PSO_A-n32-k5_iterations.png'
plot 'PSO_A-n32-k5_iterations.txt' t "Vrijednost evaluacijske funkcije PSO" w lines, 787.808 title "Optimum"

set title "Kvaliteta rješenja PSO tijekom iteracija na problemu srednjih dimenzija"
set xlabel "Iteracija"
set ylabel "Vrijednost evaluacijske funkcije"
set xrange [0:68586]
set term png
set output '../../plots/PSO_A-n60-k9_iterations.png'
plot 'PSO_A-n60-k9_iterations.txt' t "Vrijednost evaluacijske funkcije PSO" w lines, 1355.799 title "Optimum"

set title "Kvaliteta rješenja PSO tijekom iteracija na problemu većih dimenzija"
set xlabel "Iteracija"
set ylabel "Vrijednost evaluacijske funkcije"
set xrange [0:95546]
set term png
set output '../../plots/PSO_A-n80-k10_iterations.png'
plot 'PSO_A-n80-k10_iterations.txt' t "Vrijednost evaluacijske funkcije PSO" w lines, 1766.5 title "Optimum"
