set title "Kvaliteta rješenja DE tijekom iteracija"
set xlabel "Iteracija"
set ylabel "Vrijednost evaluacijske funkcije"
set term png size 1200,675
set output '../../plots/DE_iterations.png'
plot 787.808 title "Optimum problema manjih dimenzija" lt rgb "#FF0000", 'DE_A-n32-k5_iterations.txt' t "Vrijednost evaluacijske funkcije DE na problemu manjih dimenzija" w lines lt rgb "#994C00",\
1355.799 title "Optimum problema srednjih dimenzija" lt rgb "#00FF00", 'DE_A-n60-k9_iterations.txt' t "Vrijednost evaluacijske funkcije DE na problemu srednjih dimenzija" w lines lt rgb "#00994C",\
1766.5 title "Optimum problema većih dimenzija" lt rgb "#0000FF", 'DE_A-n80-k10_iterations.txt' t "Vrijednost evaluacijske funkcije DE na problemu većih dimenzija" w lines lt rgb "#4C0099"
