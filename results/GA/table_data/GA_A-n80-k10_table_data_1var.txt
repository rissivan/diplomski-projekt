\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c|c|c|c|c|}
		\hline Optimum: & \multicolumn{5}{c|}{Vjerojatnost mutacije} \\
		\hhline{|>{\arrayrulecolor{white}}->{\arrayrulecolor{black}}||*{5}{-|}} 1766.5 & 0.0025 & 0.005 & 0.01 & 0.02 & 0.03 \\
		\hhline{|=||=|=|=|=|=|} vrijednost EF & 2014.550 & 1966.706 & 1950.774 & 1936.439 & \cellcolor{green!25}1906.272 \\
		\hline
	\end{tabular}
	\caption{Najbolje vrijednosti evaluacijske funkcije GA na instanci A-n80-k10.vrp.}
	\label{tab:table-GA-n80-res}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|c|}
		\hline & \multicolumn{5}{c|}{Vjerojatnost mutacije} \\
		\hhline{|>{\arrayrulecolor{gray!25}}->{\arrayrulecolor{black}}||*{5}{-|}}  & 0.0025 & 0.005 & 0.01 & 0.02 & 0.03 \\
		\hhline{|=||=|=|=|=|=|} duljina izvođenja [\texttt{ms}] & 32412.2 & 32470.3 & \cellcolor{white}31788.5 & 32073.9 & 32394.2 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja (u $ms$) GA na instanci A-n80-k10.vrp.}
	\label{tab:table-GA-n80-time}
\end{table}

