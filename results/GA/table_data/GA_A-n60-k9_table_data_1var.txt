\begin{table}[ht!]
	\centering
	\begin{tabular}{|c||c|c|c|c|c|}
		\hline Optimum: & \multicolumn{5}{c|}{Vjerojatnost mutacije} \\
		\hhline{|>{\arrayrulecolor{white}}->{\arrayrulecolor{black}}||*{5}{-|}} 1355.799 & 0.0025 & 0.005 & 0.01 & 0.02 & 0.03 \\
		\hhline{|=||=|=|=|=|=|} vrijednost EF & 1477.140 & 1458.651 & 1441.409 & 1437.864 & \cellcolor{green!25}1435.694 \\
		\hline
	\end{tabular}
	\caption{Najbolje vrijednosti evaluacijske funkcije GA na instanci A-n60-k9.vrp.}
	\label{tab:table-GA-n60-res}
\end{table}

\begin{table}[ht!]
	\centering
	\rowcolors{1}{gray!25}{gray!25}
	\begin{tabular}{|c||c|c|c|c|c|}
		\hline & \multicolumn{5}{c|}{Vjerojatnost mutacije} \\
		\hhline{|>{\arrayrulecolor{gray!25}}->{\arrayrulecolor{black}}||*{5}{-|}}  & 0.0025 & 0.005 & 0.01 & 0.02 & 0.03 \\
		\hhline{|=||=|=|=|=|=|} duljina izvođenja [\texttt{ms}] & 30291.0 & 30145.1 & 30087.9 & 29877.7 & \cellcolor{white}29849.1 \\
		\hline
	\end{tabular}
	\caption{Prosječna duljina izvođenja (u $ms$) GA na instanci A-n60-k9.vrp.}
	\label{tab:table-GA-n60-time}
\end{table}

