//
// Created by ivan on 11/23/21.
//

#ifndef PROJEKT_RKREALPHENOTYPE_H
#define PROJEKT_RKREALPHENOTYPE_H

#include <vector>
#include <random>
#include <memory>

#include "../VehicleRoutingProblem/VRP.h"
#include "BasePhenotype.h"
#include "PermutationPhenotype.h"

class RKRealPhenotype: public BasePhenotype {
private:
    std::vector<double> realValuesChromosome;
    void calculateRepresentation();
public:
    explicit RKRealPhenotype(VRP& vrp);
    RKRealPhenotype(VRP& vrp, BasePhenotype::ConstructionMode constructionMode);
    RKRealPhenotype(std::vector<double> chr, VRP& vrp, bool normalize);
    friend std::ostream& operator<<(std::ostream& os, const RKRealPhenotype& phenotype);
    static bool compareByPenaltyValue(const std::shared_ptr<RKRealPhenotype>& p1, const std::shared_ptr<RKRealPhenotype>& p2);
    std::shared_ptr<RKRealPhenotype> generateRandomNeighbour();
    friend class DE;
    friend class BSA;
    friend class PSO;
};


#endif //PROJEKT_RKREALPHENOTYPE_H
