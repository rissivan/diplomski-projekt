//
// Created by ivan on 12/29/21.
//

#ifndef PROJEKT_BASEPHENOTYPE_H
#define PROJEKT_BASEPHENOTYPE_H

#include <boost/asio.hpp>
#include <thread>
#include <memory>
#include "../VehicleRoutingProblem/VRP.h"

class BasePhenotype {
protected:
    constexpr const static double PENALTY_FACTOR = 10;
    double penaltyVal{};
    VRP& vrp;
    bool viableSolution{};
    std::vector<int> chromosome;
public:
    explicit BasePhenotype(VRP& vrp);
    void evaluate();
    [[nodiscard]] double getPenaltyVal() const;
    [[nodiscard]] bool isViableSolution() const;

    enum ConstructionMode{
        RANDOM,
        RANDOM_FEASIBLE,
        GUIDED_CLASSIC,
        GUIDED_DENSITY
    };
};


#endif //PROJEKT_BASEPHENOTYPE_H
