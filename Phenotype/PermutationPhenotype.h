//
// Created by ivan on 11/10/21.
//

#ifndef PROJEKT_PERMUTATIONPHENOTYPE_H
#define PROJEKT_PERMUTATIONPHENOTYPE_H

#include <vector>
#include <random>
#include <memory>

#include "../VehicleRoutingProblem/VRP.h"
#include "BasePhenotype.h"
#include "RKRealPhenotype.h"

class PermutationPhenotype: public BasePhenotype {
private:
    void swapMutation();
    void insertMutation();
    void scrambleMutation();
    std::shared_ptr<PermutationPhenotype> partiallyMappedCrossover(const std::shared_ptr<PermutationPhenotype>& other);
    std::shared_ptr<PermutationPhenotype> orderCrossover1(const std::shared_ptr<PermutationPhenotype>& other);
    std::shared_ptr<PermutationPhenotype> orderCrossover2(const std::shared_ptr<PermutationPhenotype>& other);

    std::shared_ptr<PermutationPhenotype> intraSwap(int vehicle, int offset1, int offset2);
    std::shared_ptr<PermutationPhenotype> interSwap(int vehicle1, int vehicle2, int offset1, int offset2);
    std::shared_ptr<PermutationPhenotype> intraShift(int vehicle, int customerOffset, int shiftOffset);
    std::shared_ptr<PermutationPhenotype> interShift(int customerOffset, int shiftOffset);
    std::shared_ptr<PermutationPhenotype> intra2Opt(int vehicle, int offset1, int offset2);
    std::shared_ptr<PermutationPhenotype> inter2Opt(int vehicle1, int vehicle2, int offset1, int offset2);

    std::shared_ptr<PermutationPhenotype> intraSwapAndEvaluate(int vehicle, int offset1, int offset2);
    std::shared_ptr<PermutationPhenotype> interSwapAndEvaluate(int vehicle1, int vehicle2, int offset1, int offset2);
    std::shared_ptr<PermutationPhenotype> intraShiftAndEvaluate(int vehicle, int customerOffset, int shiftOffset);
    std::shared_ptr<PermutationPhenotype> interShiftAndEvaluate(int customerOffset, int shiftOffset);
    std::shared_ptr<PermutationPhenotype> intra2OptAndEvaluate(int vehicle, int offset1, int offset2);
    std::shared_ptr<PermutationPhenotype> inter2OptAndEvaluate(int vehicle1, int vehicle2, int offset1, int offset2);

    std::vector<int> getVehicleIndices();
    std::vector<int> getRouteLengths();

    int rclSize = 4;

public:

    explicit PermutationPhenotype(VRP& vrp);
    PermutationPhenotype(std::vector<int> chr, VRP& vrp);
    PermutationPhenotype(VRP &vrp, PermutationPhenotype::ConstructionMode mode);
    friend std::ostream& operator<<(std::ostream& os, const PermutationPhenotype& phenotype);
    static bool compareByPenaltyValue(const std::shared_ptr<PermutationPhenotype>& p1, const std::shared_ptr<PermutationPhenotype>& p2);
    friend bool operator==(const std::shared_ptr<PermutationPhenotype>& p1, const std::shared_ptr<PermutationPhenotype>& p2);
    void mutate();
    std::shared_ptr<PermutationPhenotype> crossover(const std::shared_ptr<PermutationPhenotype>& other);

    std::shared_ptr<PermutationPhenotype> generateRandomNeighbour();


    friend class AbstractAlgorithm;
    friend class GA;
    friend class ACO;
    friend class TS;
    friend class SA;
    friend class RKRealPhenotype;
};


#endif //PROJEKT_PERMUTATIONPHENOTYPE_H
