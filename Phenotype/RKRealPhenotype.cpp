//
// Created by ivan on 11/23/21.
//

#include <iostream>
#include <iomanip>
#include <utility>
#include <algorithm>

#include "RKRealPhenotype.h"

using namespace std;

void RKRealPhenotype::calculateRepresentation() {
    unsigned long size = realValuesChromosome.size();
    this->chromosome = vector<int>(size);
    vector<int> indicesSorted(size);
    iota(indicesSorted.begin(), indicesSorted.end(), 0);
    sort(indicesSorted.begin(), indicesSorted.end(), [this](int ind1, int ind2){return this->realValuesChromosome[ind1] < this->realValuesChromosome[ind2];});
    for(int i = 0; i < size; i++)
        this->chromosome[indicesSorted[i]] = i;
}

RKRealPhenotype::RKRealPhenotype(VRP &vrp): BasePhenotype(vrp) {
    int size = vrp.getPhenotypeSize();
    this->realValuesChromosome.reserve(size);
    for(int i = 0; i < size; i++){
        this->realValuesChromosome.push_back(VRP::getReal01());
    }
    calculateRepresentation();
}

RKRealPhenotype::RKRealPhenotype(VRP& vrp, BasePhenotype::ConstructionMode constructionMode): BasePhenotype(vrp){
    if(constructionMode != RANDOM){
        shared_ptr<PermutationPhenotype> p = make_shared<PermutationPhenotype>(vrp, constructionMode);
        auto size = p->chromosome.size();
        this->realValuesChromosome.reserve(size);
        for(int i = 0; i < size; i++){
            this->realValuesChromosome.push_back((double) p->chromosome[i] / (double) size);
        }
    } else {
        int size = vrp.getPhenotypeSize();
        this->realValuesChromosome.reserve(size);
        for(int i = 0; i < size; i++){
            this->realValuesChromosome.push_back(VRP::getReal01());
        }
    }
    calculateRepresentation();
}

RKRealPhenotype::RKRealPhenotype(std::vector<double> chr, VRP & vrp, bool normalize): BasePhenotype(vrp){
    if(normalize){
        double min = chr[0];
        double max = chr[0];
        auto size = chr.size();
        for(int i = 0; i < size; i++){
            if(chr[i] < min)
                min = chr[i];
            if(chr[i] > max)
                max = chr[i];
        }
        double diff = max - min;
        for(int i = 0; i < size; i++){
            chr[i] -= min;
            chr[i] /= diff;
        }
    }
    this->realValuesChromosome = std::move(chr);
    calculateRepresentation();
}

ostream& operator<<(ostream &os, const RKRealPhenotype &phenotype) {
    os << "(";
    unsigned long size = phenotype.realValuesChromosome.size();
    unsigned long size2 = size;
    int w = 0;
    do {
        w++;
    } while(size2 /= 10);
    for(int i = 0; i < size; i++){
        os << setfill(' ') << setprecision(2) << setw(max(w, 4)) << phenotype.realValuesChromosome[i] << ((i < size - 1) ? ", " : ")");
    }
    os << endl << "(";
    for(int i = 0; i < size; i++){
        os << setfill(' ') << setw(max(w, 4)) << phenotype.chromosome[i] << ((i < size - 1) ? ", " : ")");
    }
    return os;
}

std::shared_ptr<RKRealPhenotype> RKRealPhenotype::generateRandomNeighbour() {
    auto perm = make_shared<PermutationPhenotype>(this->chromosome, vrp);
    auto n = perm->generateRandomNeighbour();

    vector<double> realCopy = this->realValuesChromosome;
    sort(realCopy.begin(), realCopy.end());

    vector<double> newRK;
    newRK.reserve(realCopy.size());

    for(int i: n->chromosome){
        newRK.push_back(realCopy[i]);
    }

    std::shared_ptr<RKRealPhenotype> newPhenotype = make_shared<RKRealPhenotype>(newRK, vrp, false);
    return newPhenotype;
}

bool RKRealPhenotype::compareByPenaltyValue(const shared_ptr<RKRealPhenotype> &p1,
                                            const shared_ptr<RKRealPhenotype> &p2) {
    if(p1->isViableSolution() != p2->isViableSolution())
        return p1->isViableSolution() > p2->isViableSolution();
    return p1->penaltyVal < p2->penaltyVal;
}

//void RKRealPhenotype::evaluate() {
//
//}



