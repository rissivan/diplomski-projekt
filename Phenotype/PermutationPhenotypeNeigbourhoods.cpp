//
// Created by ivan on 1/3/22.
//

#include "PermutationPhenotype.h"
#include "../Algorithm/AbstractAlgorithm.h"
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/future.hpp>
#include <random>
#include <set>


using namespace std;

std::vector<int> PermutationPhenotype::getRouteLengths() {
    vector<int> routeLengths;
    routeLengths.reserve(vrp.getNoVehicles());

    vector<int> vehicleIndices = getVehicleIndices();

    for(int i = 0; i < vrp.getNoVehicles() - 1; i++){
        routeLengths.push_back(vehicleIndices[i + 1] - vehicleIndices[i] - 1);
    }
    routeLengths.push_back(vehicleIndices[0] + (int) chromosome.size() - vehicleIndices[vrp.getNoVehicles() - 1] - 1);

    return routeLengths;
}

std::vector<int> PermutationPhenotype::getVehicleIndices() {
    vector<int> vehicleIndices;
    vehicleIndices.reserve(vrp.getNoVehicles());

    int chromosomeSize = (int) chromosome.size();
    int minimalVehicleIndex = vrp.getDimension() - 1;


    for(int i = 0; i < chromosomeSize; i++){
        if(chromosome[i] >= minimalVehicleIndex){
            vehicleIndices.push_back(i);
        }
    }

    return vehicleIndices;
}

std::shared_ptr<PermutationPhenotype> PermutationPhenotype::intraSwap(int vehicle, int offset1, int offset2) {
    vector<int> newChromosome = chromosome;

    vector<int> vehicleIndices = getVehicleIndices();
    vector<int> routeLengths = getRouteLengths();

    vehicle %= vrp.getNoVehicles();
    offset1 %= routeLengths[vehicle];
    offset2 %= routeLengths[vehicle];

    int vehicleIndex = vehicleIndices[vehicle];
    int index1 = (vehicleIndex + offset1 + 1) % (int) newChromosome.size(); //because offset starts at 0 instead of 1
    int index2 = (vehicleIndex + offset2 + 1) % (int) newChromosome.size();

    swap(newChromosome[index1], newChromosome[index2]);

    return make_shared<PermutationPhenotype>(newChromosome, vrp);
}

std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::interSwap(int vehicle1, int vehicle2, int offset1, int offset2) {
    vector<int> newChromosome = chromosome;

    vector<int> vehicleIndices = getVehicleIndices();
    vector<int> routeLengths = getRouteLengths();

    vehicle1 %= vrp.getNoVehicles();
    vehicle2 %= vrp.getNoVehicles();
    offset1 %= routeLengths[vehicle1];
    offset2 %= routeLengths[vehicle2];

    int vehicleIndex1 = vehicleIndices[vehicle1];
    int vehicleIndex2 = vehicleIndices[vehicle2];
    int index1 = (vehicleIndex1 + offset1 + 1) % (int) newChromosome.size(); //because offset starts at 0 instead of 1
    int index2 = (vehicleIndex2 + offset2 + 1) % (int) newChromosome.size();

    swap(newChromosome[index1], newChromosome[index2]);

    return make_shared<PermutationPhenotype>(newChromosome, vrp);
}

std::shared_ptr<PermutationPhenotype> PermutationPhenotype::intraShift(int vehicle, int customerOffset, int shiftOffset) {
    vector<int> newChromosome = chromosome;

    vector<int> vehicleIndices = getVehicleIndices();
    vector<int> routeLengths = getRouteLengths();

    vehicle %= vrp.getNoVehicles();
    customerOffset %= routeLengths[vehicle];
    shiftOffset %= routeLengths[vehicle];

    int vehicleIndex = vehicleIndices[vehicle];
    int customerIndex = (vehicleIndex + customerOffset + 1) % (int) newChromosome.size();

    if(customerOffset + shiftOffset + 1 > routeLengths[vehicle]){
        shiftOffset -= routeLengths[vehicle];
    }

    int customerValue = newChromosome[customerIndex];

    if(shiftOffset < 0){
        newChromosome.erase(newChromosome.begin() + customerIndex);
        if(customerIndex + shiftOffset < 0)
            shiftOffset += (int) newChromosome.size();
        newChromosome.insert(newChromosome.begin() + customerIndex + shiftOffset, customerValue);
    } else {
        newChromosome.erase(newChromosome.begin() + customerIndex);
        if(customerIndex + shiftOffset > newChromosome.size())
            shiftOffset -= (int) newChromosome.size();
        newChromosome.insert(newChromosome.begin() + customerIndex + shiftOffset, customerValue);
    }

    return make_shared<PermutationPhenotype>(newChromosome, vrp);
}

std::shared_ptr<PermutationPhenotype> PermutationPhenotype::interShift(int customerOffset, int shiftOffset) {
    vector<int> newChromosome = chromosome;

    vector<int> vehicleIndices = getVehicleIndices();
    vector<int> routeLengths = getRouteLengths();

    customerOffset %= (vrp.getDimension() - 1);
    shiftOffset %= (vrp.getPhenotypeSize() - 1);

    int customerIndex = customerOffset;

    for(int index: vehicleIndices){
        if(customerIndex >= index)
            customerIndex++;
        else
            break;
    }


    if(customerOffset + shiftOffset + 1 > vrp.getPhenotypeSize() - 1){
        shiftOffset -= (vrp.getPhenotypeSize() - 1);
    }

    int customerValue = newChromosome[customerIndex];

    if(shiftOffset < 0){
        newChromosome.erase(newChromosome.begin() + customerIndex);
        if(customerIndex + shiftOffset < 0)
            shiftOffset += (int) newChromosome.size();
        newChromosome.insert(newChromosome.begin() + customerIndex + shiftOffset, customerValue);
    } else {
        newChromosome.erase(newChromosome.begin() + customerIndex);
        if(customerIndex + shiftOffset > newChromosome.size())
            shiftOffset -= (int) newChromosome.size();
        newChromosome.insert(newChromosome.begin() + customerIndex + shiftOffset, customerValue);
    }

    return make_shared<PermutationPhenotype>(newChromosome, vrp);
}

std::shared_ptr<PermutationPhenotype> PermutationPhenotype::intra2Opt(int vehicle, int offset1, int offset2) {
    vector<int> newChromosome = chromosome;

    vector<int> vehicleIndices = getVehicleIndices();
    vector<int> routeLengths = getRouteLengths();

    vehicle %= vrp.getNoVehicles();
    offset1 %= routeLengths[vehicle];
    offset2 %= routeLengths[vehicle];

    int vehicleIndex = vehicleIndices[vehicle];
    int index1 = (vehicleIndex + offset1 + 1) % (int) newChromosome.size(); //because offset starts at 0 instead of 1
    int index2 = (vehicleIndex + offset2 + 1) % (int) newChromosome.size();
    if(index1 > index2) {
        int tmp = index1;
        index1 = index2;
        index2 = tmp;
        index1++;
        index2--;
    }

    reverse(newChromosome.begin() + index1, newChromosome.begin() + index2 + 1);

    return make_shared<PermutationPhenotype>(newChromosome, vrp);
}

std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::inter2Opt(int vehicle1, int vehicle2, int offset1, int offset2) {
    vector<int> newChromosome = chromosome;

    vector<int> vehicleIndices = getVehicleIndices();
    vector<int> routeLengths = getRouteLengths();

    vehicle1 %= vrp.getNoVehicles();
    vehicle2 %= vrp.getNoVehicles();
    offset1 %= routeLengths[vehicle1];
    offset2 %= routeLengths[vehicle2];

    int vehicleIndex1 = vehicleIndices[vehicle1];
    int vehicleIndex2 = vehicleIndices[vehicle2];
    int index1 = (vehicleIndex1 + offset1 + 1) % (int) newChromosome.size(); //because offset starts at 0 instead of 1
    int index2 = (vehicleIndex2 + offset2 + 1) % (int) newChromosome.size();
    if(index1 > index2) {
        int tmp = index1;
        index1 = index2;
        index2 = tmp;
        index1++;
        index2--;
    }

    reverse(newChromosome.begin() + index1, newChromosome.begin() + index2 + 1);

    return make_shared<PermutationPhenotype>(newChromosome, vrp);
}

shared_ptr<PermutationPhenotype> PermutationPhenotype::generateRandomNeighbour() {
    vector<int> vehicleIndices = getVehicleIndices();
    vector<int> routeLengths = getRouteLengths();

    int vehicles = vrp.getNoVehicles();

    double r = VRP::getReal01();
    if(r < (double) 1 / 6){
        int vehicle;

        do{
            vehicle = AbstractAlgorithm::getRandomInt(0, vehicles);
        } while(routeLengths[vehicle] < 2);
        int offset1 = AbstractAlgorithm::getRandomInt(0, routeLengths[vehicle]);
        int offset2;
        do{
            offset2 = AbstractAlgorithm::getRandomInt(0, routeLengths[vehicle]);
        } while (offset1 != offset2);

        return intraSwap(vehicle, offset1, offset2);
    } else if (r < (double) 2 / 6){
        int vehicle1, vehicle2;

        int noLengthsOver2 = 0;
        for(auto rl: routeLengths){
            if(rl >= 2){
                noLengthsOver2++;
            }
        }

        if(noLengthsOver2 < 2){
            //return this as no neighbour can be generated
            return make_shared<PermutationPhenotype>(chromosome, vrp);
        }

        do{
            vehicle1 = AbstractAlgorithm::getRandomInt(0, vehicles);
        } while(routeLengths[vehicle1] < 2);
        do{
            vehicle2 = AbstractAlgorithm::getRandomInt(0, vehicles);
        } while(routeLengths[vehicle2] < 2 || vehicle1 == vehicle2);

        int offset1 = AbstractAlgorithm::getRandomInt(0, routeLengths[vehicle1]);
        int offset2 = AbstractAlgorithm::getRandomInt(0, routeLengths[vehicle2]);

        return interSwap(vehicle1, vehicle2, offset1, offset2);
    } else if (r < (double) 3 / 6){
        int vehicle;

        do{
            vehicle = AbstractAlgorithm::getRandomInt(0, vehicles);
        } while(routeLengths[vehicle] < 2);
        int customerOffset = AbstractAlgorithm::getRandomInt(0, routeLengths[vehicle]);
        int shiftOffset = AbstractAlgorithm::getRandomInt(1, routeLengths[vehicle]);

        return intraShift(vehicle, customerOffset, shiftOffset);
    } else if (r < (double) 4 / 6){
        int customerOffset = AbstractAlgorithm::getRandomInt(0, vrp.getDimension() - 1);
        int shiftOffset = AbstractAlgorithm::getRandomInt(1, vrp.getPhenotypeSize());

        return interShift(customerOffset, shiftOffset);
    } else if (r < (double) 5 / 6){
        int vehicle;

        do{
            vehicle = AbstractAlgorithm::getRandomInt(0, vehicles);
        } while(routeLengths[vehicle] < 2);
        int offset1 = AbstractAlgorithm::getRandomInt(0, routeLengths[vehicle]);
        int offset2;
        do{
            offset2 = AbstractAlgorithm::getRandomInt(0, routeLengths[vehicle]);
        } while (offset1 != offset2);

        return intra2Opt(vehicle, offset1, offset2);
    } else {
        int vehicle1, vehicle2;

        int noLengthsOver2 = 0;
        for(auto rl: routeLengths){
            if(rl >= 2){
                noLengthsOver2++;
            }
        }

        if(noLengthsOver2 < 2){
            //return this as no neighbour can be generated
            return make_shared<PermutationPhenotype>(chromosome, vrp);
        }

        do{
            vehicle1 = AbstractAlgorithm::getRandomInt(0, vehicles);
        } while(routeLengths[vehicle1] < 2);
        do{
            vehicle2 = AbstractAlgorithm::getRandomInt(0, vehicles);
        } while(routeLengths[vehicle2] < 2 || vehicle1 == vehicle2);

        int offset1 = AbstractAlgorithm::getRandomInt(0, routeLengths[vehicle1]);
        int offset2 = AbstractAlgorithm::getRandomInt(0, routeLengths[vehicle2]);

        return inter2Opt(vehicle1, vehicle2, offset1, offset2);
    }
}


std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::intraSwapAndEvaluate(int vehicle, int offset1,
                                           int offset2) {
    auto n = intraSwap(vehicle, offset1, offset2);
    n->evaluate();
    return n;
}

std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::interSwapAndEvaluate(int vehicle1, int vehicle2,
                                           int offset1, int offset2) {
    auto n = interSwap(vehicle1, vehicle2, offset1, offset2);
    n->evaluate();
    return n;
}

std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::intraShiftAndEvaluate(int vehicle, int customerOffset,
                                            int shiftOffset) {
    auto n = intraShift(vehicle, customerOffset, shiftOffset);
    n->evaluate();
    return n;
}

std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::interShiftAndEvaluate(int customerOffset,
                                            int shiftOffset) {
    auto n = interShift(customerOffset, shiftOffset);
    n->evaluate();
    return n;
}

std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::intra2OptAndEvaluate(int vehicle, int offset1,
                                           int offset2) {
    auto n = intra2Opt(vehicle, offset1, offset2);
    n->evaluate();
    return n;
}

std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::inter2OptAndEvaluate(int vehicle1, int vehicle2,
                                           int offset1, int offset2) {
    auto n = inter2Opt(vehicle1, vehicle2, offset1, offset2);
    n->evaluate();
    return n;
}



