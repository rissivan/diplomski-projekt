//
// Created by ivan on 11/10/21.
//

#include "PermutationPhenotype.h"
#include <iostream>
#include <random>
#include <iomanip>
#include <set>


using namespace std;

PermutationPhenotype::PermutationPhenotype(VRP &vrp) : BasePhenotype(vrp){
    //ignore the depot city
    this->chromosome = vector<int>(vrp.getPhenotypeSize());
    iota(this->chromosome.begin(), this->chromosome.end(), 0);
    VRP::shuffleVector(this->chromosome);
}

PermutationPhenotype::PermutationPhenotype(VRP &vrp, PermutationPhenotype::ConstructionMode mode): BasePhenotype(vrp) {
    if(mode == RANDOM){
        this->chromosome = vector<int>(vrp.getPhenotypeSize());
        iota(this->chromosome.begin(), this->chromosome.end(), 0);
        VRP::shuffleVector(this->chromosome);
    } else if (mode == RANDOM_FEASIBLE){
        do{
            this->chromosome.clear();
            int dim = vrp.getDimension();
            int t = 0;
            while(t < dim){
                chromosome.push_back(t++);
            }
            VRP::shuffleVector(this->chromosome);
            int vehicleIndex = vrp.getDimension();
            int maxCapacity = vrp.getCapacity();
            int currentCapacity = 0;
            int maxVehicles = vrp.getNoVehicles();
            int currentVehicles = 0;
            auto it = chromosome.begin();
            for(; it != chromosome.end(); it++){
                if(currentCapacity + vrp.getDemands()[*it + 1] > maxCapacity){
                    currentCapacity = 0;
                    chromosome.insert(it, vehicleIndex);
                    it = chromosome.begin();
                    while(*it != vehicleIndex)
                        it++;
                    vehicleIndex++;
                    currentVehicles++;
                    if(currentVehicles > maxVehicles){
                        continue;
                    }
                } else {
                    currentCapacity += vrp.getDemands()[*it + 1];
                }
            }
            evaluate();
        } while(!this->viableSolution);
    } else if (mode == GUIDED_CLASSIC) {
        do{
            this->chromosome.clear();
            bool inDepot = true;
            set<int> visitedCustomers;
            int currentVehicleIndex = this->vrp.getDimension();
            int currentCapacity = 0;

            auto distances = this->vrp.getDistances();
            auto demands = this->vrp.getDemands();
            auto maxCapacity = this->vrp.getCapacity();

            while(this->chromosome.size() != this->vrp.getPhenotypeSize()){
                int currentIndex;
                if(inDepot){
                    currentIndex = 0;
                    inDepot = false;
                } else {
                    currentIndex = *this->chromosome.rbegin();
                }

                vector<int> feasibleCustomers;

                for(int i = 1; i < this->vrp.getDimension(); i++){
                    if(!visitedCustomers.contains(i) && currentCapacity + demands[i] <= maxCapacity){
                        feasibleCustomers.push_back(i);
                    }
                }

                if(feasibleCustomers.empty()){
                    if(currentVehicleIndex <= this->vrp.getPhenotypeSize()) {
                        this->chromosome.push_back(currentVehicleIndex++);
                        inDepot = true;
                        currentCapacity = 0;
                    } else {
                        break;
                    }
                } else {
                    sort(feasibleCustomers.begin(), feasibleCustomers.end(),
                         [currentIndex, distances](int ind1, int ind2){
                             return distances[currentIndex][ind1] < distances[currentIndex][ind2];
                         });

                    int newCustomer = feasibleCustomers[VRP::getInt(0, min(rclSize - 1, (int)feasibleCustomers.size() - 1))];

                    this->chromosome.push_back(newCustomer - 1);
                    visitedCustomers.insert(newCustomer);
                    currentCapacity += demands[newCustomer];
                }
            }

            evaluate();

        } while(!this->viableSolution);
    } else if (mode == GUIDED_DENSITY){

    }
}

PermutationPhenotype::PermutationPhenotype(std::vector<int> chr, VRP& vrp) : BasePhenotype(vrp) {
    this->chromosome = std::move(chr);
}

std::ostream &operator<<(std::ostream& os, const PermutationPhenotype &phenotype) {
    os << "(";
    unsigned long size = phenotype.chromosome.size();
    unsigned long size2 = size;
    int w = 0;
    do {
        w++;
    } while(size2 /= 10);
    for(int i = 0; i < size; i++){
        os << setfill(' ') << setw(w) << phenotype.chromosome[i] << ((i < size - 1) ? ", " : ")");
    }
    return os;
}

void PermutationPhenotype::swapMutation() {
    int max = vrp.getPhenotypeSize();
    int index1;
    do {
       index1 = vrp.getInt();
    } while(index1 == max);
    int index2;
    do {
        index2 = vrp.getInt();
    } while(index1 == index2 || index2 == max);
    iter_swap(chromosome.begin() + index1, chromosome.begin() + index2);
}

void PermutationPhenotype::insertMutation() {
    int max = vrp.getPhenotypeSize();
    int index1;
    do {
        index1 = vrp.getInt();
    } while(index1 == max);
    int index2;
    do {
        index2 = vrp.getInt();
    } while(index1 == index2 || index2 == max);
    if(index1 < index2){
        int element = *(chromosome.begin() + index2);
        chromosome.erase(chromosome.begin() + index2);
        chromosome.insert(chromosome.begin() + index1 + 1, element);
    } else {
        int element = *(chromosome.begin() + index2);
        chromosome.insert(chromosome.begin() + index1, element);
        chromosome.erase(chromosome.begin() + index2);
    }
}

void PermutationPhenotype::scrambleMutation() {
    int index1 = vrp.getInt();
    int index2;
    do {
        index2 = vrp.getInt();
    } while(index1 == index2);

    if(index1 > index2){
        swap(index1, index2);
    }

    VRP::shuffleSubvector(chromosome.begin() + index1, chromosome.begin() + index2);
}

std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::partiallyMappedCrossover(const std::shared_ptr<PermutationPhenotype>& other) {
    unsigned long size = this->chromosome.size();
    vector<int> newChromosome(size);
    vector<bool> newChromosomeDone(size);
    int index1 = vrp.getInt();
    int index2;
    do {
        index2 = vrp.getInt();
    } while(index1 == index2);
    if(index1 > index2)
        swap(index1, index2);

    copy(this->chromosome.begin() + index1, this->chromosome.begin() + index2, newChromosome.begin() + index1);
    for(int i = index1; i < index2; i++){
        newChromosomeDone[i] = true;
    }

    int sectionSize = index2 - index1;

    vector<int> parent1SubVector(sectionSize), parent2SubVector(sectionSize);
    vector<bool> parent1BoolVector(sectionSize), parent2BoolVector(sectionSize);

    copy(this->chromosome.begin() + index1, this->chromosome.begin() + index2, parent1SubVector.begin());
    copy(other->chromosome.begin() + index1, other->chromosome.begin() + index2, parent2SubVector.begin());

    for(auto it2 = parent2SubVector.begin(); it2 != parent2SubVector.end(); it2++){
        auto it1 = find(parent1SubVector.begin(), parent1SubVector.end(), *it2);
        if(it1 != parent1SubVector.end()){
            parent1BoolVector[it1 - parent1SubVector.begin()] = true;
            parent2BoolVector[it2 - parent2SubVector.begin()] = true;
        }
    }

    int parent1Index = 0;
    int parent2Index = 0;

    while(true){
        bool gotToTheEnd = false;
        if(parent1Index >= sectionSize || parent2Index >= sectionSize)
            break;
        while(parent1BoolVector[parent1Index]){
            parent1Index++;
            if(parent1Index >= sectionSize){
                gotToTheEnd = true;
                break;
            }
        }
        while(parent2BoolVector[parent2Index]){
            parent2Index++;
            if(parent2Index >= sectionSize){
                gotToTheEnd = true;
                break;
            }
        }
        if(gotToTheEnd)
            break;
        auto it2 = find(other->chromosome.begin(), other->chromosome.end(), parent1SubVector[parent1Index]);
        newChromosome[it2 - other->chromosome.begin()] = parent2SubVector[parent2Index];
        newChromosomeDone[it2 - other->chromosome.begin()] = true;
        parent1Index++;
        parent2Index++;
    }

    for(int i = 0; i < size; i++){
        if(!newChromosomeDone[i])
            newChromosome[i] = other->chromosome[i];
    }

    return make_shared<PermutationPhenotype>(newChromosome, vrp);
}

std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::orderCrossover1(const shared_ptr<PermutationPhenotype> &other) {
    unsigned long size = this->chromosome.size();
    vector<int> newChromosome(size);
    vector<bool> newChromosomeDone(size);
    int index1 = vrp.getInt();
    int index2;
    do {
        index2 = vrp.getInt();
    } while(index1 == index2);
    if(index1 > index2)
        swap(index1, index2);

//    cout << index1 << " " << index2 << endl;

    copy(this->chromosome.begin() + index1, this->chromosome.begin() + index2, newChromosome.begin() + index1);
    for(int i = index1; i < index2; i++){
        newChromosomeDone[i] = true;
    }

    int sectionSize = index2 - index1;

    vector<int> parent1SubVector(sectionSize), parent2SubVector(sectionSize);
    vector<bool> parent1BoolVector(sectionSize), parent2BoolVector(sectionSize);

    copy(this->chromosome.begin() + index1, this->chromosome.begin() + index2, parent1SubVector.begin());
    copy(other->chromosome.begin() + index1, other->chromosome.begin() + index2, parent2SubVector.begin());

    auto it = other->chromosome.begin() + index2;
    if(it == other->chromosome.end())
        it = other->chromosome.begin();

    for(int i = 0; i < size; i++){
        if(newChromosomeDone[i])
            continue;
        while(find(parent1SubVector.begin(), parent1SubVector.end(), *it) != parent1SubVector.end()){
            it++;
            if(it == other->chromosome.end())
                it = other->chromosome.begin();
        }
        newChromosome[i] = *it++;
        if(it == other->chromosome.end())
            it = other->chromosome.begin();
    }

    return make_shared<PermutationPhenotype>(newChromosome, vrp);
}

std::shared_ptr<PermutationPhenotype>
PermutationPhenotype::orderCrossover2(const shared_ptr<PermutationPhenotype> &other) {
    unsigned long size = this->chromosome.size();
    vector<int> newChromosome(size);
    vector<bool> newChromosomeDone(size);
    int index1 = vrp.getInt();
    int index2;
    do {
        index2 = vrp.getInt();
    } while(index1 == index2);
    if(index1 > index2)
        swap(index1, index2);

    copy(this->chromosome.begin() + index1, this->chromosome.begin() + index2, newChromosome.begin() + index1);
    for(int i = index1; i < index2; i++){
        newChromosomeDone[i] = true;
    }

    int sectionSize = index2 - index1;

    vector<int> parent1SubVector(sectionSize), parent2SubVector(sectionSize);
    vector<bool> parent1BoolVector(sectionSize), parent2BoolVector(sectionSize);

    copy(this->chromosome.begin() + index1, this->chromosome.begin() + index2, parent1SubVector.begin());
    copy(other->chromosome.begin() + index1, other->chromosome.begin() + index2, parent2SubVector.begin());

    auto it = other->chromosome.begin() + index2;
    if(it == other->chromosome.end())
        it = other->chromosome.begin();

    for(int i = index2; i < size; i++){
        if(newChromosomeDone[i])
            continue;
        while(find(parent1SubVector.begin(), parent1SubVector.end(), *it) != parent1SubVector.end()){
            it++;
            if(it == other->chromosome.end())
                it = other->chromosome.begin();
        }
        newChromosome[i] = *it++;
        if(it == other->chromosome.end())
            it = other->chromosome.begin();
    }

    for(int i = 0; i < index1; i++){
        if(newChromosomeDone[i])
            continue;
        while(find(parent1SubVector.begin(), parent1SubVector.end(), *it) != parent1SubVector.end()){
            it++;
            if(it == other->chromosome.end())
                it = other->chromosome.begin();
        }
        newChromosome[i] = *it++;
    }

    return make_shared<PermutationPhenotype>(newChromosome, vrp);
}

void PermutationPhenotype::mutate() {
    double random01 = VRP::getReal01();
    if(random01 < (double) 1 / 3){
        swapMutation();
    } else if(random01 < (double) 2 / 3){
        insertMutation();
    } else {
        scrambleMutation();
    }
}

std::shared_ptr<PermutationPhenotype> PermutationPhenotype::crossover(const std::shared_ptr<PermutationPhenotype>& other) {
    double random01 = VRP::getReal01();
    if(random01 < (double) 1 / 3){
        return partiallyMappedCrossover(other);
    } else if (random01 < (double) 2 / 3){
        return orderCrossover1(other);
    } else {
        return orderCrossover2(other);
    }
}

bool PermutationPhenotype::compareByPenaltyValue(const std::shared_ptr<PermutationPhenotype>& p1, const std::shared_ptr<PermutationPhenotype>& p2) {
    if(p1->isViableSolution() != p2->isViableSolution())
        return p1->isViableSolution() > p2->isViableSolution();
    return p1->penaltyVal < p2->penaltyVal; //provjeriti inkonzistenciju (ako je onaj koji je viable losije rjesenje po penaltyju)
}

bool operator==(const std::shared_ptr<PermutationPhenotype>& p1, const std::shared_ptr<PermutationPhenotype>& p2) {
    if(p1->chromosome.size() != p2->chromosome.size())
        return false;
    if(p1->isViableSolution() != p2->isViableSolution())
        return false;
    if(abs(p1->getPenaltyVal() - p2->getPenaltyVal()) > 1e-6)
        return false;
    int size = (int) p1->chromosome.size();
    for(int i = 0; i < size; i++){
        if(p1->chromosome[i] != p2->chromosome[i])
            return false;
    }
    return true;
}
