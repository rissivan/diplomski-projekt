//
// Created by ivan on 12/29/21.
//

#include "BasePhenotype.h"
#include <iostream>

using namespace std;

double BasePhenotype::getPenaltyVal() const {
    return penaltyVal;
}

bool BasePhenotype::isViableSolution() const {
    return viableSolution;
}

BasePhenotype::BasePhenotype(VRP &vrp): vrp(vrp) {}

void BasePhenotype::evaluate() {
    vector<vector<int>> vehicleRoutes;
    vector<int> vehicleRoute;

    if(this->chromosome.size() != this->vrp.getPhenotypeSize()){
        viableSolution = false;
        penaltyVal = numeric_limits<double>::max();
        return;
    }

    this->viableSolution = true;

    auto it = this->chromosome.begin();
    while(*it < this->vrp.getDimension() - 1)
        it++;
    for(; it < this->chromosome.end(); it++){
        if(*it < this->vrp.getDimension() - 1){
            vehicleRoute.push_back(*it);
        } else {
            vehicleRoutes.push_back(vehicleRoute);
            vehicleRoute = vector<int>();
        }
    }
    for(it = this->chromosome.begin(); *it < this->vrp.getDimension() - 1; it++){
        vehicleRoute.push_back(*it);
    }
    vehicleRoutes.push_back(vehicleRoute);

    this->penaltyVal = 0;

    auto& distances = this->vrp.getDistances();
    auto& demands = this->vrp.getDemands();

    auto vehicleRoutesCount = vehicleRoutes.size();
    int t = 0;

    for(const auto& v: vehicleRoutes){

        double currentDistance = 0;
        int currentCapacity = 0;
        unsigned long size = v.size();
        if(size == 0)
            continue;
        for(int i = 0; i < size; i++){
            int index1;
            if(i == 0)
                index1 = 0;
            else
                index1 = v[i - 1] + 1;

            int index2 = v[i] + 1;

            currentDistance += distances[index1][index2];
            currentCapacity += demands[index2];
        }
        currentDistance += distances[v[size - 1] + 1][0];

//        if(currentCapacity > this->vrp.getCapacity()){
//            this->viableSolution = false;
//            this->penaltyVal = numeric_limits<double>::max();
//        } else {
//            this->penaltyVal += currentDistance;
//        }

        if(currentCapacity > this->vrp.getCapacity()){
            this->viableSolution = false;
            this->penaltyVal += currentDistance;
        } else {
            this->penaltyVal += currentDistance;
        }

        if(!this->viableSolution){
            this->penaltyVal += PENALTY_FACTOR * (currentDistance - this->vrp.getCapacity());
        }
        t++;

    }
}
