//
// Created by ivan on 5/10/22.
//

#include <iostream>
#include <thread>

#include <boost/asio.hpp>
#include <boost/asio/thread_pool.hpp>
#include <memory>
#include <random>
#include <chrono>
#include <fstream>
#include <utility>

#include "VehicleRoutingProblem/VRP.h"
#include "Phenotype/PermutationPhenotype.h"
#include "Phenotype/RKRealPhenotype.h"
#include "Algorithm/GA.h"
#include "Algorithm/DE.h"
#include "Algorithm/BSA.h"
#include "Algorithm/PSO.h"
#include "Algorithm/ACO.h"
#include "Algorithm/ABC.h"
#include "Algorithm/TS.h"
#include "Algorithm/SA.h"
#include "Util/Util.h"

using namespace std;

void runGA(){
//    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});
    vector<string> vrpPaths({"../data/A/A-n80-k10.vrp"});

//    vector<int> popSizes({5, 10, 20, 30, 50, 100});
    vector<int> popSizes({100});
//    vector<double> mutationRates({0.0025, 0.005, 0.01, 0.02, 0.03});
    vector<double> mutationRates({0.01, 0.02});
//    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE});
    vector<bool> searchNeighbourhood({false});

    for(const auto& vrpPath: vrpPaths){
        VRP vrp(vrpPath);

        for(int popSize: popSizes){
            for(double mr: mutationRates){
                for(auto mode: constructionModes){
                    for(bool sn: searchNeighbourhood){
                        for(int i = 1; i <= 10; i++){
                            shared_ptr<GA> ga = make_shared<GA>(vrp);

                            string resultFile = "../results/GA/experiment_data/GA_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                                "_ps_" + to_string(popSize) + "_pm_" + to_string(mr) +
                                                //                                                "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                                //                                                "_search_" + (sn ? "true" : "false") +
                                                "_" + to_string(i) + ".txt";
                            ofstream os(resultFile);

                            auto start = chrono::high_resolution_clock::now();

                            ga->setPopulationSize(popSize);
                            ga->setPM(mr);
                            ga->setConstructionMode(mode);
                            ga->setSearchNeighbourhood(sn);

                            ga->run();
                            ga->printBest(os);

                            auto stop = chrono::high_resolution_clock::now();

                            os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                            cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                            os.flush();
                            os.close();

                        }
                    }
                }
            }
        }
    }
}

void runABC(){
    vector<string> vrpPaths({"../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});
//    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});

    vector<int> popSizes({10, 20, 30, 50, 80, 100});
//    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE});

    for(const auto& vrpPath: vrpPaths){
        VRP vrp(vrpPath);

        for(int popSize: popSizes){
            for(auto mode: constructionModes){
                for(int i = 1; i <= 10; i++){
                    ABC abc(vrp);

                    string resultFile = "../results/ABC/experiment_data/ABC_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                        "_ps_" + to_string(popSize) +
                                        //                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                        "_" + to_string(i) + ".txt";
                    ofstream os(resultFile);

                    auto start = chrono::high_resolution_clock::now();

                    abc.setPopulationSize(popSize);
                    abc.setConstructionMode(mode);

                    abc.run();
                    abc.printBest(os);

                    auto stop = chrono::high_resolution_clock::now();

                    os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                    cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                    os.flush();
                    os.close();

                }
            }
        }
    }
}

void runTS(){
    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});

    vector<int> tabuTenures({1, 2, 5, 10, 20, 50});
//    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE});

    for(const auto& vrpPath: vrpPaths){
        VRP vrp(vrpPath);

        for(int tabuTenure: tabuTenures){
            for(auto mode: constructionModes){
                for(int i = 1; i <= 10; i++){
                    TS ts(vrp);

                    string resultFile = "../results/TS/experiment_data/TS_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                        "_tt_" + to_string(tabuTenure) +
                                        //                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                        "_" + to_string(i) + ".txt";
                    ofstream os(resultFile);

                    auto start = chrono::high_resolution_clock::now();

                    ts.setTabuTenure(tabuTenure);
                    ts.setConstructionMode(mode);

                    ts.run();
                    ts.printBest(os);

                    auto stop = chrono::high_resolution_clock::now();

                    os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                    cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                    os.flush();
                    os.close();

                }
            }
        }
    }
}

void runDE(){
//    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});
    vector<string> vrpPaths({"../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});

    vector<double> fs({0.1, 0.25, 0.5, 1, 1.5, 2});
    vector<double> crs({0.1, 0.25, 0.5, 0.75, 0.9, 1});
//    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE});

    for(const auto& vrpPath: vrpPaths){
        VRP vrp(vrpPath);

        for(double f: fs){
            for(double cr: crs){
                for(auto mode: constructionModes){
                    for(int i = 1; i <= 10; i++){
                        DE de(vrp);

                        string resultFile = "../results/DE/experiment_data/DE_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                            "_f_" + to_string(f) + "_cr_" + to_string(cr) +
                                            //                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                            "_" + to_string(i) + ".txt";
                        ofstream os(resultFile);

                        auto start = chrono::high_resolution_clock::now();

                        de.setF(f);
                        de.setCr(cr);
                        de.setConstructionMode(mode);

                        de.run();
                        de.printBest(os);

                        auto stop = chrono::high_resolution_clock::now();

                        os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                        cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                        os.flush();
                        os.close();

                    }
                }
            }
        }
    }
}

void runBSA(){
    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});

    vector<double> fs({0.1, 0.25, 0.5, 1, 1.5, 2});
//    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE});

    for(const auto& vrpPath: vrpPaths){
        VRP vrp(vrpPath);

        for(double f: fs){
            for(auto mode: constructionModes){
                for(int i = 1; i <= 10; i++){
                    BSA bsa(vrp);

                    string resultFile = "../results/BSA/experiment_data/BSA_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                        "_f_" + to_string(f) +
                                        //                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                        "_" + to_string(i) + ".txt";
                    ofstream os(resultFile);

                    auto start = chrono::high_resolution_clock::now();

                    bsa.setF(f);
                    bsa.setConstructionMode(mode);

                    bsa.run();
                    bsa.printBest(os);

                    auto stop = chrono::high_resolution_clock::now();

                    os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                    cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                    os.flush();
                    os.close();

                }
            }
        }
    }
}

void runPSO(){
    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});

    vector<double> cls({1, 1.5, 2, 3});
    vector<double> cgs({1, 1.5, 2, 3});
//    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE});

    for(const auto& vrpPath: vrpPaths){
        VRP vrp(vrpPath);

        for(double cl: cls){
            for(double cg: cgs){
                for(auto mode: constructionModes){
                    for(int i = 1; i <= 10; i++){
                        PSO pso(vrp);

                        string resultFile = "../results/PSO/experiment_data/PSO_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                            "_cl_" + to_string(cl) + "_cg_" + to_string(cg) +
                                            //                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                            "_" + to_string(i) + ".txt";
                        ofstream os(resultFile);

                        auto start = chrono::high_resolution_clock::now();

                        pso.setCl(cl);
                        pso.setCg(cg);
                        pso.setConstructionMode(mode);

                        pso.run();
                        pso.printBest(os);

                        auto stop = chrono::high_resolution_clock::now();

                        os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                        cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                        os.flush();
                        os.close();

                    }
                }
            }
        }
    }
}

void runSA(){
    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});

    vector<double(*)(double)> decrementFunctions({&SA::linearDecrement, &SA::geometricDecrement, &SA::verySlowDecrement});
    vector<vector<double>> coefficients({{0.005, 0.01, 0.05, 0.1}, {0.99, 0.999, 0.9999, 0.99995}, {0.001, 0.005, 0.01, 0.05}});

//    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE});

    for(const auto& vrpPath: vrpPaths){
        VRP vrp(vrpPath);
        for(int dec = 0; dec < 3; dec++){
            for(double coef: coefficients[dec]){
                for(auto mode: constructionModes){
                    for(int i = 1; i <= 10; i++){
                        SA sa(vrp);

                        string decString;
                        if(dec == 0){
                            decString = "lin";
                        } else if (dec == 1){
                            decString = "geom";
                        } else if (dec == 2) {
                            decString = "vslow";
                        }

                        string resultFile = "../results/SA/experiment_data/SA_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                            "_decF_" + decString + "_coef_" + to_string(coef) +
                                            //                                            "_cl_" + to_string(cl) + "_cg_" + to_string(cg) +
                                            //                                            "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                            "_" + to_string(i) + ".txt";
                        ofstream os(resultFile);

                        auto start = chrono::high_resolution_clock::now();

                        sa.setDecrementFunction(decrementFunctions[dec]);
                        SA::setSaB(coef);
                        SA::setSaBeta(coef);
                        SA::setSaAlpha(coef);

                        sa.setConstructionMode(mode);

                        sa.run();
                        sa.printBest(os);

                        auto stop = chrono::high_resolution_clock::now();

                        os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                        cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                        os.flush();
                        os.close();

                    }
                }
            }
        }
    }
}

void runACO(){
//    vector<string> vrpPaths({"../data/A/A-n32-k5.vrp", "../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});
    vector<string> vrpPaths({"../data/A/A-n60-k9.vrp", "../data/A/A-n80-k10.vrp"});

    vector<double> alphas({0.5, 1, 2, 3.5, 5});
    vector<double> betas({0.5, 1, 2, 3.5, 5});
    vector<double> gammas({3});
//    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE});

    for(const auto& vrpPath: vrpPaths){
        VRP vrp(vrpPath);

        for(double alpha: alphas){
            for(double beta: betas){
                for(double gamma: gammas){
                    for(auto mode: constructionModes){
                        for(int i = 1; i <= 10; i++){
                            ACO aco(vrp);

                            string resultFile = "../results/ACO/experiment_data/ACO_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                                "_a_" + to_string(alpha) + "_b_" + to_string(beta) + "_g_" + to_string(gamma) +
                                                //                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                                "_" + to_string(i) + ".txt";
                            ofstream os(resultFile);

                            auto start = chrono::high_resolution_clock::now();

                            aco.setAlpha(alpha);
                            aco.setBeta(beta);
                            aco.setGamma(gamma);
                            aco.setConstructionMode(mode);

                            aco.run();
                            aco.printBest(os);

                            auto stop = chrono::high_resolution_clock::now();

                            os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                            cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                            os.flush();
                            os.close();

                        }
                    }
                }
            }
        }
    }
}

void runPSO2ndRun(){
    struct PSO_config{
        std::string vrpPath;
        double cl{};
        double cg{};
        PSO_config()= default;
        PSO_config(std::string v, double cl, double cg): vrpPath(std::move(v)), cl(cl), cg(cg){}
    };

    vector<PSO_config> configs({{"../data/A/A-n32-k5.vrp", 2, 2}, {"../data/A/A-n60-k9.vrp", 2, 3}, {"../data/A/A-n80-k10.vrp", 3, 1}});

    vector<double> ws({0.1, 0.2, 0.4, 0.5, 0.75, 1});

//    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE});
    auto mode = BasePhenotype::RANDOM_FEASIBLE;

    for(const auto& config: configs){
        string vrpPath = config.vrpPath;
        VRP vrp(vrpPath);

        double cl = config.cl;
        double cg = config.cg;

        for(double w: ws){


            for(int i = 1; i <= 10; i++){
                PSO pso(vrp);

                string resultFile = "../results/PSO/experiment_data/PSO_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                    "_cl_" + to_string(cl) + "_cg_" + to_string(cg) + "_w_" + to_string(w) +
                                    //                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                    "_" + to_string(i) + ".txt";
                ofstream os(resultFile);

                auto start = chrono::high_resolution_clock::now();

                pso.setCl(cl);
                pso.setCg(cg);
                pso.setConstructionMode(mode);
                pso.setW(w);

                pso.run();
                pso.printBest(os);

                auto stop = chrono::high_resolution_clock::now();

                os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                os.flush();
                os.close();

            }
        }
    }
}

void runACO2ndRun(){
    struct ACO_config{
        std::string vrpPath;
        double alpha{};
        double beta{};
        ACO_config()= default;
        ACO_config(std::string v, double alpha, double beta): vrpPath(std::move(v)), alpha(alpha), beta(beta){}
    };

    vector<ACO_config> configs({{"../data/A/A-n32-k5.vrp", 1, 1}, {"../data/A/A-n60-k9.vrp", 1, 0.5}, {"../data/A/A-n80-k10.vrp", 1, 1}});

    vector<double> gammas({0.5, 1, 2, 3, 5});

    for(const auto& config: configs){
        string vrpPath = config.vrpPath;
        VRP vrp(vrpPath);

        double alpha = config.alpha;
        double beta = config.beta;

        for(double gamma: gammas){
            for(int i = 1; i <= 10; i++){
                ACO aco(vrp);

                string resultFile = "../results/ACO/experiment_data/ACO_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                    "_a_" + to_string(alpha) + "_b_" + to_string(beta) + "_g_" + to_string(gamma) +
                                    //                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                    "_" + to_string(i) + ".txt";
                ofstream os(resultFile);

                auto start = chrono::high_resolution_clock::now();

                aco.setAlpha(alpha);
                aco.setBeta(beta);
                aco.setGamma(gamma);

                aco.run();
                aco.printBest(os);

                auto stop = chrono::high_resolution_clock::now();

                os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                os.flush();
                os.close();

            }
        }
    }
}


void runGA2(){

    struct GA_config{
        std::string vrpPath;
        int popSize{};
        double mutationRate{};
        GA_config()= default;
        GA_config(std::string v, int p, double m): vrpPath(std::move(v)), popSize(p), mutationRate(m){}
    };

//    vector<GA_config> configs({{"../data/A/A-n32-k5.vrp", 100, 0.005}, {"../data/A/A-n60-k9.vrp", 100, 0.03}, {"../data/A/A-n80-k10.vrp", 100, 0.03}});
    vector<GA_config> configs({{"../data/A/A-n60-k9.vrp", 100, 0.03}, {"../data/A/A-n80-k10.vrp", 100, 0.03}});

    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
//    vector<bool> searchNeighbourhood({false, true});
    vector<bool> searchNeighbourhood({true});

    for(const GA_config& config: configs){
        string vrpPath = config.vrpPath;
        int popSize = config.popSize;
        double mutationRate = config.mutationRate;

        VRP vrp(vrpPath);

        for(auto mode: constructionModes){
            for(bool sn: searchNeighbourhood){
                for(int i = 1; i <= 10; i++){
                    shared_ptr<GA> ga = make_shared<GA>(vrp);

                    string fileName = "GA_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                     "_ps_" + to_string(popSize) + "_pm_" + to_string(mutationRate) +
                                     "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                     "_search_" + (sn ? "true" : "false") +
                                     "_" + to_string(i) + ".txt";


                    string resultFile = "../results/GA/experiment_data/" + fileName;
                    string iterationFile = "../results/GA/iteration_graph_data/" + fileName;

                    ofstream os(resultFile);
                    ga->outputToFile(iterationFile);

                    auto start = chrono::high_resolution_clock::now();

                    ga->setPopulationSize(popSize);
                    ga->setPM(mutationRate);

                    ga->setConstructionMode(mode);
                    ga->setSearchNeighbourhood(sn);

                    ga->run();
                    ga->printBest(os);

                    auto stop = chrono::high_resolution_clock::now();

                    os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                    cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                    os.flush();
                    os.close();

                }
            }
        }
    }
}

void runDE2(){
    struct DE_config{
        std::string vrpPath;
        double f{};
        double cr{};
        DE_config()= default;
        DE_config(std::string v, double f, double cr): vrpPath(std::move(v)), f(f), cr(cr){}
    };

    vector<DE_config> configs({{"../data/A/A-n60-k9.vrp", 0.25, 0.9}, {"../data/A/A-n80-k10.vrp", 0.5, 0.1}});
//    vector<DE_config> configs({{"../data/A/A-n32-k5.vrp", 0.25, 0.25}, {"../data/A/A-n60-k9.vrp", 0.25, 0.9}, {"../data/A/A-n80-k10.vrp", 0.5, 0.1}});

    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<bool> searchNeighbourhood({false, true});

    for(const DE_config& config: configs){
        string vrpPath = config.vrpPath;
        double f = config.f;
        double cr = config.cr;

        VRP vrp(vrpPath);

        for(auto mode: constructionModes){
            for(bool sn: searchNeighbourhood){
                for(int i = 1; i <= 10; i++){
                    shared_ptr<DE> de = make_shared<DE>(vrp);

                    string fileName = "DE_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                        "_f_" + to_string(f) + "_cr_" + to_string(cr) +
                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                        "_search_" + (sn ? "true" : "false") +
                                        "_" + to_string(i) + ".txt";

                    string resultFile = "../results/DE/experiment_data/" + fileName;
                    string iterationFile = "../results/DE/iteration_graph_data/" + fileName;

                    ofstream os(resultFile);
                    de->outputToFile(iterationFile);

                    auto start = chrono::high_resolution_clock::now();

                    de->setF(f);
                    de->setCr(cr);

                    de->setConstructionMode(mode);
                    de->setSearchNeighbourhood(sn);

                    de->run();
                    de->printBest(os);

                    auto stop = chrono::high_resolution_clock::now();

                    os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                    cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                    os.flush();
                    os.close();

                }
            }
        }
    }
}

void runBSA2(){
    struct BSA_config{
        std::string vrpPath;
        double f{};
        BSA_config()= default;
        BSA_config(std::string v, double f): vrpPath(std::move(v)), f(f){}
    };

    vector<BSA_config> configs({{"../data/A/A-n60-k9.vrp", 1}, {"../data/A/A-n80-k10.vrp", 1}});
//    vector<BSA_config> configs({{"../data/A/A-n32-k5.vrp", 1.5}, {"../data/A/A-n60-k9.vrp", 1}, {"../data/A/A-n80-k10.vrp", 1}});

    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<bool> searchNeighbourhood({false, true});

    for(const BSA_config& config: configs){
        string vrpPath = config.vrpPath;
        double f = config.f;

        VRP vrp(vrpPath);

        for(auto mode: constructionModes){
            for(bool sn: searchNeighbourhood){
                for(int i = 1; i <= 10; i++){
                    shared_ptr<BSA> bsa = make_shared<BSA>(vrp);

                    string fileName = "BSA_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                        "_f_" + to_string(f) +
                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                        "_search_" + (sn ? "true" : "false") +
                                        "_" + to_string(i) + ".txt";

                    string resultFile = "../results/BSA/experiment_data/" + fileName;
                    string iterationFile = "../results/BSA/iteration_graph_data/" + fileName;

                    ofstream os(resultFile);
                    bsa->outputToFile(iterationFile);

                    auto start = chrono::high_resolution_clock::now();

                    bsa->setF(f);

                    bsa->setConstructionMode(mode);
                    bsa->setSearchNeighbourhood(sn);

                    bsa->run();
                    bsa->printBest(os);

                    auto stop = chrono::high_resolution_clock::now();

                    os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                    cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                    os.flush();
                    os.close();

                }
            }
        }
    }
}

void runPSO2(){
    struct PSO_config{
        std::string vrpPath;
        double cl{};
        double cg{};
        double w{};
        PSO_config()= default;
        PSO_config(std::string v, double cl, double cg, double w): vrpPath(std::move(v)), cl(cl), cg(cg), w(w){}
    };

    vector<PSO_config> configs({{"../data/A/A-n60-k9.vrp", 2, 3, 0.4}, {"../data/A/A-n80-k10.vrp", 3, 1, 0.2}});
//    vector<PSO_config> configs({{"../data/A/A-n32-k5.vrp", 2, 2, 0.2}, {"../data/A/A-n60-k9.vrp", 2, 3, 0.4}, {"../data/A/A-n80-k10.vrp", 3, 1, 0.2}});

    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    vector<bool> searchNeighbourhood({false, true});

    for(const PSO_config& config: configs){
        string vrpPath = config.vrpPath;
        double cl = config.cl;
        double cg = config.cg;
        double w = config.w;

        VRP vrp(vrpPath);

        for(auto mode: constructionModes){
            for(bool sn: searchNeighbourhood){
                for(int i = 1; i <= 10; i++){
                    shared_ptr<PSO> pso = make_shared<PSO>(vrp);

                    string fileName = "PSO_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                        "_cl_" + to_string(cl) + "_cg_" + to_string(cg) +
                                        "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                        "_search_" + (sn ? "true" : "false") +
                                        "_" + to_string(i) + ".txt";

                    string resultFile = "../results/PSO/experiment_data/" + fileName;
                    string iterationFile = "../results/PSO/iteration_graph_data/" + fileName;

                    ofstream os(resultFile);
                    pso->outputToFile(iterationFile);

                    auto start = chrono::high_resolution_clock::now();

                    pso->setCl(cl);
                    pso->setCg(cg);
                    pso->setW(w);

                    pso->setConstructionMode(mode);
                    pso->setSearchNeighbourhood(sn);

                    pso->run();
                    pso->printBest(os);

                    auto stop = chrono::high_resolution_clock::now();

                    os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                    cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                    os.flush();
                    os.close();

                }
            }
        }
    }
}

void runACO2(){
    struct ACO_config{
        std::string vrpPath;
        double alpha{};
        double beta{};
        double gamma{};
        ACO_config()= default;
        ACO_config(std::string v, double a, double b, double g): vrpPath(std::move(v)), alpha(a), beta(b), gamma(g){}
    };

//    vector<ACO_config> configs({{"../data/A/A-n60-k9.vrp", 1, 0.5, 3}, {"../data/A/A-n80-k10.vrp", 1, 1, 0.5}});
    vector<ACO_config> configs({{"../data/A/A-n32-k5.vrp", 1, 1, 0.5}, {"../data/A/A-n60-k9.vrp", 1, 0.5, 3}, {"../data/A/A-n80-k10.vrp", 1, 1, 0.5}});

    vector<bool> searchNeighbourhood({false, true});

    for(const ACO_config& config: configs){
        string vrpPath = config.vrpPath;
        double alpha = config.alpha;
        double beta = config.beta;
        double gamma = config.gamma;

        VRP vrp(vrpPath);

        for(bool sn: searchNeighbourhood){
            for(int i = 1; i <= 10; i++){
                shared_ptr<ACO> aco = make_shared<ACO>(vrp);

                string fileName = "ACO_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                    "_a_" + to_string(alpha) + "_b_" + to_string(beta) + "_g_" + to_string(gamma) +
                                    "_search_" + (sn ? "true" : "false") +
                                    "_" + to_string(i) + ".txt";

                string resultFile = "../results/ACO/experiment_data/" + fileName;
                string iterationFile = "../results/ACO/iteration_graph_data/" + fileName;

                ofstream os(resultFile);
                aco->outputToFile(iterationFile);

                auto start = chrono::high_resolution_clock::now();

                aco->setAlpha(alpha);
                aco->setBeta(beta);
                aco->setGamma(gamma);

                aco->setSearchNeighbourhood(sn);

                aco->run();
                aco->printBest(os);

                auto stop = chrono::high_resolution_clock::now();

                os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                os.flush();
                os.close();

            }
        }
    }
}

void runABC2(){
    struct ABC_config{
        std::string vrpPath;
        int popSize{};
        ABC_config()= default;
        ABC_config(std::string v, int ps): vrpPath(std::move(v)), popSize(ps){}
    };

//    vector<ABC_config> configs({{"../data/A/A-n32-k5.vrp", 10}, {"../data/A/A-n60-k9.vrp", 80}, {"../data/A/A-n80-k10.vrp", 80}});
    vector<ABC_config> configs({{"../data/A/A-n80-k10.vrp", 80}});

    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});
    for(const ABC_config& config: configs){
        string vrpPath = config.vrpPath;
        int popSize = config.popSize;

        VRP vrp(vrpPath);

        for(auto mode: constructionModes){
            for(int i = 1; i <= 10; i++){
                shared_ptr<ABC> abc = make_shared<ABC>(vrp);

                string fileName = "ABC_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                    "_ps_" + to_string(popSize) +
                                    "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                    "_" + to_string(i) + ".txt";

                string resultFile = "../results/ABC/experiment_data/" + fileName;
                string iterationFile = "../results/ABC/iteration_graph_data/" + fileName;

                ofstream os(resultFile);
                abc->outputToFile(iterationFile);

                auto start = chrono::high_resolution_clock::now();

                abc->setPopulationSize(popSize);

                abc->setConstructionMode(mode);

                abc->run();
                abc->printBest(os);

                auto stop = chrono::high_resolution_clock::now();

                os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                os.flush();
                os.close();

            }
        }
    }
}

void runTS2(){
    struct TS_config{
        std::string vrpPath;
        int tabuTenure{};
        TS_config()= default;
        TS_config(std::string v, int tt): vrpPath(std::move(v)), tabuTenure(tt){}
    };

    vector<TS_config> configs({{"../data/A/A-n60-k9.vrp", 5}, {"../data/A/A-n80-k10.vrp", 2}});
//    vector<TS_config> configs({{"../data/A/A-n32-k5.vrp", 20}, {"../data/A/A-n60-k9.vrp", 5}, {"../data/A/A-n80-k10.vrp", 2}});

    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});

    for(const TS_config& config: configs){
        string vrpPath = config.vrpPath;
        int tabuTenure = config.tabuTenure;

        VRP vrp(vrpPath);

        for(auto mode: constructionModes){
            for(int i = 1; i <= 10; i++){
                shared_ptr<TS> ts = make_shared<TS>(vrp);

                string fileName = "TS_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                    "_tt_" + to_string(tabuTenure) +
                                    "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                    "_" + to_string(i) + ".txt";

                string resultFile = "../results/TS/experiment_data/" + fileName;
                string iterationFile = "../results/TS/iteration_graph_data/" + fileName;

                ofstream os(resultFile);
                ts->outputToFile(iterationFile);

                auto start = chrono::high_resolution_clock::now();

                ts->setTabuTenure(tabuTenure);

                ts->setConstructionMode(mode);

                ts->run();
                ts->printBest(os);

                auto stop = chrono::high_resolution_clock::now();

                os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                os.flush();
                os.close();

            }
        }
    }
}

void runSA2(){
    struct SA_config{
        std::string vrpPath;
        double(*decF)(double){};
        double coef{};
        std::string dec;
        SA_config()= default;
        SA_config(std::string v, double(*df)(double), double coef, std::string dec): vrpPath(std::move(v)), decF(df), coef(coef), dec(std::move(dec)){}
    };

    vector<SA_config> configs({{"../data/A/A-n32-k5.vrp", &SA::verySlowDecrement, 0.01, "vslow"},
                               {"../data/A/A-n60-k9.vrp", &SA::geometricDecrement, 0.9999, "geom"},
                               {"../data/A/A-n80-k10.vrp", &SA::geometricDecrement, 0.99995, "geom"}});

    vector<BasePhenotype::ConstructionMode> constructionModes({BasePhenotype::RANDOM_FEASIBLE, BasePhenotype::GUIDED_CLASSIC});

    for(const SA_config& config: configs){
        string vrpPath = config.vrpPath;
        auto decF = config.decF;
        double coef = config.coef;
        string decString = config.dec;

        VRP vrp(vrpPath);

        for(auto mode: constructionModes){
            for(int i = 1; i <= 10; i++){
                shared_ptr<SA> sa = make_shared<SA>(vrp);

                string fileName = "SA_" + util::split(util::split(vrpPath, "/")[3], ".")[0] +
                                    "_decF_" + decString + "_coef_" + to_string(coef) +
                                    "_mode_" + ((mode == BasePhenotype::GUIDED_CLASSIC) ? "GC" : "RF") +
                                    "_" + to_string(i) + ".txt";

                string resultFile = "../results/SA/experiment_data/" + fileName;
                string iterationFile = "../results/SA/iteration_graph_data/" + fileName;

                ofstream os(resultFile);
                sa->outputToFile(iterationFile);

                auto start = chrono::high_resolution_clock::now();

                sa->setDecrementFunction(decF);
                SA::setSaB(coef);
                SA::setSaBeta(coef);
                SA::setSaAlpha(coef);

                sa->setConstructionMode(mode);

                sa->run();
                sa->printBest(os);

                auto stop = chrono::high_resolution_clock::now();

                os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

                cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

                os.flush();
                os.close();

            }
        }
    }
}

void runGAThreads(){
    string vrpPath = "../data/A/A-n32-k5.vrp";
    int popSize = 100;
    double mr = 0.005;
    BasePhenotype::ConstructionMode constructionMode = BasePhenotype::RANDOM_FEASIBLE;
    bool sn = false;

    VRP vrp(vrpPath);

    for(int i = 1; i <= 10; i++){
        shared_ptr<GA> ga = make_shared<GA>(vrp);

        string resultFile = "../results/GA/experiment_data/thread_count/GA_1_threads_" + to_string(i) + ".txt";
        ofstream os(resultFile);

        auto start = chrono::high_resolution_clock::now();

        ga->setPopulationSize(popSize);
        ga->setPM(mr);
        ga->setConstructionMode(constructionMode);
        ga->setSearchNeighbourhood(sn);

        ga->run();
        ga->printBest(os);

        auto stop = chrono::high_resolution_clock::now();

        os << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << " ms" << endl;

        cout << "-------------------------------" << endl << resultFile << endl << "-------------------------------" << endl << endl;

        os.flush();
        os.close();

    }
}